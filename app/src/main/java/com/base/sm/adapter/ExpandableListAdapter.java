package com.base.sm.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.base.activity.MainActivity;
import com.base.factory.SingleRenderer;
import com.base.sm.drawer.DrawerActionListener;
import com.base.sm.drawer.DrawerCloseListener;
import com.base.sm.drawer.RecycleViewItem;
import com.commonasset.utils.Print;
import com.softbondit.news.karu.R;
import com.retrofit.response.category.Category;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by anandbose on 09/06/15.
 */
public class ExpandableListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements SingleRenderer {
    public static final int HEADER = 0;
    public static final int CHILD = 1;
    public static final int CAT_ONLY = 2;
    public static final int SEARCH_VIEW = 3;
    private List<RecycleViewItem> data = new ArrayList<>();
    MainActivity context;
    DrawerCloseListener drawerCloseListener;

    public ExpandableListAdapter(DrawerCloseListener drawerCloseListener, MainActivity context) {
        this.context = context;
        this.drawerCloseListener = drawerCloseListener;
    }

    public void addItem(RecycleViewItem item){
        data.add(item);
    }

    public RecycleViewItem getItemAtPostion(int position){
        return  data.get(position);
    }
    public RecycleViewItem getItemByCatId(long catId){
        for (RecycleViewItem recycleViewItem: data){
            if (recycleViewItem.getCatId() == catId){
                return recycleViewItem;
            }
        }
        return null;
    }

    public void resetSelection(){
        for (RecycleViewItem item: data){
            item.setSelected(false);
        }
    }
    public void addCategories(ArrayList<Category> categories){
        ArrayList<Category> parents = new ArrayList<>();
        HashMap<Long, ArrayList<Category>> mapSubCategory = new HashMap<>();
        for (Category cat: categories){
            if (cat.getParent() != -1){
                ArrayList<Category> subCat = mapSubCategory.get(cat.getParent());
                if (subCat == null){
                    subCat = new ArrayList<>();
                    mapSubCategory.put(cat.getParent(), subCat);
                }
                subCat.add(cat);
            }else{
                parents.add(cat);
            }
        }

        if (parents != null && parents.size() > 0) {
            for (Category category : parents) {
                ArrayList<Category> subCats = mapSubCategory.get(category.getId());
                if (subCats == null) {
                    RecycleViewItem smi = new RecycleViewItem(category.getTitle(), category.getId(), ExpandableListAdapter.CAT_ONLY, 0);
                    data.add(smi);
                } else {
                    RecycleViewItem smi = new RecycleViewItem(category.getTitle(), category.getId(), ExpandableListAdapter.HEADER, 0);
                    data.add(smi);
                    for (Category subCatetory : subCats) {
                        RecycleViewItem child = new RecycleViewItem(subCatetory.getTitle(), subCatetory.getId(), ExpandableListAdapter.CHILD, 0);
                        data.add(child);
                    }
                }
            }
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int type) {
        View view = null;
        Context context = parent.getContext();
        float dp = context.getResources().getDisplayMetrics().density;
        LayoutInflater infalter = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        switch (type) {
            case SEARCH_VIEW:
                view = infalter.inflate(R.layout.drawer_list_search, parent, false);
                ListSearchViewHolder searchView = new ListSearchViewHolder(view);
                return searchView;
            case CAT_ONLY:
                view = infalter.inflate(R.layout.drawer_list_cat_only, parent, false);
                ListCatOnlyViewHolder headerCatOnly = new ListCatOnlyViewHolder(view);
                return headerCatOnly;
            case HEADER:
                view = infalter.inflate(R.layout.drawer_list_header, parent, false);
                ListHeaderViewHolder header = new ListHeaderViewHolder(view);
                return header;
            case CHILD:
                view = infalter.inflate(R.layout.drawer_list_subcat_only, parent, false);
                ListSubCatOnlyViewHolder headerSubCatOnly = new ListSubCatOnlyViewHolder(view);
                return headerSubCatOnly;
        }
        return null;
    }

    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        final RecycleViewItem item = data.get(position);
        switch (item.getType()) {
            case HEADER:
                final ListHeaderViewHolder itemController = (ListHeaderViewHolder) holder;
                itemController.refferalItem = item;
                itemController.header_title.setText(item.getTitle());
                if (item.getInvisibleChildren() == null) {
                    itemController.btn_expand_toggle.setImageResource(R.drawable.ic_up_indicator);
                } else {
                    itemController.btn_expand_toggle.setImageResource(R.drawable.ic_down_indicator);
                }
                itemController.itemController.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (item.getInvisibleChildren() == null) {
                            item.setInvisibleChildren(new ArrayList<RecycleViewItem>());
                            int count = 0;
                            int pos = data.indexOf(itemController.refferalItem);
                            while (data.size() > pos + 1 && data.get(pos + 1).getType() == CHILD) {
                                item.getInvisibleChildren().add(data.remove(pos + 1));
                                count++;
                            }
                            notifyItemRangeRemoved(pos + 1, count);
                            Print.e(this, "Close Sub list");
                            //itemController.btn_expand_toggle.setImageResource(R.drawable.circle_plus);
                            rotate(itemController.btn_expand_toggle, 0 , 180);
                        } else {
                            int pos = data.indexOf(itemController.refferalItem);
                            int index = pos + 1;
                            boolean groupItemSelectedFound = false;
                            for(RecycleViewItem recycleViewItem: data){
                                if(recycleViewItem.isSelected()){
                                    groupItemSelectedFound = true;
                                    break;
                                }
                            }
                            for (RecycleViewItem i : item.getInvisibleChildren()) {
                                if(groupItemSelectedFound){
                                    i.setSelected(false);
                                }
                                data.add(index, i);
                                index++;
                            }
                            Print.e(this, "Open Sub list");
                            notifyItemRangeInserted(pos + 1, index - pos - 1);
                            rotate(itemController.btn_expand_toggle, 180, 0);
                           // itemController.btn_expand_toggle.setImageResource(R.drawable.circle_minus);
                            item.setInvisibleChildren(null);
                        }
                    }
                });
                break;
            case CHILD:
                final ListSubCatOnlyViewHolder subCatOnlyItemController = (ListSubCatOnlyViewHolder) holder;
                TextView subCatOnlyTextView = (TextView) subCatOnlyItemController.header_title;
                subCatOnlyTextView.setText(data.get(position).getTitle());
                subCatOnlyItemController.row.setSelected(data.get(position).isSelected());
                setOnClicListner(holder.itemView, position);

                break;
            case CAT_ONLY:
                final ListCatOnlyViewHolder catOnlyItemController = (ListCatOnlyViewHolder) holder;
                TextView catOnlyTextView = (TextView) catOnlyItemController.header_title;
                catOnlyTextView.setText(data.get(position).getTitle());
                catOnlyItemController.row.setSelected(data.get(position).isSelected());
                setOnClicListner(holder.itemView, position);
                break;
            case SEARCH_VIEW:
                final ListSearchViewHolder searchViewHolder = (ListSearchViewHolder) holder;
                searchViewHolder.row.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        context.onDrawerSearchClicked();
                    }
                });
                break;
        }
    }

    private void setOnClicListner(View itemView, final int position){
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawerCloseListener.setItemClicked(true);
                Print.e(this, "Selected Position"+position);

                for(RecycleViewItem recycleViewItem: data){
                    recycleViewItem.setSelected(false);
                }
                context.setSelectedDrawerItem(data.get(position));
                data.get(position).setSelected(true);
                notifyDataSetChanged();
                ((DrawerActionListener)(context)).OnDrawerItemClickListener();
            }
        });
    }
    @Override
    public int getItemViewType(int position) {
        return data.get(position).getType();
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public void updateRenderer(Object arg) {
        Print.e(this, "updateRenderer");
    }

    @Override
    public void insertRenderer(Object arg) {
        Print.e(this, "insertRenderer");
    }

    @Override
    public void deleteRenderer(Object arg) {
        Print.e(this, "deleteRenderer");
    }

    private static class ListHeaderViewHolder extends RecyclerView.ViewHolder {
        public TextView header_title;
        public ImageView btn_expand_toggle;
        LinearLayout itemController;
        public RecycleViewItem refferalItem;
        public LinearLayout row;
        public ListHeaderViewHolder(View itemView) {
            super(itemView);
            header_title = (TextView) itemView.findViewById(R.id.header_title);
            itemController = (LinearLayout) itemView.findViewById(R.id.itemController);
            btn_expand_toggle = (ImageView) itemView.findViewById(R.id.btn_expand_toggle);
            row = (LinearLayout) itemView.findViewById(R.id.row);
        }
    }

    private static class ListCatOnlyViewHolder extends RecyclerView.ViewHolder {
        public TextView header_title;
        public LinearLayout row;
        public ListCatOnlyViewHolder(View itemView) {
            super(itemView);
            header_title = (TextView) itemView.findViewById(R.id.header_title);
            row = (LinearLayout) itemView.findViewById(R.id.row);
        }
    }

    private static class ListSubCatOnlyViewHolder extends RecyclerView.ViewHolder {
        public TextView header_title;
        public LinearLayout row;
        public ListSubCatOnlyViewHolder(View itemView) {
            super(itemView);
            header_title = (TextView) itemView.findViewById(R.id.header_title);
            row = (LinearLayout) itemView.findViewById(R.id.row);
        }
    }

    private static class ListSearchViewHolder extends RecyclerView.ViewHolder {
        public LinearLayout row;
        public ListSearchViewHolder(View itemView) {
            super(itemView);
            row = (LinearLayout) itemView.findViewById(R.id.row);
        }
    }
    private void rotate(ImageView imageView, int from, int to){
        RotateAnimation rotate = new RotateAnimation(from, to, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        rotate.setDuration(300);
        rotate.setFillAfter(true);
        rotate.setInterpolator(new LinearInterpolator());
        imageView.startAnimation(rotate);
    }

    private void rotateInstant(ImageView imageView, int from, int to){
        RotateAnimation rotate = new RotateAnimation(from, to, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        rotate.setFillAfter(true);
        rotate.setInterpolator(new LinearInterpolator());
        imageView.startAnimation(rotate);
    }

}
