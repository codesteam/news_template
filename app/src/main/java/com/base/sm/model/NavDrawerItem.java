package com.base.sm.model;


import android.annotation.SuppressLint;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import com.base.activity.MainActivity;
import com.base.utility.FragmentParent;


public  class NavDrawerItem extends FragmentParent {
    public static Fragment parentFragment;
    private String title;
    protected FragmentManager fragmentManager;
    protected MainActivity context;
    public NavDrawerItem() {

    }

    @SuppressLint("ValidFragment")
    public NavDrawerItem(MainActivity context, String title)
    {
        this.title = title;
        this.context = context;
        fragmentManager = ((MainActivity) context).getSupportFragmentManager();

    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
        context.setTitle(title + "");
    }

    /**
     *To intercept sliding menu click event. Call super.fire() to enable default behaviour.
     */
    public  void fire(){
        ((FragmentParent) NavDrawerItem.parentFragment).hide();
        fragmentManager.beginTransaction().hide(NavDrawerItem.parentFragment).show(this).commit();
        NavDrawerItem.parentFragment = this;
        this.awake();
    }

}
