package com.base.sm.drawer;

import java.util.List;

/**
 * Created by water on 11/29/16.
 */
public class DrawerItem {
    SmItemSelection parent;
    List<SmItemSelection> childs;

    public DrawerItem(SmItemSelection parent, List<SmItemSelection> childs){
        this.parent = parent;
        this.childs = childs;
    }
    public SmItemSelection getParent() {
        return parent;
    }

    public List<SmItemSelection> getChilds() {
        return childs;
    }

    public int hashChild() {
        if (childs != null){
            return  childs.size();
        }
        return 0;
    }
}
