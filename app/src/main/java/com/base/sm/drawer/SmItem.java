package com.base.sm.drawer;

public class SmItem {

	private String name;
	private SmItemSelection smItem;
	public SmItem(String name, SmItemSelection smItem)
	{
		this.name = name;
		this.smItem = smItem;
	}
	
	public SmItem(String name)
	{
		this.name = name;
	}
	
	public String getName()
	{
		return name;
	}
	
	public SmItemSelection getSMItemSelection()
	{
		return smItem;
	}
}
