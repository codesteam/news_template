package com.base.sm.drawer;

import android.support.v4.widget.DrawerLayout;
import android.view.View;

import com.base.activity.MainActivity;

/**
 * Created by water on 11/30/16.
 */
public class DrawerCloseListener implements DrawerLayout.DrawerListener {
    private MainActivity context;
    private boolean itemClicked;

    public void setItemClicked(boolean itemClicked) {
        this.itemClicked = itemClicked;
    }

    public DrawerCloseListener(MainActivity context){
        this.context = context;
    }

    @Override
    public void onDrawerSlide(View drawerView, float slideOffset) {

    }

    @Override
    public void onDrawerOpened(View drawerView) {

    }

    @Override
    public void onDrawerClosed(View drawerView) {
        if (itemClicked){
            itemClicked = false;
            ((DrawerActionListener)(context)).OnDrawerClosedListener();
        }
    }

    @Override
    public void onDrawerStateChanged(int newState) {

    }
}
