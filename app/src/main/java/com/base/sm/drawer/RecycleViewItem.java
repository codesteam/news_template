package com.base.sm.drawer;

import java.util.List;

/**
 * Created by water on 11/30/16.
 */
public class RecycleViewItem {
    private long catId;
    private String title;
    private int type;
    private int group;
    private List<RecycleViewItem> invisibleChildren;
    boolean selected;
    public RecycleViewItem(String title, long catId, int type, int group){
        this.title = title;
        this.catId = catId;
        this.type = type;
        this.group = group;
    }

    public int getType() {
        return type;
    }

    public long getCatId() {
        return catId;
    }
    public String getTitle() {
        return title;
    }

    public List<RecycleViewItem> getInvisibleChildren() {
        return invisibleChildren;
    }

    public void setInvisibleChildren(List<RecycleViewItem> invisibleChildren) {
        this.invisibleChildren = invisibleChildren;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public int getGroup() {
        return group;
    }
}
