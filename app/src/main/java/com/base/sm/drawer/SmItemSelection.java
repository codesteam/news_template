package com.base.sm.drawer;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.base.activity.MainActivity;
import com.base.fragments.FragSubGroupList;
import com.base.sm.model.NavDrawerItem;
import com.softbondit.news.karu.R;

import java.util.List;


public class SmItemSelection extends NavDrawerItem {

	FragSubGroupList subCategoryFragment;
	public List<SmItemSelection> invisibleChildren;
	public int type;
	public SmItemSelection()
	{

	}

    @SuppressLint("ValidFragment")
    public SmItemSelection(MainActivity context, String title, long groupId, long subgroupId, int type) {
        super(context, title);
		this.type = type;

    }

	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		//Print.e(this, "onCreateView");
		final View m_view = inflater.inflate (R.layout.fragment_parent,null);

		subCategoryFragment = new FragSubGroupList();
		//groupItems.initHeaderMessage(new StringBuilder(getTitle()));
		//groupItems.initList(subObjs);
		initialize(R.id.fa_parent);
		//addFragment(subCategoryFragment);
		return m_view;
	}
	
	@Override
	  public void onDestroy() {
	    super.onDestroy();
	  }
	
	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		//groupItems.initHeaderMessage(new StringBuilder(getTitle()));
		//groupItems.initList(subObjs);
		super.onResume();
	}
	
	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}
	@Override
	public void fire()
	{
		action();

		/*sm_packer.Toggle();
		//Print.e(this, "fire");
		((FragmentParent)SMEvent.parent_fragment).onSmHide();
		fragmentManager.beginTransaction().hide(SMEvent.parent_fragment).show(this).commit();
		groupItems.reloadList(subObjs);
		SMEvent.parent_fragment = this;
		groupItems.initHeaderMessage(getEventName());
		this.onSmSelected();*/
	}

	private void action(){
		super.fire();
		//groupItems.reloadList(subObjs);
		//groupItems.initHeaderMessage(new StringBuilder(getTitle()));
	}

}
