package com.base.sm.drawer;

/**
 * Created by water on 11/30/16.
 */
public interface DrawerActionListener {
    void OnDrawerItemClickListener();
    void OnDrawerClosedListener();

}
