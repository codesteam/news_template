package com.base.common;

import android.app.Activity;
import android.content.Intent;

import com.base.activity.EntryDetailsActivity;
import com.db.helper.EntryBinding;

import static com.base.activity.EntryDetailsActivity.EXTRA_ENTRY_BINCING;

/**
 * Created by water on 6/19/17.
 */

public class StaticFun {
    public static String getYoutubeIdFromUrl(String url){
        String[] parts = url.split("=");
        if (parts != null && parts.length > 1){
            return parts[1];
        }else{
            return  "";
        }

    }

    public static void OPEN_DETAILS_ACTIVITY(EntryBinding selectedEntry, Activity activity, int requestCodeForReturnActivity){
        //((MainActivity)this.getActivity()).setSelectedCatId(catId);
        // setting the new slected catId, because details view called from MainActivity Search view,
        //example: when in user view showing cat  10 items but user select cat 11 item from search view
        Intent intent = new Intent(activity, EntryDetailsActivity.class);
        intent.putExtra(EXTRA_ENTRY_BINCING,  selectedEntry);
        activity.startActivityForResult(intent, requestCodeForReturnActivity);
    }

    /*public static void OPEN_DETAILS_ACTIVITY_MULTIPLE_INSTANCE(EntryBinding selectedEntry, Activity activity){
        Intent intent = new Intent(activity, EntryDetailsActivity.class);
        intent.putExtra(EXTRA_ENTRY_BINCING,  selectedEntry);
        activity.startActivity(intent);
    }*/

}
