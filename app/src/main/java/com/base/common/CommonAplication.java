package com.base.common;

import android.app.Application;
import android.content.Context;
import android.os.Build;

import com.db.DBManager;
import com.materialsearchview.SearchViewManager;

/**
 * Created by water on 5/14/15.
 */


public class CommonAplication extends Application {

    private static Context mContext;
    DBManager dbManager;
    //RenderManager renderManager;

    public static  String ACTION_START_SYNC	= "ACTION_START_SYNC.";
    public static  String ACTION_STOP_SYNC	= "ACTION_STOP_SYNC.";
    public static  String ACTION_SYNC_COMPLETE = "ACTION_SYNC_COMPLETE.";
    public static Context getAppContext() {
        return CommonAplication.mContext;
    }
    @Override
    public void onCreate() {
        super.onCreate();
        ACTION_START_SYNC += getPackageName();
        ACTION_STOP_SYNC += getPackageName();
        ACTION_SYNC_COMPLETE += getPackageName();

        CommonAplication.mContext = getApplicationContext();
        dbManager = DBManager.getInstance(mContext);
        //renderManager = RenderManager.getInstance(mContext);
        dbManager.createDaoMap(mContext);


      //initImageLoader();
    }

   /* private void initImageLoader() {

        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
                .cacheOnDisc(true).cacheInMemory(true)
                .imageScaleType(ImageScaleType.EXACTLY)
                .displayer(new FadeInBitmapDisplayer(300)).build();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
                getApplicationContext())
                .defaultDisplayImageOptions(defaultOptions)
                .memoryCache(new WeakMemoryCache())
                .discCacheSize(100 * 1024 * 1024).build();

        ImageLoader.getInstance().init(config);
    }*/
}
