package com.base.common;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.softbondit.news.karu.R;


public class CommonSettings {


    public static String PACKAGE_NAME;
    private static CommonSettings singleton = null;
    private String KEY_SD_CARD_STATE= ".KEY_SD_CARD_STATE";
    private String KEY_STORAGE_ROOT_PATH = ".KEY_STORAGE_ROOT_PATH";
    private String KEY_APP_FIRST_TIME_RUN = ".KEY_APP_FIRST_TIME_RUN";
    private String NOTIFICATION_ENABLE = ".NOTIFICATION_ENABLE";
    private String NOTIFICATION_SOUND_ENABLE = ".NOTIFICATION_SOUND_ENABLE";
    private String NOTIFICATION_VIVRATION_ENABLE = ".NOTIFICATION_VIVRATION_ENABLE";
    private String NOTIFIED = ".NOTIFIED";
    private String KEY_DETAILS_PAGE_TEXT_SIZE= ".KEY_DETAILS_PAGE_TEXT_SIZE";
    private SharedPreferences _prefs;
    private SharedPreferences.Editor _editor;
    Context context;
    private CommonSettings(Context context) {
        PACKAGE_NAME = context.getPackageName();
        _prefs = PreferenceManager.getDefaultSharedPreferences(context);
        _editor = _prefs.edit();
        this.context = context;
    }

    public static CommonSettings getInstance(Context context) {
        if (singleton == null){
            singleton = new CommonSettings(context);
        }
        return singleton;
    }


    @Override
    protected Object clone() throws CloneNotSupportedException {

        return new CloneNotSupportedException();
    }

    public void setString(String key, String value){
        _editor.putString(key, value);
        _editor.commit();
    }

    public String getString(String key){
        return _prefs.getString(key, null);
    }



    public void setSDCardState(boolean value)
    {
        _editor.putBoolean(PACKAGE_NAME + KEY_SD_CARD_STATE, value);
        _editor.commit();
    }

    public boolean getSDCardState()
    {
        return _prefs.getBoolean(PACKAGE_NAME + KEY_SD_CARD_STATE, true);

    }

    public String getStorageRootPath() {

        return _prefs.getString(PACKAGE_NAME + KEY_STORAGE_ROOT_PATH, null);
    }

    public void setStorageRootPath(String value)
    {
        _editor.putString(PACKAGE_NAME + KEY_STORAGE_ROOT_PATH, value);
        _editor.commit();
    }


    public void setAppFirstTimeRuning(boolean value)
    {
        _editor.putBoolean(PACKAGE_NAME + KEY_APP_FIRST_TIME_RUN, value);
        _editor.commit();
    }

    public boolean getAppFirstTimeRuning()
    {
        return _prefs.getBoolean(PACKAGE_NAME + KEY_APP_FIRST_TIME_RUN, true);

    }

    public void setNotificationEnable(boolean value){
        _editor.putBoolean(PACKAGE_NAME + NOTIFICATION_ENABLE, value);
        _editor.commit();
    }

    public boolean getNotificationEnable(){
        return _prefs.getBoolean(PACKAGE_NAME + NOTIFICATION_ENABLE, true);
    }

    public void setNotificationVibrationEnable(boolean value){
        _editor.putBoolean(PACKAGE_NAME + NOTIFICATION_VIVRATION_ENABLE, value);
        _editor.commit();
    }

    public boolean getNotificationVibrationEnable(){
        return _prefs.getBoolean(PACKAGE_NAME + NOTIFICATION_VIVRATION_ENABLE, true);
    }

    public void setNotificationSoundEnable(boolean value){
        _editor.putBoolean(PACKAGE_NAME + NOTIFICATION_SOUND_ENABLE, value);
        _editor.commit();
    }

    public boolean getNotificationSoundEnable(){
        return _prefs.getBoolean(PACKAGE_NAME + NOTIFICATION_SOUND_ENABLE, true);
    }
    public void setNotified(boolean value){
        _editor.putBoolean(PACKAGE_NAME + NOTIFIED, value);
        _editor.commit();
    }

    public boolean getNotified(){
        return _prefs.getBoolean(PACKAGE_NAME + NOTIFIED, true);
    }

    public void setDetailsPageTextSize(int value)
    {
        _editor.putInt(PACKAGE_NAME + KEY_DETAILS_PAGE_TEXT_SIZE, value);
        _editor.commit();
    }

    public int getDetailsPageTextSize()
    {
        return _prefs.getInt(PACKAGE_NAME + KEY_DETAILS_PAGE_TEXT_SIZE, (int) context.getResources().getDimension(R.dimen.setting_text_size_normal));

    }
}
