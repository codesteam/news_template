package com.base.common;

import android.os.Build;

public class CommonConstants {

    public static int REQUEST_CODE_FOR_DETAILS_ACTIVITY = 65;
    public static int DEFAULT_CAT_LIST_POSITION=0;  // for those case when we have to pick a category from entry itself
                                                    // since entry return list of categoris so pick the first index position
                                                    // e.g when user click on notification we have to find entry cat from catlist that entry has
                                                    // but we dont no for which category entry is pointing specifically
                                                    // so using index 0

    public static int DEFAULT_MIDIA_LIST_POSITION=0; // since there is media list (small list, large list, video list)
                                                    // which one we will sow for thumbnil, there is no selection specifically
                                                    // so using the index 0
    public static int DEFAULT_CAT_SEARCH_ID = 0;
    public static int DEFAULT_CAT_HOME_ID = 99;
    public static int MAX_NUMBER_OF_SPECIAL_NEWS = 3;
    public static int DEFAULT_SELECTED_DRAWER_ITME = 1;
    public static String NOT_FOUND = "404";
    public static int MIN_SDK_VERSION_FOR_SEARCH_VEIW = Build.VERSION_CODES.ICE_CREAM_SANDWICH;
    public static int swipeRefreshLayoutProgressOfset = 2; // must be even number// this value multiplied with action bar size
    public static final int AD_WAIT_IN_SEC = 10;
}
