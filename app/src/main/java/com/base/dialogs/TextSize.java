package com.base.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RelativeLayout;

import com.base.common.CommonSettings;
import com.base.factory.TextSizeChangeListner;
import com.softbondit.news.karu.R;

/**
 * Created by steam on 8/1/17.
 */

public class TextSize  extends Dialog implements
        android.view.View.OnClickListener {

    public Context c;
    public Dialog d;
    public Button yes, no;
    RelativeLayout rlSmall;
    CommonSettings commonSettings;
    int textSize;
    int tsSmall;
    int tsNormall;
    int tsLarge;
    int tsHuge;
    RadioButton rbSmall;
    RadioButton rbNormall;
    RadioButton rbLarge;
    RadioButton rbHuge;
    RadioButton selectedRb;
    TextSizeChangeListner textSizeChangeListner;
    int initTextSize;
    public TextSize(Context c, TextSizeChangeListner textSizeChangeListner) {
        super(c);
        // TODO Auto-generated constructor stub
        this.c = c;
        commonSettings = CommonSettings.getInstance(c);
        this.textSizeChangeListner = textSizeChangeListner;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_text_size);
        setCancelable(false);
        ((RelativeLayout) findViewById(R.id.rlSmall)).setOnClickListener(this);
        ((RelativeLayout) findViewById(R.id.rlNormal)).setOnClickListener(this);
        ((RelativeLayout) findViewById(R.id.rlLarge)).setOnClickListener(this);
        ((RelativeLayout) findViewById(R.id.rlHuge)).setOnClickListener(this);
        ((Button) findViewById(R.id.btnCancel)).setOnClickListener(this);
        ((Button) findViewById(R.id.btnDefaultSize)).setOnClickListener(this);
        ((Button) findViewById(R.id.btnApply)).setOnClickListener(this);
        rbSmall = (RadioButton) findViewById(R.id.radioSmall);
        rbNormall = (RadioButton) findViewById(R.id.radioNormal);
        rbLarge = (RadioButton) findViewById(R.id.radioLarge);
        rbHuge = (RadioButton) findViewById(R.id.radioHuge);

        tsSmall = (int) c.getResources().getDimension(R.dimen.setting_text_size_small);
        tsNormall = (int) c.getResources().getDimension(R.dimen.setting_text_size_normal);
        tsLarge = (int) c.getResources().getDimension(R.dimen.setting_text_size_large);
        tsHuge = (int) c.getResources().getDimension(R.dimen.setting_text_size_huge);
        initTextSize = textSize = commonSettings.getDetailsPageTextSize();

        if (textSize == tsSmall){
            rbSmall.setChecked(true);
            selectedRb = rbSmall;
        }else if (textSize == tsNormall){
            rbNormall.setChecked(true);
            selectedRb = rbNormall;
        }else if(textSize == tsLarge){
            rbLarge.setChecked(true);
            selectedRb = rbLarge;
        }else if (textSize == tsHuge){
            rbHuge.setChecked(true);
            selectedRb = rbHuge;
        }


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rlSmall:
                textSize = (int) c.getResources().getDimension(R.dimen.setting_text_size_small);
                textSizeChangeListner.onTextSizeChange(textSize);
                selectedRb.setChecked(false);
                selectedRb = rbSmall;
                selectedRb.setChecked(true);
                break;
            case R.id.rlNormal:
                textSize = (int) c.getResources().getDimension(R.dimen.setting_text_size_normal);
                textSizeChangeListner.onTextSizeChange(textSize);
                selectedRb.setChecked(false);
                selectedRb = rbNormall;
                selectedRb.setChecked(true);
                break;
            case R.id.rlLarge:
                textSize = (int) c.getResources().getDimension(R.dimen.setting_text_size_large);
                textSizeChangeListner.onTextSizeChange(textSize);
                selectedRb.setChecked(false);
                selectedRb = rbLarge;
                selectedRb.setChecked(true);
                break;
            case R.id.rlHuge:
                textSize = (int) c.getResources().getDimension(R.dimen.setting_text_size_huge);
                textSizeChangeListner.onTextSizeChange(textSize);
                selectedRb.setChecked(false);
                selectedRb = rbHuge;
                selectedRb.setChecked(true);
                break;
            case R.id.btnCancel:
                if (initTextSize != textSize){
                    textSizeChangeListner.onTextSizeChange(initTextSize);
                }
                dismiss();
                break;
            case R.id.btnDefaultSize:
                textSize = (int) c.getResources().getDimension(R.dimen.setting_text_size_normal);
               // if (initTextSize != textSize){
                    commonSettings.setDetailsPageTextSize(textSize);
                    textSizeChangeListner.onTextSizeChange(textSize);
               // }
                dismiss();
                break;
            case R.id.btnApply:
                commonSettings.setDetailsPageTextSize(textSize);
                dismiss();
                break;
            default:
                break;
        }
    }
}


