package com.base.components;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.text.Html;
import android.widget.RemoteViews;

import com.base.activity.MainActivity;
import com.base.common.CommonSettings;
import com.commonasset.utils.TimestampConverter;
import com.db.dao.EntryDao;
import com.softbondit.news.karu.R;
import com.retrofit.response.entry.Entry;

import java.util.Random;

/**
 * Created by water on 1/8/17.
 */

public class ComplexNotification {
    private Entry entry;
    private Context mContext;
    private int NOTIFICATION_ID = 987;

    public ComplexNotification(Context mContext, Entry entry){
        this.entry = entry;
        this.mContext = mContext;
    }
    public NotificationCompat.Builder buildNotificationCompact() {
        /*PendingIntent pIntent = PendingIntent.getActivity(
                mContext,
                NOTIFICATION_ID,
                getIntent(entry),
                PendingIntent.FLAG_UPDATE_CURRENT);*/
        String title = "<b>"+mContext.getString(R.string.app_name)+"</b> "+"<small>"+TimestampConverter.parseDate(entry.getCreatedAt()+"")+"</small>";

        NotificationCompat.Builder builder = new NotificationCompat.Builder(mContext)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setTicker(mContext.getString(R.string.app_name)) // text that show on notification bar
                .setAutoCancel(true)
                .setContentIntent(getPendingIntent(entry))
                .setContentTitle(Html.fromHtml(title))
                .setContentText(entry.getTitle())
                .setSmallIcon(R.mipmap.ic_launcher)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setGroup(mContext.getString(R.string.app_name));


        if (!CommonSettings.getInstance(mContext).getNotified()){
            if (CommonSettings.getInstance(mContext).getNotificationSoundEnable()){
                //builder.setSound(Uri.parse("android.resource://" + mContext.getPackageName() + "/" + R.raw.push_breaking_news));
            }
            if (CommonSettings.getInstance(mContext).getNotificationVibrationEnable()){
                //builder.setVibrate(new long[] { 1000, 1000});
            }
            CommonSettings.getInstance(mContext).setNotified(true);
        }

        NotificationCompat.BigTextStyle bigText = new NotificationCompat.BigTextStyle();
        bigText.bigText(Html.fromHtml(entry.getShortDescription()+"\n\ntap to view"));
        bigText.setBigContentTitle(Html.fromHtml(title));
        builder.setStyle(bigText);


        return builder;

    }

    private RemoteViews getComplexNotificationView() {
        // Using RemoteViews to bind custom layouts into Notification
        RemoteViews notificationView = new RemoteViews(
                mContext.getPackageName(),
                R.layout.complex_notification_item
        );

        // Locate and set the Image into customnotificationtext.xml ImageViews
        notificationView.setImageViewResource(R.id.ivIcon, R.mipmap.ic_launcher);

        // Locate and set the Text into customnotificationtext.xml TextViews
        notificationView.setTextViewText(R.id.tvAppName, mContext.getString(R.string.app_name));
        notificationView.setTextViewText(R.id.tvTitel, entry.getTitle());
        notificationView.setTextViewText(R.id.tvTime, TimestampConverter.parseDate(entry.getCreatedAt()+""));
        //notificationView.setTextViewText(R.id.text, getText());

        return notificationView;
    }


    private PendingIntent getPendingIntent(Entry entry){
        Intent notificationIntent = new Intent(mContext, MainActivity.class);

        Bundle bundle = new Bundle();
        bundle.putLong(EntryDao.ID, entry.getId());
        notificationIntent.putExtras(bundle);

        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent contentIntent = PendingIntent.getActivity(mContext,
                new Random().nextInt(), notificationIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        return contentIntent;
        //PendingIntent pi = PendingIntent.getActivity(mContext, 0, intent, Intent.FLAG_ACTIVITY_NEW_TASK);
    }
 /*   private Intent getIntent(Entry entry){
        Intent intent = new Intent(CommonAplication.getAppContext(), MainActivity.class);
        intent.putExtra(STATUS_NOTIFICATION, STATUS_NOTIFICATION);
        intent.putExtra(BaseDao.ID, entry.getId());
        if (entry.getCategories()!= null && entry.getCategories().size() > 0){
            intent.putExtra(Entry_CatDao.ENTRY_ID, entry.getCategories().get(0));
        }
        return intent;
        //PendingIntent pi = PendingIntent.getActivity(mContext, 0, intent, Intent.FLAG_ACTIVITY_NEW_TASK);
    }*/
}
