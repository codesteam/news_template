package com.base.components;

import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;

import com.base.common.CommonSettings;
import com.db.dao.EntryDao;
import com.retrofit.api.APIClientResponse;
import com.retrofit.handler.EntryGetAPICall;
import com.retrofit.handler.StatusGetAPICall;
import com.retrofit.response.entry.Entry;
import com.retrofit.response.entry.EntryResponce;

import java.util.ArrayList;

/**
 * Created by water on 12/22/16.
 */

public class StatusReceiver extends BroadcastReceiver implements RenderRecognizer{

    private Context mContext;
    @Override
    public void onReceive(final Context context, Intent intent) {

        mContext = context;
       // Print.e(this, "onReceive");
      /* if (Intent.ACTION_BOOT_COMPLETED.equals(intent.getAction())) {
            *//*Intent serviceIntent = new Intent(context, MyService.class);
            context.startService(serviceIntent);*//*
           Print.e(this, "StatusReceiver ACTION_BOOT_COMPLETED");

        }*/
        callToStatusApi(context);
    }

    private void callToStatusApi(Context context){
        StatusGetAPICall.getInstance().callAPI(context, this, new APIClientResponse() {
            @Override
            public void failureOnApiCall(String msg, Object sender) {

            }

            @Override
            public void failureOnNetworkConnection(String msg, Object sender) {

            }

            @Override
            public void successOnApiCall(String msg, Object sender) {

                switch (msg){
                    case EntryGetAPICall.SUCCESS_ENTRY_RESPONCE:
                        if (CommonSettings.getInstance(mContext).getNotificationEnable()){
                            EntryResponce entryResponce = (EntryResponce) sender;
                            ArrayList<Entry> entries = (ArrayList<Entry>) entryResponce.getEntries();
                            if (entries!= null){
                                CommonSettings.getInstance(mContext).setNotified(false);
                                Notifier notifier = new Notifier(mContext, entries);
                                notifier.start();
                            }
                        }
                        break;
                }
            }
        });
    }

    private void sendNotificaton(Entry entry){
        NotificationCompat.Builder mBuilder = new ComplexNotification(mContext, entry).buildNotificationCompact();
        NotificationManager mNotificationManager =
                (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);

        mNotificationManager.notify((int)((long)entry.getId()), mBuilder.build());

    }

    class Notifier extends Thread {

        public boolean isRunning = false;
        public long DELAY = 2500;
        ArrayList<Entry> entries;
        EntryDao entryDao;
        Context context;
        public Notifier(Context context, ArrayList<Entry> entries){
            this.entries = entries;
            this.context = context;
            entryDao = new EntryDao(mContext);
        }
        @Override
        public void run() {
            super.run();

            isRunning = true;
            while (isRunning) {
                try {
                    for(Entry entry: entries){
                        if (entryDao.getNotified(entry) == 0){
                            sendNotificaton(entry);
                            //entryDao.updateNotified(entry, 1);
                            Thread.sleep(DELAY);
                        }
                    }
                    isRunning = false;
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    isRunning = false;
                }
            }
        }

        public boolean isRunning() {
            return this.isRunning;
        }
    }



    private static int MINUTE = 1;
    private static int CALL_AFTER_MINUTE = 90 * 1000;

    public static void setupAlarm(Context context) {
        Intent myIntent = new Intent(context, StatusReceiver.class);
        PendingIntent myPendingIntent = PendingIntent.getBroadcast(context, 0, myIntent, 0);
        AlarmManager alarmMgr = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmMgr.setInexactRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, 1000, CALL_AFTER_MINUTE, myPendingIntent);
    }
    public static void cancelAlarm(Context context) {

        Intent intent = new Intent(context, StatusReceiver.class);
        PendingIntent sender = PendingIntent
                .getBroadcast(context, 0, intent, 0);
        AlarmManager alarmManager = (AlarmManager) context
                .getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(sender);

    }
}