/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.base.activity;

import android.animation.Animator;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.annotation.RequiresApi;
import android.support.design.widget.AppBarLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.DecelerateInterpolator;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.base.adapter.ScreenSlidePagerAdapter;
import com.base.common.CommonConstants;
import com.base.dialogs.TextSize;
import com.base.factory.EntryCatRenderer;
import com.base.factory.OnOpenEntry;
import com.base.factory.TextSizeChangeListner;
import com.base.fragments.FragDetails;
import com.base.modarate.view.IAdView;
import com.base.modarate.view.IZixing;
import com.base.modarate.view.Modarate;
import com.base.modarate.view.ZixisngSettings;
import com.base.utility.ConnectionDetector;
import com.base.utility.SoftKeyboard;
import com.commonasset.utils.Print;
import com.db.dao.CategoryDao;
import com.db.dao.EntryDao;
import com.db.helper.EntryBinding;
import com.github.ksoichiro.android.observablescrollview.ScrollState;
import com.github.ksoichiro.android.observablescrollview.Scrollable;
import com.softbondit.news.karu.BuildConfig;
import com.softbondit.news.karu.R;
import com.materialsearchview.MaterialSearchView;
import com.materialsearchview.OnSearchItemClick;
import com.materialsearchview.SearchViewManager;
import com.materialsearchview.db.SearchItem;
import com.retrofit.api.APIClientResponse;
import com.retrofit.handler.EntryGetAPICall;

import java.util.ArrayList;
import java.util.List;


public class EntryDetailsActivity<S extends Scrollable> extends AppCompatActivity implements IAdView, IZixing,  OnSearchItemClick, OnOpenEntry, TextSizeChangeListner {


    public static final String EXTRA_ENTRY_BINCING = "entry_binding";
    public static final String EXTRA_LIST_ENTRY_BINCING = "list_entry_binding";
    public static final String EXTRA_RECURSIVE_KILL = "recursive_kill";
    public static final String EXTRA_ENTRY_CAT = "entry_cat";

    RelativeLayout pagerContainer;
    LinearLayout searchViewContainer;
    SearchViewManager searchViewManager;
    private AppBarLayout mToolbar;
    List<Long> cats;
    ArrayList<? extends EntryBinding> sameCatAllEntries;
    private ViewPager mPager;
    private ScreenSlidePagerAdapter mPagerAdapter;
    Long selectedCatId;
    EntryBinding entryBinding;
    private Modarate modarate;
    int toolbarHeight;
    int yPosAppbar;
    int pagerBasePadding;
    int pagerCurrentPadding;
    ScrollState scrollState;
    int toolbarAnimDuration = 300;
    int scrollViewVelocityY = 0;
    int scrollSpeed = 1;
    int scrollSpeedMul = 1;
    boolean activityJustOpened;
    PageChangeListener pageChangeListener;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        MainActivity.forwardTransction = false;
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        activityJustOpened = true;
        mToolbar = (AppBarLayout) findViewById(R.id.appbar);
        pagerContainer = (RelativeLayout) findViewById(R.id.pagerContainer) ;
        modarate = ZixisngSettings.getInstance(this).createViewFliper(this,this, getString(R.string.ad_banner_unit_id),
                getString(R.string.ad_inter_unit_id), CommonConstants.AD_WAIT_IN_SEC, BuildConfig.system_droid );
        Bundle bundle = getIntent().getExtras();
        entryBinding  = (EntryBinding) bundle.getSerializable(EXTRA_ENTRY_BINCING);
        selectedCatId = entryBinding.getEntry().getPrimaryCatId();
        mPager = (ViewPager) findViewById(R.id.pager);
        mPagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());
        mPager.setAdapter(mPagerAdapter);
        mPager.addOnPageChangeListener(pageChangeListener = new PageChangeListener());
        cats = entryBinding.getEntry().getCategories();
        searchViewContainer = (LinearLayout) findViewById(R.id.searchViewContainer);
        if (Build.VERSION.SDK_INT >= CommonConstants.MIN_SDK_VERSION_FOR_SEARCH_VEIW) {
            searchViewManager = SearchViewManager.getInstance(this);
            searchViewManager.addParrent((ViewGroup) findViewById(R.id.searchViewContainer));
        }else{
            searchViewContainer.setVisibility(View.GONE);
        }

        TypedValue tv = new TypedValue();
        if (getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true)) {
            toolbarHeight = TypedValue.complexToDimensionPixelSize(tv.data,getResources().getDisplayMetrics());
        }

        mToolbar.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if (Build.VERSION.SDK_INT < 16) {
                    mToolbar.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                } else {
                    mToolbar.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                }

                int[] locationApBar = new int[2];
                mToolbar.getLocationInWindow(locationApBar);
                yPosAppbar =  locationApBar[1];
                pagerCurrentPadding = pagerBasePadding = toolbarHeight;


            }
        });

        new DBOperation().execute("");
        //mToolbar.setVisibility(View.INVISIBLE);
        setBViewFliper();
        Print.e(this, "toolbarHeight : "+ toolbarHeight);

    }
    @Override
    protected void onResume() {
        super.onResume();
        if (Build.VERSION.SDK_INT >= CommonConstants.MIN_SDK_VERSION_FOR_SEARCH_VEIW) {
            searchViewManager.getSearchView().activityResumed();
            searchViewManager.setSearchItemClickListner(this);
            searchViewManager.addParrent(searchViewContainer);
        }
        modarate.resume();
    }
    @Override
    protected void onPause() {
        super.onPause();
        if (Build.VERSION.SDK_INT >= CommonConstants.MIN_SDK_VERSION_FOR_SEARCH_VEIW) {
            searchViewManager.removeParrent();
        }
        modarate.pause();
    }

    @Override
    protected void onDestroy() {

        super.onDestroy();
        modarate.onDestroy();
    }
    @Override
    public void setBViewFliping() {
        setBViewFliper();
    }

    public void setBViewFliper() {
        if (ConnectionDetector.isNetworkPresent(this)) {
            modarate.setBViewFliper((LinearLayout) findViewById(R.id.llAdView));
        } else {
            setBFliperVisibility(View.GONE);
        }

    }

    public void setBFliperVisibility(int v) {
        modarate.setBFliperVisibility(v);
    }
    @Override
    public void show() {
        // TODO Auto-generated method stub
        Print.e(this, "Show In add");
        if (ConnectionDetector.isNetworkPresent(this)) {
            modarate.show();

        }

    }
    @Override
    public void onTextSizeChange(int size) {
        FragDetails currentFrag = (FragDetails) mPagerAdapter.getFragmentAt(pageChangeListener.getCurrentPage());
        currentFrag.pageTextSize(size);
    }

    @Override
    public void onTextChangeApply() {

    }

    private class DBOperation extends AsyncTask<String, Void, String> {

        boolean notFound = false;
        @Override
        protected String doInBackground(String... params) {
            sameCatAllEntries = (ArrayList<? extends EntryBinding>) EntryCatRenderer.getInstance().get(selectedCatId);
            if (sameCatAllEntries==null){
                //this cat entries not found in EtryCatRenderer, pull  from local database
                sameCatAllEntries = (ArrayList<? extends EntryBinding>) new EntryDao(EntryDetailsActivity.this).getAllEntry(selectedCatId);
                if (sameCatAllEntries != null && sameCatAllEntries.size() > 0){
                    EntryCatRenderer.getInstance().add(selectedCatId, (List<EntryBinding>) sameCatAllEntries);
                }else{
                    notFound = true;

                }

            }
            return "Executed";
        }

        @Override
        protected void onPostExecute(String result) {

            if (notFound){
                // this cat entries not found in local database , download in server from diff thread, same time render it
                sameCatAllEntries = new ArrayList<>();
                ArrayList<EntryBinding> temp = new ArrayList<>();
                temp.add(entryBinding);
                sameCatAllEntries = (ArrayList<? extends EntryBinding>)temp;
                EntryCatRenderer.getInstance().add(selectedCatId, (List<EntryBinding>) sameCatAllEntries);
                new DownloadEntries(selectedCatId).execute(selectedCatId+"");
            }

            setViewPager(sameCatAllEntries, entryBinding);

        }

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected void onProgressUpdate(Void... values) {}
    }





    @Override
    public void onBackPressed() {
        Print.e(this, "onBackPressed");
        if (Build.VERSION.SDK_INT >= CommonConstants.MIN_SDK_VERSION_FOR_SEARCH_VEIW) {
            if (searchViewManager.getSearchView().isOpen()) {
                // Close the search on the back button press.
                searchViewManager.getSearchView().closeSearch();
            } else { // activity is closing
                setResultForReturn(false, selectedCatId);
                super.onBackPressed();
            }
        }else{ // activity is closing
            setResultForReturn(false, selectedCatId);
            super.onBackPressed();
        }

    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (Build.VERSION.SDK_INT >= CommonConstants.MIN_SDK_VERSION_FOR_SEARCH_VEIW) {
            if (requestCode == MaterialSearchView.REQUEST_VOICE && resultCode == RESULT_OK) {
                ArrayList<String> matches = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                if (matches != null && matches.size() > 0) {
                    String searchWrd = matches.get(0);
                    if (!TextUtils.isEmpty(searchWrd)) {
                        searchViewManager.getSearchView().setQuery(searchWrd, false);
                    }
                }

                return;
            }
        }

        switch(requestCode) {
            case 65: { // request code 65 sent by this activity to open again when user click for open details of  releted items
                // if user click on cat text view under the entry title then 65 return back to activity
                // if 65 return back then we will close all details activity and open Entry list fragment with selected category
                Print.e(this, "75: request code: " + requestCode);
                Print.e(this, "result code: " + resultCode);
                if (resultCode == Activity.RESULT_OK) { // testing if result is ok
                    Print.e(this, "Result Ok: selectedCatId: " + selectedCatId);
                    boolean recursiveKill = data.getBooleanExtra(EXTRA_RECURSIVE_KILL, false);
                    if (recursiveKill){
                        selectedCatId = data.getLongExtra(EntryDetailsActivity.EXTRA_ENTRY_CAT, -1); // pulling cat id of the entry we pass to entry details activity
                        setResultForReturn(recursiveKill, selectedCatId);
                        finish();
                    }

                }
                break;
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void finish() {
       /* if (cats != null && cats.size() > 0){
            Intent resultIntent = new Intent();
            resultIntent.putExtra(EXTRA_ENTRY_CAT, cats.get(0));
            setResult(RESULT_OK, resultIntent);
           // finish();
        }
        Print.e(this, "finish is called");*/
       // setResult(RESULT_OK, selectedCatId);
       // setResultForReturn();
        super.finish();
    }



    public void setResultForReturn(boolean recursiveKill, long selectedCatId){ // returing result to Main activity, we are sending latest selected catId
        //drawer will open based on latest cat id
        Intent resultIntent = new Intent();
        resultIntent.putExtra(EXTRA_RECURSIVE_KILL, recursiveKill);
        resultIntent.putExtra(EXTRA_ENTRY_CAT, selectedCatId);
        setResult(RESULT_OK, resultIntent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_details_activity, menu);
        return true;
    }
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        if (Build.VERSION.SDK_INT < CommonConstants.MIN_SDK_VERSION_FOR_SEARCH_VEIW) {
            menu.findItem(R.id.action_search).setVisible(false);
        }
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle toolbar item clicks here. It'll
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        int id = item.getItemId();
        switch (id) {
            case R.id.action_search:
                if (Build.VERSION.SDK_INT >= CommonConstants.MIN_SDK_VERSION_FOR_SEARCH_VEIW) {
                    // Open the search view on the menu item click.
                    searchViewManager.getSearchView().openSearch();
                    SoftKeyboard.OpenSoftKeyboard(this);
                }
                return true;
            case android.R.id.home: // this case tool bar backbutton
                setResultForReturn(false, selectedCatId);
                finish();
                return true;
            case R.id.action_text_size:
                new TextSize(this, this).show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    @Override
    public void onClick(SearchItem searchItem) {  // called when user click on an item from serch dropdown menu
        if (searchItem != null){
            EntryDao entryDao = new EntryDao(EntryDetailsActivity.this);
            long entryId = searchItem.getId();
            EntryBinding entryBinding = entryDao.getEntryBinding(entryId);
            Long catId = entryBinding.getEntry().getPrimaryCatId();
            if (catId.equals(selectedCatId)){ // check whether user searched item is same category is displaying now
                setViewPager(sameCatAllEntries, entryBinding);
            }else{
                selectedCatId = catId;
                ArrayList<? extends EntryBinding> sameCatAllEntries1 = (ArrayList<? extends EntryBinding>) entryDao.getAllEntry(entryBinding.getEntry().getPrimaryCatId());
                setViewPager(sameCatAllEntries1, entryBinding);
            }
        }
    }

    @Override
    public void onOpenEntryListner(EntryBinding selectedEntry) { // call from General Recycle View Adapter when user click on a item
        //processData(entryBinding, entryBinding.getEntry().getId()); //TODO call fragment to load item
    }

    private void setViewPager(ArrayList<? extends EntryBinding> sameCatAllEntries, EntryBinding entryBinding){
        int curentItemPos = 0;
        for (int i = 0; i < sameCatAllEntries.size(); i++){
            if (sameCatAllEntries.get(i).getEntry().getId().equals(entryBinding.getEntry().getId())){
                curentItemPos = i;
                break;
            }
        }
        mPagerAdapter.setSameCatAllEntries(sameCatAllEntries);
        mPagerAdapter.setFirstOpenItemPosition(curentItemPos);
        mPagerAdapter.notifyDataSetChanged();
        mPager.setCurrentItem(curentItemPos);
       /* if (sameCatAllEntries.size() > 3){
            mPager.setOffscreenPageLimit(3);
        }else{
            mPager.setOffscreenPageLimit(sameCatAllEntries.size());
        }*/
        if (sameCatAllEntries.size() > 0){
            mPager.setOffscreenPageLimit(1);
        }


    }

    int oldScrollY = 0;
    boolean animating = false;
    int paddingAmountChanged = 0;

    int paddingTracker = 0;
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR1)
    public void setScrollState(ScrollState scrollState){
        this.scrollState = scrollState;
        if (scrollState == ScrollState.DOWN && upwardAnimationState){
            // since toobar is going outside off screen
            //user start to scroll down we should stop the animation and return back the toolbar on old position
            mToolbar.clearAnimation();
            animateDownward(mToolbar);
            mToolbar.clearAnimation();
        }

    }
    boolean upwardAnimationState = false;
    boolean downwardAnimationState = false;

    public void onScrollChanged(int currentScrollY){

       // Print.e(this, "yPosAppBar: "+ yPosAppbar);
       // Print.e(this, "currentScrollY: "+ currentScrollY + " toolbar down pos: " + (yPosAppbar+toolbarHeight));

        if (currentScrollY < (pagerBasePadding) && oldScrollY > currentScrollY && !animating){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR1) {
                animateUpward(mToolbar);
            }else{ // toolbar must stay on top

            }
        }else if ((oldScrollY < currentScrollY && !animating)){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR1) {
                animateDownward(mToolbar);
            }else{ // toolbar must stay on top

            }
        }
        Print.e(this, "oldY: "+oldScrollY+ " curY: "+ currentScrollY +" amTrns: " + paddingAmountChanged);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {

            if (oldScrollY - currentScrollY > 0 && scrollState == ScrollState.UP){ // last position is greater then new position pager is scrolling up
                if (pagerCurrentPadding > 0){ // ignore moving pager if it's reached to visiable screen top y p(x,0)
                    int pagerNewPadding = pagerCurrentPadding - scrollSpeed;
                    int distance = scrollSpeed;
                    if (pagerNewPadding < 0){
                        pagerNewPadding = 0;
                        distance = pagerCurrentPadding;
                    }

                    mPager.setPadding(0,pagerNewPadding,0, 0);
                    pagerCurrentPadding = pagerNewPadding;
                    paddingAmountChanged +=distance;
                    Print.e(this, "distance++: "+ distance);
                    Print.e(this, "ScrollState.UP: pagerCurrentPadding: "+ pagerCurrentPadding + " Pading Amonut changed: "+ paddingAmountChanged);

                }
            }else if (scrollState == ScrollState.DOWN){ // last position is smaller then new position pager is moving downward
                if (paddingAmountChanged > 0){ // since user scroll up, we shift the pager upwards total amount is paddingAmountChanged
                    int pagerNewPadding = pagerCurrentPadding + scrollSpeed;
                    int distance = scrollSpeed;
                    paddingAmountChanged -=distance;
                    if (paddingAmountChanged <= 0){
                        paddingAmountChanged = 0;
                        pagerNewPadding = pagerBasePadding;
                        //adjustedPosition = pagerBasePadding;
                    }
                    //pagerContainer.setY(pagerNewPos);
                    mPager.setPadding(0,pagerNewPadding,0, 0);
                    pagerCurrentPadding = pagerNewPadding;

                    Print.e(this, "ScrollState.DOWN: pagerCurrentPadding: "+ pagerCurrentPadding + " Pading Amonut changed: "+ paddingAmountChanged);

                }
            }

        }

        oldScrollY = currentScrollY;

    }

    long oldT;
    long curT;
    int oldV;
    int curV;
    public void onFling(int velocityY) {
        scrollViewVelocityY = velocityY;
        curT = System.currentTimeMillis();
        curV = velocityY;
        int velocity = Math.abs(oldV - curV);
        long timeDiff = curT - oldT;

        scrollSpeed = (Math.abs((int)(velocity/timeDiff)) + 3)*scrollSpeedMul;

        Print.e(this, "scrollSpeed: "+ scrollSpeed);

        oldT = curT;
        oldV = curV;

    }

    public void setScrollSpeedMul(boolean isDetailsHeaderVisible){
        if (isDetailsHeaderVisible){
            scrollSpeedMul = 1;
        }else{
            scrollSpeedMul = 10000;
        }
    }
    @RequiresApi(api = Build.VERSION_CODES.HONEYCOMB_MR1)
    private void animateUpward(final View v){
        mToolbar.animate()
                .translationY(-toolbarHeight*2)
                .alpha(1).setDuration(toolbarAnimDuration)
                .setInterpolator(new DecelerateInterpolator())
                .setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animator) {
                        animating = true;
                        upwardAnimationState = true;
                        downwardAnimationState = false;
                    }

                    @Override
                    public void onAnimationEnd(Animator animator) {
                        animating = false;

                       /* if (lastScrollY < oldScrollY){
                            oldScrollY = lastScrollY;
                            animateDownward(v, lastScrollY);
                        }*/
                    }

                    @Override
                    public void onAnimationCancel(Animator animator) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animator) {

                    }
                });
    }

    @RequiresApi(api = Build.VERSION_CODES.HONEYCOMB_MR1)
    private void animateDownward(View v){
        mToolbar.animate()
                .translationY(0)
                .alpha(1).setDuration(toolbarAnimDuration)
                .setInterpolator(new DecelerateInterpolator())
                .setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animator) {
                        animating = true;
                    }

                    @Override
                    public void onAnimationEnd(Animator animator) {
                        animating = false;
                        upwardAnimationState = false;
                        downwardAnimationState = true;
                    }

                    @Override
                    public void onAnimationCancel(Animator animator) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animator) {

                    }
                });
    }

    private class DownloadEntries extends AsyncTask<String, Integer, String> {
        private long lastDownloadedTime;
        long catId;
        CategoryDao categoryDao;

        public DownloadEntries(long catId) {
            this.catId = catId;
            categoryDao = new CategoryDao(EntryDetailsActivity.this);
            String timeStamp =categoryDao.getLastDownloaded(catId);
            this.lastDownloadedTime = timeStamp!=null?  Long.valueOf(timeStamp): 0;
        }

        @Override
        protected String doInBackground(final String... params) {
            // if ( entryDao.count(catId) <= 0){
            EntryGetAPICall.getInstance().callAPI(EntryDetailsActivity.this, null, new APIClientResponse() {
                @Override
                public void failureOnApiCall(String msg, Object sender) {


                }

                @Override
                public void failureOnNetworkConnection(String msg, Object sender) {

                }

                @Override
                public void successOnApiCall(String msg, Object sender) {
                    long systemTime = System.currentTimeMillis();
                    Print.e(this, "systemTime: "+ systemTime);
                    if (systemTime > lastDownloadedTime) {
                        lastDownloadedTime = systemTime;
                    }
                    Print.e(this, "Download complete");
                    categoryDao.updateLastDownloaded(catId, lastDownloadedTime+"");
                    categoryDao.setCatState(catId, CategoryDao.ON_START_DOWNLOAD_COMPLETE);
                    onRenderView();
                }

                private void onRenderView() {
                    sameCatAllEntries = (ArrayList<? extends EntryBinding>) new EntryDao(EntryDetailsActivity.this).getAllEntry(selectedCatId);
                    EntryDetailsActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            setViewPager(sameCatAllEntries, entryBinding);
                        }
                    });
                }
            }, params[0], lastDownloadedTime + "", null, null, null, null);

            return params[0];
        }
    }

    public class PageChangeListener extends ViewPager.SimpleOnPageChangeListener {

        private int currentPage;

        @Override
        public void onPageSelected(int position) {
            currentPage = position;
            if (!activityJustOpened){ // stop requesting for ad instantly when activity just opned
                show();
                Print.e(this, "Showing inter ad");

            }else{
                activityJustOpened=false;
                Print.e(this, "activity just opned");
            }


        }

        public final int getCurrentPage() {
            return currentPage;
        }
    }
}
