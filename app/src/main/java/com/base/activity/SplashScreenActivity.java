/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.base.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.view.View;
import android.widget.ProgressBar;

import com.base.common.CommonSettings;
import com.softbondit.news.karu.R;
import com.retrofit.api.APIClientResponse;
import com.retrofit.handler.CategoryGetAPICall;
import com.retrofit.handler.StatusGetAPICall;

public class SplashScreenActivity extends AppCompatActivity {

    public static final String EXTRA_NAME = "cheese_name";
    ProgressBar progressBar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);

      callToServer();

    }

    private void callToServer(){
        progressBar.setVisibility(View.VISIBLE);
        if (CommonSettings.getInstance(this).getAppFirstTimeRuning()){
            CategoryGetAPICall.getInstance().callAPI(this, null, new APIClientResponse() {
                @Override
                public void failureOnApiCall(String msg, Object sender) {
                    onFailureApiCall();
                }

                @Override
                public void failureOnNetworkConnection(String msg, Object sender) {
                    onFailureApiCall();
                }

                @Override
                public void successOnApiCall(String msg, Object sender) {
                    CommonSettings.getInstance(SplashScreenActivity.this).setAppFirstTimeRuning(false);
                    stopProgressbar();
                    goToMainActivity();
                }
                private void onFailureApiCall(){
                    noInternetConnection();
                    stopProgressbar();
                }
            }, null, null, null, null);
        }else{
            StatusGetAPICall.getInstance().callAPI(this, null, new APIClientResponse() {
                @Override
                public void failureOnApiCall(String msg, Object sender) {
                    stopProgressbar();
                    goToMainActivity();
                }

                @Override
                public void failureOnNetworkConnection(String msg, Object sender) {
                    stopProgressbar();
                    goToMainActivity();
                }

                @Override
                public void successOnApiCall(String msg, Object sender) {
                    stopProgressbar();
                    goToMainActivity();
                }
            });
        }
    }

    void stopProgressbar(){
        if (progressBar != null){
            progressBar.setVisibility(View.GONE);
        }
    }

    private void goToMainActivity(){
        Intent i = new Intent(this, MainActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
        finish();
    }

    private void noInternetConnection() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.MyDialogTheme));
        builder.setTitle(getString(R.string.internet_connection));
        builder.setMessage(getString(R.string.check_your_internet_connection));

        String positiveText = getString(R.string.retry);
        builder.setPositiveButton(positiveText,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // positive button logic
                        callToServer();
                       dialog.dismiss();
                    }
                });

        String negativeText = getString(R.string.exit);
        builder.setNegativeButton(negativeText,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // negative button logic
                        SplashScreenActivity.this.finish();
                    }
                });

        AlertDialog dialog = builder.create();
        // display dialog_window
        dialog.show();
    }
}
