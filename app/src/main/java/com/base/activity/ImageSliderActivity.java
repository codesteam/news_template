package com.base.activity;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.Toast;

import com.base.common.CommonAplication;
import com.base.common.CommonConstants;
import com.daimajia.slider.library.Indicators.PagerIndicator;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.softbondit.news.karu.R;

import java.util.ArrayList;


public class ImageSliderActivity extends ActionBarActivity implements BaseSliderView.OnSliderClickListener, ViewPagerEx.OnPageChangeListener{

    public static final String EXTRA_IMAGE_REF = "image_refs";
    public static final String EXTRA_IMAGE_SHORT_DESC_REF = "image_short_desc_refs";
    private SliderLayout mDemoSlider;
    private ActionBar actionBar;
    private int mContentSize;
    private String of = " of ";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_slider);
        mDemoSlider = (SliderLayout)findViewById(R.id.slider);

        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setIcon(R.drawable.ic_action_grid);

        Bundle bundle = getIntent().getExtras();
        ArrayList<String> imageUrls = bundle.getStringArrayList(EXTRA_IMAGE_REF);
        ArrayList<String> imageShortDesc = bundle.getStringArrayList(EXTRA_IMAGE_SHORT_DESC_REF);

        //HashMap<String,String> url_maps = new HashMap<String, String>();
        if (imageUrls != null){
            for (int i = 0; i < imageUrls.size(); i++){
                String shortDesc = imageShortDesc.get(i);
                if (shortDesc.equals(CommonConstants.NOT_FOUND)){
                    shortDesc = "";
                }
                TextSliderView textSliderView = new TextSliderView(CommonAplication.getAppContext());
                // initialize a SliderLayout
                textSliderView
                        .description(shortDesc)
                        .image(imageUrls.get(i))
                        .setScaleType(BaseSliderView.ScaleType.Fit)
                        .setOnSliderClickListener(this);

                //add your extra information
                textSliderView.bundle(new Bundle());
                textSliderView.getBundle()
                        .putString("extra",shortDesc);
                mDemoSlider.addSlider(textSliderView);

            }
            mContentSize = imageUrls.size();
            actionBar.setTitle("1"+of+imageUrls.size());
        }

        mDemoSlider.setIndicatorVisibility(PagerIndicator.IndicatorVisibility.Invisible);
        mDemoSlider.stopAutoCycle();
       // mDemoSlider.setPresetTransformer(SliderLayout.Transformer.Accordion);
       // mDemoSlider.setCustomAnimation(new DescriptionAnimation());
     //   mDemoSlider.setDuration(4000);
        mDemoSlider.addOnPageChangeListener(this);



    }

    @Override
    protected void onStop() {
        // To prevent a memory leak on rotation, make sure to call stopAutoCycle() on the slider before activity or fragment is destroyed
        mDemoSlider.stopAutoCycle();
        super.onStop();
    }

    @Override
    public void onSliderClick(BaseSliderView slider) {
        Toast.makeText(this,slider.getBundle().get("extra") + "",Toast.LENGTH_SHORT).show();
    }

  /*  @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.main,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_custom_indicator:
                mDemoSlider.setCustomIndicator((PagerIndicator) findViewById(R.id.custom_indicator));
                break;
            case R.id.action_custom_child_animation:
                mDemoSlider.setCustomAnimation(new ChildAnimationExample());
                break;
            case R.id.action_restore_default:
                mDemoSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
                mDemoSlider.setCustomAnimation(new DescriptionAnimation());
                break;
            case R.id.action_github:
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://github.com/daimajia/AndroidImageSlider"));
                startActivity(browserIntent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }*/

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}

    @Override
    public void onPageSelected(int position) {
        Log.d("Slider Demo", "Page Changed: " + position);
        actionBar.setTitle((position+1)+of+mContentSize);
    }

    @Override
    public void onPageScrollStateChanged(int state) {}
}
