/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.base.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Pair;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.base.common.CommonAplication;
import com.base.common.CommonConstants;
import com.base.common.StaticFun;
import com.base.components.StatusReceiver;
import com.base.factory.OnOpenEntry;
import com.base.factory.RenderManager;
import com.base.factory.Renderer;
import com.base.fragments.FragSubGroupList;
import com.base.modarate.view.IAdView;
import com.base.modarate.view.IZixing;
import com.base.modarate.view.Modarate;
import com.base.modarate.view.ZixisngSettings;
import com.base.sm.adapter.ExpandableListAdapter;
import com.base.sm.drawer.DrawerActionListener;
import com.base.sm.drawer.DrawerCloseListener;
import com.base.sm.drawer.RecycleViewItem;
import com.base.utility.ConnectionDetector;
import com.base.utility.SoftKeyboard;
import com.commonasset.utils.Print;
import com.db.DBHelper;
import com.db.dao.CategoryDao;
import com.db.dao.EntryDao;
import com.db.helper.EntryBinding;
import com.softbondit.news.karu.R;
import com.softbondit.news.karu.BuildConfig;
import com.materialsearchview.MaterialSearchView;
import com.materialsearchview.OnSearchItemClick;
import com.materialsearchview.SearchViewManager;
import com.materialsearchview.db.SearchItem;

import java.util.ArrayList;

/**
 * TODO
 */
public class MainActivity extends AppCompatActivity implements IAdView, IZixing, DrawerActionListener, OnSearchItemClick {


    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle drawerToggle;
    RecyclerView mRecyclerView;                           // Declaring RecyclerView
    RecyclerView.LayoutManager mLayoutManager;
    DBHelper dbHelper;
    Toolbar toolbar;
    DrawerCloseListener drawerCloseListener;
    RecycleViewItem selectedDrawerItem;
    //private MaterialSearchView searchView;
    private  CategoryDao categoryDao;
    LinearLayout searchViewContainer;
    RenderManager renderManager;
    ExpandableListAdapter expandableListAdapter;
    SearchViewManager searchViewManager;
    FragSubGroupList fragSubGroupList;
    private Modarate modarate;

    public static boolean forwardTransction = true;
    public void setSelectedDrawerItem(RecycleViewItem selectedDrawerItem) {
        this.selectedDrawerItem = selectedDrawerItem;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        dbHelper = new DBHelper(this);
        categoryDao = new CategoryDao(this);

        renderManager = RenderManager.getInstance(this);
        categoryDao.resetCatState();

        drawerCloseListener = new DrawerCloseListener(this);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setBackgroundColor(getResources().getColor(R.color.bg2));
        final ActionBar ab = getSupportActionBar();
        ab.setHomeAsUpIndicator(R.drawable.ic_menu);
        ab.setDisplayHomeAsUpEnabled(true);
        modarate = ZixisngSettings.getInstance(this).createViewFliper(this,this, getString(R.string.ad_banner_unit_id),
                getString(R.string.ad_inter_unit_id), CommonConstants.AD_WAIT_IN_SEC, BuildConfig.system_droid );
        mRecyclerView = (RecyclerView) findViewById(R.id.RecyclerView); // Assigning the RecyclerView Object to the xml View
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawerToggle = setupDrawerToggle(toolbar);
        mDrawerLayout.addDrawerListener(drawerToggle);
        mDrawerLayout.setScrimColor(getResources().getColor(android.R.color.transparent));
        //searchView = (MaterialSearchView) findViewById(R.id.search_view);
        expandableListAdapter = new ExpandableListAdapter(drawerCloseListener, this);
        searchViewContainer = (LinearLayout) findViewById(R.id.searchViewContainer);
        if (Build.VERSION.SDK_INT >= CommonConstants.MIN_SDK_VERSION_FOR_SEARCH_VEIW) {
            searchViewManager = SearchViewManager.getInstance(this);
            searchViewManager.create();
            searchViewManager.addParrent((ViewGroup) findViewById(R.id.searchViewContainer));
        }else{
            searchViewContainer.setVisibility(View.GONE);
        }
        StatusReceiver.setupAlarm(CommonAplication.getAppContext());

        setBViewFliper();

    }

    @Override
    protected void onResume() {
        super.onResume();
        renderManager.addRenderer(CategoryDao.RENDERER_KEY, new Pair<Renderer, String>(expandableListAdapter, null));
        if (Build.VERSION.SDK_INT >= CommonConstants.MIN_SDK_VERSION_FOR_SEARCH_VEIW) {
            renderManager.addRenderer(MaterialSearchView.RENDERER_KEY, new Pair<Renderer, String>(searchViewManager.getSearchView(), null));
            searchViewManager.addParrent(searchViewContainer);
            searchViewManager.setSearchItemClickListner(this);
            searchViewManager.getSearchView().activityResumed();

        }
        if (expandableListAdapter.getItemCount() == 0){
            setDrawer();
        }

        modarate.resume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        renderManager.removeRenderer(CategoryDao.RENDERER_KEY);
        if (Build.VERSION.SDK_INT >= CommonConstants.MIN_SDK_VERSION_FOR_SEARCH_VEIW) {
            renderManager.removeRenderer(MaterialSearchView.RENDERER_KEY);
            searchViewManager.removeParrent();
        }

        modarate.pause();
        Print.e(this, "onPause");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (Build.VERSION.SDK_INT >= CommonConstants.MIN_SDK_VERSION_FOR_SEARCH_VEIW) {
            //searchView.clearSuggestions();
            searchViewManager.getSearchView().clearSuggestions();
        }
        modarate.onDestroy();
        Print.e(this, "onDestroy");
    }

    @Override
    public void setBViewFliping() {
        setBViewFliper();
    }

    public void setBViewFliper() {
        if (ConnectionDetector.isNetworkPresent(this)) {
            modarate.setBViewFliper((LinearLayout) findViewById(R.id.llAdView));
            //setBFliperVisibility(View.VISIBLE);
        } else {
            setBFliperVisibility(View.GONE);
        }

    }

    public void setBFliperVisibility(int v) {
        modarate.setBFliperVisibility(v);
    }
    @Override
    public void show() {
        // TODO Auto-generated method stub
        Print.e(this, "Show In add");
        if (ConnectionDetector.isNetworkPresent(this)) {
            modarate.show();

        }

    }
    @Override
    public void finish() {
        Print.e(this, "finish");
        super.finish();
    }

    @Override
    public void onBackPressed() {
        if (Build.VERSION.SDK_INT >= CommonConstants.MIN_SDK_VERSION_FOR_SEARCH_VEIW) {
            if (searchViewManager.getSearchView().isOpen()) {
                // Close the search on the back button press.
                searchViewManager.getSearchView().closeSearch();
            } else {
                super.onBackPressed();
            }
        }else{
            super.onBackPressed();
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (Build.VERSION.SDK_INT >= CommonConstants.MIN_SDK_VERSION_FOR_SEARCH_VEIW) {
            if (requestCode == MaterialSearchView.REQUEST_VOICE && resultCode == RESULT_OK) {
                ArrayList<String> matches = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                if (matches != null && matches.size() > 0) {
                    String searchWrd = matches.get(0);
                    if (!TextUtils.isEmpty(searchWrd)) {
                        searchViewManager.getSearchView().setQuery(searchWrd, false);
                    }
                }

                return;
            }
        }

        Print.e(this, "onActivityResult: request code: "+ requestCode);
        switch(requestCode) {
            case 65: { // request code 65 sent to Entry Details activity when we open that
                Print.e(this, "65: request code: "+ requestCode);
                Print.e(this, "result code: "+ resultCode);
                if (resultCode == Activity.RESULT_OK) { // testing if result is ok
                    long cat = data.getLongExtra(EntryDetailsActivity.EXTRA_ENTRY_CAT, -1); // pulling cat id of the entry we pass to entry details activity
                    Print.e(this, "Result Ok: selectedCatId: "+ cat);
                    if (cat != -1){ // if it is a valid cat id
                        if (selectedDrawerItem != null){
                            if (selectedDrawerItem.getCatId() == cat){ // now we are testing entry that we passed to Entrydetails activity and the catgory that opned in SubgroupListFragment
                                //TODO: matched do what we need
                                Print.e(this, "onActivityResult: cat matched"+ cat);
                            }else if(selectedDrawerItem.getCatId() == CommonConstants.DEFAULT_CAT_HOME_ID){
                                //TODO: user open news from home
                            }
                            else{ // means user click on result of diffrent category that are showing on SubgroupListFragment. (e.g on SubgroupListFragment cat 7 items are showing but user click on cat 8 entry from Search view list)
                                selectedDrawerItem = expandableListAdapter.getItemByCatId(cat);
                                Print.e(this, "onActivityResult: cat did'nt matched "+ cat);
                                if (selectedDrawerItem != null){
                                    Print.e(this, "Reseting selection");
                                    expandableListAdapter.resetSelection();
                                    selectedDrawerItem.setSelected(true);
                                    expandableListAdapter.notifyDataSetChanged();
                                }
                                onNewSubGroup(selectedDrawerItem);
                            }
                        }else{ // there is not Subgroup list fragment opened yet, user click the push notification back then aplication was clossed

                        }

                    }
                }
                break;
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }



    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Print.e(this, "onNewIntent");
        Bundle extras = intent.getExtras();
        if(extras != null){ // main activity open/intent message dispatched by notifier
            if(extras.containsKey(EntryDao.ID)) { // main activity bundle has entry id to bypass the detealis activity directly
                Print.e(this, "onNewIntent  Contaings Key: "+ EntryDao.ID);
                long id = extras.getLong(EntryDao.ID);
                EntryDao entryDao = new EntryDao(MainActivity.this);
                EntryBinding entryBinding = entryDao.getEntryBinding(id);
                Long catId = entryBinding.getEntry().getPrimaryCatId();
                if (catId != null){ // all entries of this category already downloaded
                    StaticFun.OPEN_DETAILS_ACTIVITY( entryBinding, MainActivity.this, CommonConstants.REQUEST_CODE_FOR_DETAILS_ACTIVITY);
                }else{ // rest of entries of this entry category not downloaded/
                    // TODO download all entries of this entry's category
                    Print.e(this, "TODO download all entries of this entry's category ");
                }

                //onFragmentTrans(entryBinding);
                //gotoDetailsActivity(entryBinding);
            }else{ // default transation
               // onFragmentTrans(null);
            }
        }
    }



    private void setDrawer(){

        //Pair<ArrayList<Category>,  HashMap<Long, ArrayList<Category>>> pair  = categoryDao.getCategories();

        if (Build.VERSION.SDK_INT >= CommonConstants.MIN_SDK_VERSION_FOR_SEARCH_VEIW) {
            expandableListAdapter.addItem(new RecycleViewItem(getResources().getString(R.string.search_for_topics), CommonConstants.DEFAULT_CAT_HOME_ID, ExpandableListAdapter.SEARCH_VIEW, 1));
        }
        //expandableListAdapter.addItem(new RecycleViewItem(getResources().getString(R.string.home), CommonConstants.DEFAULT_CAT_HOME_ID, ExpandableListAdapter.CAT_ONLY, 1));

        expandableListAdapter.addCategories(categoryDao.getCategories());

        mRecyclerView.setAdapter(expandableListAdapter);                              // Setting the adapter to RecyclerView
        mLayoutManager = new LinearLayoutManager(this);                 // Creating a layout Manager
        mRecyclerView.setLayoutManager(mLayoutManager);
        //mDrawerLayout.openDrawer(mRecyclerView);
        mDrawerLayout.addDrawerListener(drawerCloseListener);
        if (Build.VERSION.SDK_INT >= CommonConstants.MIN_SDK_VERSION_FOR_SEARCH_VEIW) {
            selectedDrawerItem = expandableListAdapter.getItemAtPostion(1);

        }else{
            selectedDrawerItem = expandableListAdapter.getItemAtPostion(0);
        }
        selectedDrawerItem.setSelected(true);
        onNewSubGroup(selectedDrawerItem);


    }
    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main_activity, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        if (Build.VERSION.SDK_INT < CommonConstants.MIN_SDK_VERSION_FOR_SEARCH_VEIW) {
            menu.findItem(R.id.action_search).setVisible(false);
        }
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle toolbar item clicks here. It'll
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        int id = item.getItemId();
        switch (id) {
            case R.id.action_search:
                if (Build.VERSION.SDK_INT >= CommonConstants.MIN_SDK_VERSION_FOR_SEARCH_VEIW) {
                    // Open the search view on the menu item click.
                    Print.e(this, "Open Search View");
                    //searchView.openSearch();
                    searchViewManager.getSearchView().openSearch();
                    SoftKeyboard.OpenSoftKeyboard(this);
                }
                return true;
        }



        return super.onOptionsItemSelected(item);
    }


    private void setupDrawerContent(NavigationView navigationView) {
        final Menu menu = navigationView.getMenu();
       /* for (int i = 1; i <= 3; i++) {
            menu.add("Runtime item "+ i);
        }

        final SubMenu subMenu = menu.addSubMenu("SubMenu Title");
        for (int i = 1; i <= 2; i++) {
            subMenu.add("SubMenu Item " + i);
        }*/
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                menuItem.setChecked(true);
                mDrawerLayout.closeDrawers();
                return true;
            }
        });
    }



    private ActionBarDrawerToggle setupDrawerToggle(Toolbar toolbar) {

        // NOTE: Make sure you pass in a valid toolbar reference.  ActionBarDrawToggle() does not require it

        // and will not render the hamburger icon without it.

        return new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.drawer_open,  R.string.drawer_close);

    }

    @Override

    protected void onPostCreate(Bundle savedInstanceState) {

        super.onPostCreate(savedInstanceState);

        // Sync the toggle state after onRestoreInstanceState has occurred.

        drawerToggle.syncState();

    }



    @Override

    public void onConfigurationChanged(Configuration newConfig) {

        super.onConfigurationChanged(newConfig);

        // Pass any configuration change to the drawer toggles

        drawerToggle.onConfigurationChanged(newConfig);

    }


    @Override
    public void OnDrawerItemClickListener() {
        Print.e(this, "OnDrawerItemClickListener itemId: "+ selectedDrawerItem.getCatId());
        mDrawerLayout.closeDrawers();
    }

    @Override
    public void OnDrawerClosedListener() {
        Print.e(this, "OnDrawerClosedListener itemId: "+ selectedDrawerItem.getCatId());
        onNewSubGroup(selectedDrawerItem);
    }

    public void onNewSubGroup(final RecycleViewItem selectedDrawerItem){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Bundle bundle = new Bundle();
                 fragSubGroupList = new FragSubGroupList();

                toolbar.setTitle(selectedDrawerItem.getTitle()+"");
                bundle.putLong(FragSubGroupList.EXTRA_CAT_ID, selectedDrawerItem.getCatId());
                fragSubGroupList.setArguments(bundle);
                FragmentManager fragmentManager = MainActivity.this.getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                //fragmentTransaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
                fragmentTransaction.replace(R.id.container_body, fragSubGroupList, fragSubGroupList.getClass().getName());
                // fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commitAllowingStateLoss();
                fragmentManager.executePendingTransactions();

                Print.e(this, selectedDrawerItem.getTitle()+" cat id: "+selectedDrawerItem.getCatId());
            }
        });
    }
   /* private void onFragmentTranss(final EntryBinding entryBinding){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Bundle bundle = new Bundle();
                FragSubGroupList fragSubGroupList = new FragSubGroupList();
                if (entryBinding != null){ // from search view, from notification item click
                    Print.e(this, "Found EntryBinding");
                    bundle.putBoolean(FragSubGroupList.EXTRA_REQUEST_FOR_DETAILS_ACTIVITY, true);
                    bundle.putSerializable(EntryDetailsActivity.EXTRA_ENTRY_BINCING, entryBinding);
                    List<Long> cats = entryBinding.getEntry().getCategories();
                    if (cats!= null && cats.size()> 0){
                        selectedDrawerItem = expandableListAdapter.getItemByCatId(cats.get(0));
                        if (selectedDrawerItem != null){
                            Print.e(this, "Reseting selection");
                            expandableListAdapter.resetSelection();
                            selectedDrawerItem.setSelected(true);
                            expandableListAdapter.notifyDataSetChanged();
                        }//else entry dose not belongs to any category that drawer has
                    }else{ // garbage entry
                        selectedDrawerItem = null;
                        Print.e(this, "Garbage Entry");
                    }
                }else{
                    Print.e(this, "No EntryBinding");
                }
                if (selectedDrawerItem != null){
                    toolbar.setTitle(selectedDrawerItem.getTitle()+"");
                    bundle.putLong(FragSubGroupList.EXTRA_CAT_ID, selectedDrawerItem.getCatId());
                    fragSubGroupList.setArguments(bundle);
                    FragmentManager fragmentManager = MainActivity.this.getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    //fragmentTransaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
                    fragmentTransaction.replace(R.id.container_body, fragSubGroupList, fragSubGroupList.getClass().getName());
                   // fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.commit();
                    Print.e(this, selectedDrawerItem.getTitle()+" cat id: "+selectedDrawerItem.getCatId());

                }else{
                    Print.e(this, "Recycle view item found null on main activity");
                }
            }
        });

    }*/

    public void onDrawerSearchClicked(){
        mDrawerLayout.closeDrawers();
        //searchView.openSearch();
        searchViewManager.getSearchView().openSearch();
    }

    @Override
    public void onClick(SearchItem searchItem) {
        if (searchItem != null){
            EntryDao entryDao = new EntryDao(MainActivity.this);
            EntryBinding entryBinding = entryDao.getEntryBinding(searchItem.getId());
            Print.e(this, entryBinding.getEntry().getTitle()+"");
            //onFragmentTrans(entryBinding);
            Print.e(this, "Warning ...value should not be null or 0 of downloadedCatId: "+entryBinding.getEntry().getPrimaryCatId() );
            newEntryOpen(entryBinding);
        }
    }


    public void newEntryOpen(EntryBinding selectedEntry) {
        ((OnOpenEntry)fragSubGroupList).onOpenEntryListner(selectedEntry);
    }
}
