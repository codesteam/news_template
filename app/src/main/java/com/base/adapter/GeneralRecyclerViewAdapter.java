package com.base.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.base.common.CommonAplication;
import com.base.common.StaticFun;
import com.base.factory.OnOpenEntry;
import com.commonasset.utils.GlideImageRenderer;
import com.commonasset.utils.Print;
import com.commonasset.utils.TimestampConverter;
import com.db.dao.EntryDao;
import com.db.helper.EntryBinding;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubeThumbnailLoader;
import com.google.android.youtube.player.YouTubeThumbnailView;
import com.softbondit.news.karu.R;
import com.retrofit.response.entry.Entry;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by water on 6/19/17.
 */

public  class GeneralRecyclerViewAdapter
        extends RecyclerView.Adapter<GeneralRecyclerViewAdapter.ViewHolder> {

    private final TypedValue mTypedValue = new TypedValue();
    private int mBackground;
    public List<EntryBinding> mValues;
    Context context;
    OnOpenEntry onOpenEntry;
    public Long selectedCatId;

    public  class ViewHolder extends RecyclerView.ViewHolder {
        public Entry entry;

        public final View mView;
        public final ImageView generalImage;
        public final TextView generalTitle;
        public final TextView generalPostedTime;
        public final TextView youtubeDuration;
        public ViewHolder(View view) {
            super(view);
            mView = view;
            generalImage = (ImageView) view.findViewById(R.id.generalImage);
            generalTitle = (TextView) view.findViewById(R.id.generalTitle);
            generalPostedTime = (TextView) view.findViewById(R.id.generalPostedTime);
            youtubeDuration = (TextView) view.findViewById(R.id.tvDuration);

        }

        @Override
        public String toString() {
            return super.toString() + " '" + generalTitle.getText();
        }
    }

    public Entry getValueAt(int position) {
        return mValues.get(position).getEntry();
    }

    public GeneralRecyclerViewAdapter(Context context, OnOpenEntry onOpenEntry) {
        context.getTheme().resolveAttribute(R.attr.selectableItemBackground, mTypedValue, true);
        this.context = context;
        this.onOpenEntry = onOpenEntry;
        mBackground = mTypedValue.resourceId;
        mValues = new ArrayList<>();
    }



    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_row_general_item, parent, false);
        view.setBackgroundResource(mBackground);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.entry = mValues.get(position).getEntry();
        if (holder.entry == null){
            Print.e(this, "holder.entry null");
        }
        if (holder.generalTitle == null){
            Print.e(this, "holder.generalTitle null");
        }
        holder.generalTitle.setText(holder.entry.getTitle()+"");
        holder.generalPostedTime.setText(TimestampConverter.parseDate(holder.entry.getPubDate()+""));
        Print.e(this, "onBindViewHolder");

        String imageUrl = "";
        if (holder.entry.getMediaType() == EntryDao.TYPE_MEDIA_IMAGE){
            holder.youtubeDuration.setVisibility(View.GONE);
            if (mValues.get(position).getSmallImageMedia() != null && mValues.get(position).getSmallImageMedia().size() > 0){
                imageUrl = mValues.get(position).getSmallImageMedia().get(0);
            }

        }else{
            holder.youtubeDuration.setVisibility(View.VISIBLE);
            Print.e(this, "Video Url: "+ mValues.get(position).getVideoMedia().get(0));
            imageUrl = "https://img.youtube.com/vi/"+ StaticFun.getYoutubeIdFromUrl(mValues.get(position).getVideoMedia().get(0))+"/mqdefault.jpg";
            holder.youtubeDuration.setText(mValues.get(position).getVideoMediaDuration().get(0));
            //imageUrl = "https://img.youtube.com/vi/"+"SRcnnId15BA"+"/hqdefault.jpg";

        }
        GlideImageRenderer.renderThumbnail(CommonAplication.getAppContext(), holder.generalImage, imageUrl, null);
        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                   /* Context context = v.getContext();
                    Intent intent = new Intent(context, EntryDetailsActivity.class);
                    intent.putExtra(EntryDetailsActivity.EXTRA_TITLE, holder.entry.getTitle());

                    context.startActivity(intent);*/
                onOpenEntry.onOpenEntryListner(mValues.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        Print.e(this, "getItemCount: "+mValues.size() );
        return mValues.size();
    }

    private final class ThumbnailListener implements
            YouTubeThumbnailView.OnInitializedListener,
            YouTubeThumbnailLoader.OnThumbnailLoadedListener {

        @Override
        public void onInitializationSuccess(
                YouTubeThumbnailView view, YouTubeThumbnailLoader loader) {
            loader.setOnThumbnailLoadedListener(this);
            //view.setImageResource(R.drawable.loading_thumbnail);
            String videoId = (String) view.getTag();
            loader.setVideo(videoId);
        }

        @Override
        public void onInitializationFailure(
                YouTubeThumbnailView view, YouTubeInitializationResult loader) {
            // view.setImageResource(R.drawable.no_thumbnail);
        }

        @Override
        public void onThumbnailLoaded(YouTubeThumbnailView view, String videoId) {
        }

        @Override
        public void onThumbnailError(YouTubeThumbnailView view, YouTubeThumbnailLoader.ErrorReason errorReason) {
            //view.setImageResource(R.drawable.no_thumbnail);
        }
    }
}