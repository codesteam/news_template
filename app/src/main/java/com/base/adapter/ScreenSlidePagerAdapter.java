package com.base.adapter;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.ViewGroup;

import com.base.fragments.FragDetails;
import com.commonasset.utils.Print;
import com.db.helper.EntryBinding;

import java.util.ArrayList;

/**
 * Created by water on 7/13/17.
 */

public class ScreenSlidePagerAdapter extends FragmentPagerAdapter {

    private Fragment[] fragments;
    ArrayList<? extends EntryBinding> sameCatAllEntries = new ArrayList<>();
    int firstOpenItemPosition;

    public void setFirstOpenItemPosition(int firstOpenItemPosition) {
        this.firstOpenItemPosition = firstOpenItemPosition;
    }

    public ScreenSlidePagerAdapter(FragmentManager fm) {
        super(fm);
    }

    public void setSameCatAllEntries(ArrayList<? extends EntryBinding> sameCatAllEntries) {
        this.sameCatAllEntries = sameCatAllEntries;
        fragments = new Fragment[sameCatAllEntries.size()];
    }

    @Override
    public Fragment getItem(int position) {
        //if (fragments[position] == null){
            FragDetails fragDetails = new FragDetails();
            Bundle args = new Bundle();
            if (position == firstOpenItemPosition){
                args.putBoolean(FragDetails.KEY_IS_FIRST_ITEM, true);
            }else{
                args.putBoolean(FragDetails.KEY_IS_FIRST_ITEM, false);
            }
            fragDetails.setArguments(args);
            fragDetails.setEntryBinding(sameCatAllEntries.get(position));
            Print.e(this, "sameCatAllEntries: "+sameCatAllEntries.get(position).getEntry().getId()+"");
            fragments[position] = fragDetails;

       // }

        //fragDetails.setArguments(args);
        return  fragments[position];
    }

    public Fragment getFragmentAt(int index) {
        return fragments[index];
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    @Override
    public int getCount() {
        return sameCatAllEntries.size();
    }

    // this method will keeps all fragment in memory stack
    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
       // super.destroyItem(container, position, object);
    }
}


