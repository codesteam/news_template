package com.base.factory;

/**
 * Created by water on 1/6/17.
 */

public interface SingleRenderer<T> extends Renderer{
    void updateRenderer(T arg);
    void insertRenderer(T arg);
    void deleteRenderer(T arg);
}
