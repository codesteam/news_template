package com.base.factory;

import java.util.List;

/**
 * Created by water on 1/6/17.
 */

public interface ListRenderer<T> extends Renderer{
    void updateRenderer(List<T> arg);
    void insertRenderer(List<T> arg);
    void deleteRenderer(List<T> arg);
}
