package com.base.factory;

import com.db.helper.EntryBinding;

import java.util.HashMap;
import java.util.List;

/**
 * Created by water on 7/21/17.
 */

public class EntryCatRenderer {
    public static final EntryCatRenderer entryGroupRenderer = new EntryCatRenderer();
    HashMap<Integer, List<EntryBinding>> hashMap =new HashMap<>();
    CatRendererRequester catRendererRequester;

    public void setCatRendererRequester(CatRendererRequester catRendererRequester) {
        this.catRendererRequester = catRendererRequester;
    }

    public CatRendererRequester getCatRendererRequester() {
        return catRendererRequester;
    }

    public static EntryCatRenderer getInstance(){
        return entryGroupRenderer;
    }

    public void add(long catL, List<EntryBinding> entryBindings){
        int cat = (int)catL;
        hashMap.put(cat, entryBindings);
    }

    public void delete(int cat){
        hashMap.remove(cat);
    }

    public List<EntryBinding> get(long catL){
        int cat = (int)catL;
        return hashMap.get(cat);
    }

    public void clear(){
        hashMap.clear();
    }
}
