package com.base.factory;

import android.content.Context;
import android.graphics.Interpolator;
import android.util.AttributeSet;

import com.github.ksoichiro.android.observablescrollview.ObservableScrollView;

/**
 * Created by water on 7/19/17.
 */

public class FixedSpeedScroller extends ObservableScrollView {

    private int mDuration = 2000;

    public FixedSpeedScroller(Context context) {
        super(context);
    }

    public FixedSpeedScroller(Context context, Interpolator interpolator) {
        super(context, (AttributeSet) interpolator);
    }


    @Override
    public void setSmoothScrollingEnabled(boolean smoothScrollingEnabled) {
        super.setSmoothScrollingEnabled(smoothScrollingEnabled);
    }

    public void setFixedDuration(int duration) {
        this.mDuration = duration;
    }

    @Override
    public void fling(int velocityY) {
        super.fling(velocityY / 4);
    }
}