package com.base.factory;

/**
 * Created by steam on 8/1/17.
 */

public interface TextSizeChangeListner {
    void onTextSizeChange(int size);
    void onTextChangeApply();
}
