package com.base.factory;

import com.db.helper.EntryBinding;

/**
 * Created by water on 6/19/17.
 */

public interface OnOpenEntry {
    void onOpenEntryListner(EntryBinding selectedEntry);
}
