package com.base.factory;

import android.content.Context;
import android.util.Pair;

import java.util.HashMap;

/**
 * Created by water on 12/9/16.
 */
public class RenderManager {
    private static RenderManager renderManager;
    private static HashMap<String, Pair<Renderer, String>> mapRenderer;

    public static RenderManager getInstance(Context context){
        if (renderManager == null){
            mapRenderer = new HashMap<>();
            return renderManager = new RenderManager();
        }
        return  renderManager;
    }
    public void addRenderer(String key, Pair<Renderer, String> value){
        if (!mapRenderer.containsKey(key)){
            mapRenderer.put(key, value);
        }
    }
    public Pair<Renderer, String> getRenderer(String key){
        if (key == null){
            return  null;
        }
        return  mapRenderer.get(key);
    }

    public void removeRenderer(String key){
        if (mapRenderer.containsKey(key)){
            mapRenderer.remove(key);
        }

    }
}
