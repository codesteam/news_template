package com.base.fragments;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.base.activity.EntryDetailsActivity;
import com.base.activity.ImageSliderActivity;
import com.base.adapter.GeneralRecyclerViewAdapter;
import com.base.common.CommonAplication;
import com.base.common.CommonConstants;
import com.base.common.CommonSettings;
import com.base.common.StaticFun;
import com.base.factory.OnOpenEntry;
import com.base.utility.ConnectionDetector;
import com.base.utility.FragmentNested;
import com.commonasset.utils.GlideImageRenderer;
import com.commonasset.utils.ImageRenderListner;
import com.commonasset.utils.Print;
import com.commonasset.utils.TimestampConverter;
import com.db.dao.EntryDao;
import com.db.helper.EntryBinding;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollView;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollViewCallbacks;
import com.github.ksoichiro.android.observablescrollview.ScrollState;
import com.github.ksoichiro.android.observablescrollview.Scrollable;
import com.softbondit.news.karu.R;
import com.retrofit.api.APIClientResponse;
import com.retrofit.handler.EntryDetailsGetAPICall;
import com.retrofit.response.entry.details.EntryDetailsResponce;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by water on 7/13/17.
 */

public class FragDetails<S extends Scrollable> extends FragmentNested implements ObservableScrollViewCallbacks, OnOpenEntry {

    public static final String KEY_IS_FIRST_ITEM="is_first_item";
    ImageView featureImageView;
    TextView tvShortDescription;
    WebView wvDetails;
    GeneralRecyclerViewAdapter reletedItemsAdapter;
    RecyclerView reletedRV;
    ProgressBar progressBarMain;
    ProgressBar progressBarImageView;
    RelativeLayout detailsBody;
    RelativeLayout detailsHeader;
    LinearLayout noInternetConnection;
    LinearLayout serverError;
    Button retryNoInternetConnection;
    Button retryServerError;
    EntryBinding entryBinding;
    private SwipeRefreshLayout swipeContainer;
    //LinearLayout swipeContainerHolder;
    private S mScrollable;
    boolean rendered = false;
    TextView tvTime;
    TextView tvTitle;
    TextView tvCategoryName;
    CommonSettings commonSettings;
    LinearLayout bodyHeader;
    View viewHorizontal;

    float webViewScale = 1;
    public void setEntryBinding(EntryBinding entryBinding) {
        this.entryBinding = entryBinding;
    }

    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);


    }


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        Print.e(this, "isVisibleToUser"+isVisibleToUser);
        if (isVisibleToUser && tvShortDescription!=null && !rendered){
            // tvShortDescription is randomly selected for gurding not to call process data
            //since view pager is preloading all fragments
            rendered = true;
            processData(entryBinding, entryBinding.getEntry().getId());
        }
       if (commonSettings != null){ // this method call before the onCreate
           // should be here cause if all are rendered and then user change the text size,
           //only this function called
           setViewsTextSize(commonSettings.getDetailsPageTextSize());
       }
        super.setUserVisibleHint(isVisibleToUser);
    }

    public void pageTextSize(int size){
        //int size = CommonSettings.getInstance(this.getActivity()).getDetailsPageTextSize();
        Print.e(this, "text size: "+size);
        setViewsTextSize(size);
    }

    private void setViewsTextSize(final int size){



        final DisplayMetrics dm = new DisplayMetrics();
        this.getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
       // pixelSize = (int)scaledPixelSize * dm.scaledDensity;

        //webViewScale is found less in higher res device

        int webViewFontSize = (int) ((size * dm.scaledDensity)/ webViewScale);

        WebSettings webSettings = wvDetails.getSettings();
        Print.e(this, "scaledDensity: "+ dm.scaledDensity);
        Print.e(this, "webViewScale: "+ webViewScale);
        Print.e(this, "size: "+ size);

        webSettings.setDefaultFontSize(webViewFontSize);
        tvTitle.setTextSize((int)((webViewFontSize * 1.4)*webViewScale ));



    }
    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        commonSettings = CommonSettings.getInstance(this.getActivity());
        Print.e(this, "onCreate");
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Print.e(this, "onCreate View");
        View mainView = inflater.inflate (R.layout.frag_details,null);
        detailsBody = (RelativeLayout) mainView.findViewById(R.id.detailsBody);
        detailsHeader = (RelativeLayout) mainView.findViewById(R.id.detailsHeader);
        bodyHeader = (LinearLayout) mainView.findViewById(R.id.bodyHeader);
        viewHorizontal = (View) mainView.findViewById(R.id.viewHorizontal);
        //detailsBody.setVisibility(View.INVISIBLE);
        //detailsHeader.setVisibility(View.GONE);

        noInternetConnection = (LinearLayout) mainView.findViewById(R.id.noInternetConnection);
        serverError = (LinearLayout) mainView.findViewById(R.id.serverError);
        retryNoInternetConnection = (Button) mainView.findViewById(R.id.btnRetry);
        retryServerError = (Button) mainView.findViewById(R.id.btnServerErrorRetry);
        reletedRV = (RecyclerView)  mainView.findViewById(R.id.relatedlList);
        featureImageView = (ImageView) mainView.findViewById(R.id.featuredImage);
        tvShortDescription = (TextView) mainView.findViewById(R.id.tvShortDescription) ;
        wvDetails= (WebView) mainView.findViewById(R.id.wvContent) ;
        tvTitle = (TextView) mainView.findViewById(R.id.fullHeader);
        tvTime = (TextView) mainView.findViewById(R.id.time);
        tvCategoryName = (TextView) mainView.findViewById(R.id.cat);
        progressBarMain = (ProgressBar) (mainView.findViewById(R.id.messageContainer).findViewById(R.id.progressBar));
        progressBarImageView = (ProgressBar) (mainView.findViewById(R.id.containerMainImageView).findViewById(R.id.progressBar));
        progressBarMain.setVisibility(View.GONE);

        reletedItemsAdapter = new GeneralRecyclerViewAdapter(this.getActivity(), this);
        reletedRV.setAdapter(reletedItemsAdapter);
        reletedRV.setLayoutManager(new LinearLayoutManager(reletedRV.getContext()));
        reletedRV.setNestedScrollingEnabled(false);

        mScrollable = (S) createScrollable(mainView);
        mScrollable.setScrollViewCallbacks(this);


        ObservableScrollView observableScrollView = (ObservableScrollView) mainView.findViewById(R.id.scrollable);
        observableScrollView.setScrollViewCallbacks(this);

        swipeContainer = (SwipeRefreshLayout) mainView.findViewById(R.id.swipeContainer);
        swipeContainer.setColorSchemeResources(android.R.color.black,
                android.R.color.black,
                android.R.color.black,
                android.R.color.black);
       /* TypedValue tv = new TypedValue();
        int actionBarHeight = 0;
        if (getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true))
        {
            actionBarHeight = TypedValue.complexToDimensionPixelSize(tv.data,getResources().getDisplayMetrics());
        }

        swipeContainer.setProgressViewEndTarget(true, actionBarHeight * CommonConstants.swipeRefreshLayoutProgressOfset / 2);
        swipeContainer.setProgressViewOffset(true, actionBarHeight, actionBarHeight*CommonConstants.swipeRefreshLayoutProgressOfset);
*/

        /*Bundle bundle = this.getActivity().getIntent().getExtras();
        entryBinding  = (EntryBinding) bundle.getSerializable(EXTRA_ENTRY_BINCING);
        Print.e(this, "OnCreate View");
        Print.e(this, "entry id "+entryBinding.getEntry().getId());
        cats = entryBinding.getEntry().getCategories();*/

        retryNoInternetConnection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                processData(entryBinding, entryBinding.getEntry().getId());            }
        });

        retryServerError.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                processData(entryBinding, entryBinding.getEntry().getId());            }
        });


        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                processData(entryBinding, entryBinding.getEntry().getId());

            }
        });



        Print.e(this, "onCreateView");



        tvCategoryName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((EntryDetailsActivity)FragDetails.this.getActivity()).setResultForReturn(true, entryBinding.getEntry().getPrimaryCatId());
                ((EntryDetailsActivity)FragDetails.this.getActivity()).finish();
            }
        });

        setViewStateOfMainBody(View.INVISIBLE, View.INVISIBLE, View.INVISIBLE, View.INVISIBLE);
        Bundle bundle = this.getArguments();
        if (bundle.getBoolean(KEY_IS_FIRST_ITEM)){
            // only first loaded fragment by view pager will access of function processData
            //processData(entryBinding, entryBinding.getEntry().getId());
            new DBOperation().execute("");
        }

        wvDetails.setWebViewClient(new WebViewClient() {
            @Override
            public void onScaleChanged(WebView view, float oldScale, float newScale) {
                super.onScaleChanged(view, oldScale, newScale);
                webViewScale = newScale;
            }
        });

        Print.e(this, getDeviceDensity(this.getActivity()));

        return mainView;
    }

    private class DBOperation extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            processData(entryBinding, entryBinding.getEntry().getId());
            return "Executed";
        }

        @Override
        protected void onPostExecute(String result) {

        }

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected void onProgressUpdate(Void... values) {}
    }

    private void render(final EntryBinding entryBinding, final String details){
        if ( FragDetails.this.getActivity() == null)
            return;
        FragDetails.this.getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                tvShortDescription.setText(Html.fromHtml(entryBinding.getEntry().getShortDescription()));
                wvDetails.getSettings().setJavaScriptEnabled(true);
                wvDetails.getSettings().setUseWideViewPort(true);
                wvDetails.getSettings().setLoadWithOverviewMode(true);
                wvDetails.setBackgroundColor(Color.TRANSPARENT);
                wvDetails.loadDataWithBaseURL("", details, "text/html", "UTF-8", "");
                tvTitle.setText(entryBinding.getEntry().getTitle());
                tvTime.setText(TimestampConverter.parseDate(entryBinding.getEntry().getPubDate()+"")+"");
                tvCategoryName.setText(entryBinding.getEntry().getCatName());
                setViewsTextSize(commonSettings.getDetailsPageTextSize());
                if (entryBinding.getEntry().getMediaType() == EntryDao.TYPE_MEDIA_IMAGE){
           /*if ( entryBinding.getLargeImageMedia()== null){
               hideDetailsHeader();
           }*/
                    loadMediaImages(entryBinding.getLargeImageMedia(), entryBinding.getLargeImageMediaShortDesc());
                }else{
                    loadMediaVideo(entryBinding.getVideoMedia());
                }

                EntryDao entryDao = new EntryDao( FragDetails.this.getActivity());
                List<EntryBinding> gb =  entryDao.getRelatedEntries(entryBinding.getEntry().getId());
                List<EntryBinding> ebFound = new ArrayList<>();
                if (gb != null){
                    Print.e(this, "size: "+ gb.size());
                    for (EntryBinding eb : gb) {
                        if (eb.getEntry() != null){ // already this Entry downloaded
                            ebFound.add(eb);
                        }else{ // Entry not downloaded but found as releted entry

                        }
                    }
                    updateRelatedEntryListView(ebFound);

                }else{
                    Print.e(this, "No Related Items found");
                }
                setViewStateOfMainBody(View.GONE, View.VISIBLE, View.GONE, View.GONE);
                swipeContainer.setRefreshing(false);
            }
        });


    }

    private void updateRelatedEntryListView( List<EntryBinding> entryBindings){
        reletedItemsAdapter.mValues.clear();
        reletedItemsAdapter.mValues.addAll(entryBindings);
        reletedItemsAdapter.notifyDataSetChanged();
        reletedRV.invalidate();
    }
    private void hideDetailsHeader(){
        detailsHeader.setVisibility(View.GONE);
        ((EntryDetailsActivity)this.getActivity()).setScrollSpeedMul(true);
    }
    private void loadMediaImages(final ArrayList<String> mediaRef, final ArrayList<String> mediaShortDesc){
        if (mediaRef != null && mediaRef.size() > 0){
            GlideImageRenderer.renderFeatured(CommonAplication.getAppContext(),
                    featureImageView, mediaRef.get(0), new ImageRenderListner() {
                        @Override
                        public void onSuccess() {
                            mainProgressBar(View.INVISIBLE);
                        }

                        @Override
                        public void onFailed() {
                            mainProgressBar(View.INVISIBLE);
                        }
                    });

            featureImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    gotoImageSliderActivity(mediaRef, mediaShortDesc);
                }
            });
        }

        //load image gallery from here
    }
    private void loadMediaVideo(ArrayList<String> mediaRef){
        //load video gallary from here
    }

    private void gotoImageSliderActivity(ArrayList<String> imageRefs, ArrayList<String> imageShortDesc){
        Intent intent = new Intent(this.getActivity(), ImageSliderActivity.class);
        intent.putStringArrayListExtra(ImageSliderActivity.EXTRA_IMAGE_REF, imageRefs);
        intent.putStringArrayListExtra(ImageSliderActivity.EXTRA_IMAGE_SHORT_DESC_REF, imageShortDesc);
        this.startActivity(intent);
    }

    private void processData(final EntryBinding entryBinding, final long entryId){
        this.entryBinding = entryBinding;
        FragDetails.this.getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                resetViewState();
                mainProgressBar(View.VISIBLE);
                setViewStateOfMainBody(View.VISIBLE, View.GONE, View.GONE, View.GONE);

                final String detailsFromDb = new EntryDao(FragDetails.this.getActivity()).getDetails(entryId);
                if (ConnectionDetector.isNetworkPresent(FragDetails.this.getActivity())){
                    EntryDetailsGetAPICall.getInstance().callAPI(FragDetails.this.getActivity(), null, new APIClientResponse() {
                        @Override
                        public void failureOnApiCall(String msg, Object sender) {
                            failedToDownloadData();
                        }

                        @Override
                        public void failureOnNetworkConnection(String msg, Object sender) {
                            failedToDownloadData();
                        }

                        @Override
                        public void successOnApiCall(String msg, Object sender) {

                            EntryDetailsResponce entryDetailsResponce = (EntryDetailsResponce) sender;
                            String details = entryDetailsResponce.getEntryDetails().getDetails();
                            if (details != null && details.length() > 0){
                                EntryDao entryDao = new EntryDao(FragDetails.this.getActivity());
                                entryDao.updateDetails(entryDetailsResponce.getEntryDetails());
                                render(entryBinding, entryDetailsResponce.getEntryDetails().getDetails());
                            }else{// no details found from server, means server is returning empty string or null
                                if (detailsFromDb != null){ // render what is in db
                                    render(entryBinding, detailsFromDb);
                                }else{ // nothing found to render
                                    //TODO internet conncetion is available but no data from server or cached
                                    // Try again later could be shown in screen
                                    setViewStateOfMainBody(View.GONE, View.GONE, View.GONE, View.VISIBLE);
                                    swipeContainer.setRefreshing(false);
                                }
                            }
                        }

                        private void failedToDownloadData(){
                            if (detailsFromDb != null){
                                render(entryBinding, detailsFromDb);
                            }else{ // please check your internet connection
                                setViewStateOfMainBody(View.GONE, View.GONE, View.VISIBLE, View.GONE);
                                swipeContainer.setRefreshing(false);
                            }
                        }
                    }, entryId+"", entryBinding.getEntry().getModifiedAt()+"");
                }else if(detailsFromDb != null){ // internet connection not found but there is a version of details is cached before
                    render(entryBinding, detailsFromDb);
                }else{ // please check your internet connection
                    dataNotFoundCheckYourInternetConnection();
                    setViewStateOfMainBody(View.GONE, View.GONE, View.VISIBLE, View.GONE);
                    swipeContainer.setRefreshing(false);

                }
            }
        });

    }

    private void dataNotFoundCheckYourInternetConnection(){

    }

    @Override
    public void onOpenEntryListner(EntryBinding selectedEntry) {
       // processData(selectedEntry, selectedEntry.getEntry().getId());
        Print.e(this, "onOpenEntryListner");
        StaticFun.OPEN_DETAILS_ACTIVITY(selectedEntry, this.getActivity(), CommonConstants.REQUEST_CODE_FOR_DETAILS_ACTIVITY);
    }

    private void setViewStateOfMainBody(int mainPb, int content, int iProblem, int sError){
        progressBarMain.setVisibility(mainPb);
        //detailsBody.setVisibility(content);
        bodyHeader.setVisibility(content);
        viewHorizontal.setVisibility(content);
        detailsHeader.setVisibility(content);
        noInternetConnection.setVisibility(iProblem); // this will be above  if (sError == View.VISIBLE){
        if (sError == View.VISIBLE){
            if (ConnectionDetector.isNetworkPresent(FragDetails.this.getActivity())){
                serverError.setVisibility(sError);
                noInternetConnection.setVisibility(View.INVISIBLE);
            }else{
                noInternetConnection.setVisibility(sError);
                serverError.setVisibility(View.INVISIBLE);
            }
        }else{
            serverError.setVisibility(sError);
        }
    }

    private void mainProgressBar(int pbMainImageView){
        progressBarImageView.setVisibility(pbMainImageView);
    }

    private void resetViewState(){
        tvShortDescription.setText("");
        tvTime.setText("");
        tvTitle.setText("");
        tvCategoryName.setText("");
        featureImageView.setImageResource(android.R.color.transparent);
        if (Build.VERSION.SDK_INT < 18) {
            wvDetails.clearView();
        } else {
            wvDetails.loadUrl("about:blank");
        }

    }


    ////////////////



    int counter;

    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    @RequiresApi(api = Build.VERSION_CODES.HONEYCOMB_MR1)
    @Override
    public void onScrollChanged(int scrollY, boolean firstScroll, boolean dragging) {
        detailsHeader.setTranslationY(scrollY / 2);
        int[] locWebView = new int[2];
        detailsBody.getLocationOnScreen(locWebView);
        ((EntryDetailsActivity)this.getActivity()).onScrollChanged(locWebView[1]);
    }

    @Override
    public void onFling(int velocityY) {
        ((EntryDetailsActivity)this.getActivity()).onFling(velocityY);
    }

    @Override
    public void onDownMotionEvent() {
        Print.e(this, "on motion event");
    }

    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    @RequiresApi(api = Build.VERSION_CODES.HONEYCOMB_MR1)
    @Override
    public void onUpOrCancelMotionEvent(ScrollState scrollState) {
        Log.e("DEBUG", "onUpOrCancelMotionEvent: " + scrollState);
        ((EntryDetailsActivity)this.getActivity()).setScrollState(scrollState);
        if (scrollState == ScrollState.DOWN) {

            /*if (!getSupportActionBar().isShowing()) {
                if (getSupportActionBar() != null) {
                    getSupportActionBar().show();
                }
            }*/
           // mToolbar.animate().translationY(0).setInterpolator(new AccelerateInterpolator()).start();
           // swipeContainerHolder.setPadding(0,toolbarHeight,0,0);
            //swipeContainerHolder.animate().translationY(toolbarHeight).setInterpolator(new AccelerateInterpolator()).start();

        }

      /*  if (scrollState == ScrollState.UP) {
            if (toolbarIsShown()) {
                hideToolbar();
            }
        } else if (scrollState == ScrollState.DOWN) {
            if (toolbarIsHidden()) {
                showToolbar();
            }
        }*/
    }

    /*private boolean toolbarIsShown() {
        return ViewHelper.getTranslationY(mToolbar) == 0;
    }

    private boolean toolbarIsHidden() {
        return ViewHelper.getTranslationY(mToolbar) == -mToolbar.getHeight();
    }

    private void showToolbar() {
        moveToolbar(0);
    }

    private void hideToolbar() {

    }*/

    private void moveToolbar(float toTranslationY) {

    }
    @RequiresApi(api = Build.VERSION_CODES.HONEYCOMB)
    protected ObservableScrollView createScrollable(View mainView) {
        ObservableScrollView observableScrollView =(ObservableScrollView) mainView.findViewById(R.id.scrollable);
        observableScrollView.setSmoothScrollingEnabled(true);

        return  observableScrollView;
    }

    @Override
    public void onResume() {
        super.onResume();
    }
    @Override
    public void onPause() {

        super.onPause();
        Print.e(this, "onPause");
    }

    @Override
    public void onDestroy() {
        Print.e(this, "onDestroy");
        super.onDestroy();
    }

    @Override
    public void awake() {
        Print.e(this, "awake");
    }

    @Override
    public void hide() {
        Print.e(this, "hide");

    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
    }

    @Override
    public void reload() {

    }

    public static String getDeviceDensity(Context context){
        String deviceDensity = "";
        switch (context.getResources().getDisplayMetrics().densityDpi) {
            case DisplayMetrics.DENSITY_LOW:
                deviceDensity =  0.75 + " ldpi";
                break;
            case DisplayMetrics.DENSITY_MEDIUM:
                deviceDensity =  1.0 + " mdpi";
                break;
            case DisplayMetrics.DENSITY_HIGH:
                deviceDensity =  1.5 + " hdpi"; // symphony w69q
                break;
            case DisplayMetrics.DENSITY_XHIGH:
                deviceDensity =  2.0 + " xhdpi"; // samsung j-7
                break;
            case DisplayMetrics.DENSITY_XXHIGH:
                deviceDensity =  3.0 + " xxhdpi";
                break;
            case DisplayMetrics.DENSITY_XXXHIGH:
                deviceDensity =  4.0 + " xxxhdpi";
                break;
            default:
                deviceDensity = "Not found";
        }
        return deviceDensity;
    }
}
