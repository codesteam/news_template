/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.base.fragments;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.base.activity.MainActivity;
import com.base.adapter.GeneralRecyclerViewAdapter;
import com.base.common.CommonAplication;
import com.base.common.CommonConstants;
import com.base.common.StaticFun;
import com.base.factory.EntryCatRenderer;
import com.base.factory.OnOpenEntry;
import com.base.modarate.view.IAdView;
import com.base.modarate.view.ZixisngSettings;
import com.base.utility.ConnectionDetector;
import com.base.utility.FloatingGradientEffect;
import com.commonasset.utils.GlideImageRenderer;
import com.commonasset.utils.ImageRenderListner;
import com.commonasset.utils.Print;
import com.commonasset.utils.TimestampConverter;
import com.db.dao.CategoryDao;
import com.db.dao.EntryDao;
import com.db.dao.GroupDaoTemp;
import com.db.helper.EntryBinding;
import com.db.helper.EntryTypeBinding;
import com.softbondit.news.karu.R;
import com.retrofit.api.APIClientResponse;
import com.retrofit.handler.EntryGetAPICall;
import com.retrofit.handler.HomeGetAPICall;
import com.retrofit.response.entry.Entry;

import java.util.ArrayList;
import java.util.List;

public class FragSubGroupList extends Fragment implements OnOpenEntry {

    public static final String EXTRA_CAT_ID = "cat_id";
    public static final String EXTRA_REQUEST_FOR_DETAILS_ACTIVITY = "request_for_details_activity";
    ImageView featuredImageView;
    TextView featuredTitle;
    TextView featuredShortDescription;
    TextView featuredPostedTime;
    TextView featuredVideoDuration;
    EntryDao entryDao;
    MainActivity mainActivity;
    RecyclerView recyclerViewSpecial;
    RecyclerView recyclerViewGeneral;
    LinearLayout featuredLayout;
    //Long catId;
    SpecialRecyclerViewAdapter specialRecyclerViewAdapter;
    GeneralRecyclerViewAdapter generalRecyclerViewAdapter;
    //ArrayList<EntryBinding> generalBindings;
    ArrayList<EntryBinding> specialBindings;
    CategoryDao categoryDao;
    ProgressBar progressBarContainerMain;
    ProgressBar progressBarImageView;
    LinearLayout noInternetConnectionContainer;
    NestedScrollView containerView;
    LinearLayout serverError;
    private SwipeRefreshLayout swipeContainer;
    private long selectedCategoryId;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Print.e(this, "onCreate");
        mainActivity = (MainActivity) getActivity();
        entryDao = new EntryDao(mainActivity);
        categoryDao = new CategoryDao(mainActivity);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Print.e(this, "onCreateView");
        ViewGroup mainView = (ViewGroup)  inflater.inflate(
                R.layout.fragment_entry_list, container, false);
        Bundle bundle =getArguments();
        selectedCategoryId = bundle.getLong(EXTRA_CAT_ID);

        recyclerViewGeneral = (RecyclerView)  mainView.findViewById(R.id.generalList);
        recyclerViewSpecial = (RecyclerView)  mainView.findViewById(R.id.specialList);
        featuredImageView = (ImageView) mainView.findViewById(R.id.featuredImage);
        featuredTitle = (TextView) mainView.findViewById(R.id.featuredTitle);
        featuredShortDescription = (TextView) mainView.findViewById(R.id.featuredShortDescription);
        featuredPostedTime = (TextView) mainView.findViewById(R.id.featuredPostedTime);
        featuredVideoDuration = (TextView) mainView.findViewById(R.id.tvDuration);
        featuredLayout = (LinearLayout) mainView.findViewById(R.id.featuredLayout);
        swipeContainer = (SwipeRefreshLayout) mainView.findViewById(R.id.swipeContainer);
        serverError = (LinearLayout) mainView.findViewById(R.id.serverError);
        /*TypedValue tv = new TypedValue();
        int actionBarHeight = 0;
        if (this.getActivity().getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true))
        {
            actionBarHeight = TypedValue.complexToDimensionPixelSize(tv.data,getResources().getDisplayMetrics());
        }

        swipeContainer.setProgressViewEndTarget(true, actionBarHeight * CommonConstants.swipeRefreshLayoutProgressOfset / 2);
        swipeContainer.setProgressViewOffset(true, actionBarHeight, actionBarHeight*CommonConstants.swipeRefreshLayoutProgressOfset);
*/

        generalRecyclerViewAdapter = new GeneralRecyclerViewAdapter(mainActivity, this);
        recyclerViewGeneral.setAdapter(generalRecyclerViewAdapter);
        recyclerViewGeneral.setLayoutManager(new LinearLayoutManager(recyclerViewGeneral.getContext()));

        specialBindings = new ArrayList<>();
        specialRecyclerViewAdapter = new SpecialRecyclerViewAdapter(mainActivity, specialBindings);
        recyclerViewSpecial.setAdapter(specialRecyclerViewAdapter);
        recyclerViewSpecial.setLayoutManager(new LinearLayoutManager(recyclerViewSpecial.getContext()));

        //renderManager.setRenderer(this);

        progressBarContainerMain = (ProgressBar) (mainView.findViewById(R.id.mainProgressBarContainer).findViewById(R.id.progressBar));
        progressBarImageView = (ProgressBar) (mainView.findViewById(R.id.featuredImageContainer).findViewById(R.id.progressBar));

        noInternetConnectionContainer =(LinearLayout) mainView.findViewById(R.id.noInternetConnection);
        containerView = (NestedScrollView) mainView.findViewById(R.id.containerScrollView);

       setMainProgressBarVsSwipeRefreshViewState(View.GONE, View.GONE);
        progressBarImageView.setVisibility(View.GONE);
        containerView.setVisibility(View.GONE);
        noInternetConnectionContainer.setVisibility(View.GONE);
        serverError.setVisibility(View.GONE);
        mainView.findViewById(R.id.btnRetry).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                populateView(selectedCategoryId, false);
            }
        });

        mainView.findViewById(R.id.btnServerErrorRetry).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                populateView(selectedCategoryId, false);
            }
        });

        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Your code to refresh the list here.
                // Make sure you call swipeContainer.setRefreshing(false)
                // once the network request has completed successfully.
                populateView(selectedCategoryId, true);

            }
        });
        swipeContainer.setColorSchemeResources(android.R.color.black,
                android.R.color.black,
                android.R.color.black,
                android.R.color.black);
        recyclerViewGeneral.setNestedScrollingEnabled(false);
        recyclerViewSpecial.setNestedScrollingEnabled(false);
        /*if (bundle.containsKey(EXTRA_REQUEST_FOR_DETAILS_ACTIVITY)){ // request for open the details activity
            Print.e(this, "onCreateView  Contaings Key: "+ EXTRA_REQUEST_FOR_DETAILS_ACTIVITY);
            EntryBinding entryBinding = (EntryBinding) bundle.getSerializable(EXTRA_ENTRY_BINCING);
            long entyID = entryBinding.getEntry().getId();
            // condition 1: data downloaded for this app launch time, database is updated on this launched time
            //condition 2: data did't downloaded for this app launch time, databse has data but they were updated last launched time
            //condition 3: data didn't downloaded at all, data-base is empty for this catetory

            // trigger1: goto details activity bypassing this frag ui population, download and update data on background thread
            //on destroy of this frag check background thread is running if running kill it
            // on pause keap alive this thread, and that thread will only update database not the ui
            // on resume check if the thread is running, if running show progress bar, if killled check if database updated for this applaunched
                            //if not then show progressbar and download data

            //since news body has huge data trigger 2 prefarable
            //trigger2: got details activiy bypassing this frag ui population, on resume of this frag check if databse updated for this app launced
                                // if not then show progressbar and download data

            /*//*** when user back from details activity it tigger of this fragment full life cycle onCreate -> onCreateView -> onResume
                        // so when its back trigger2 automatically happen
            gotoDetailsActivity(entryBinding);
        }else{
            populateView(); // check if data downloaded for this app launch time
        }*/


        populateView(selectedCategoryId, false);



        return mainView;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!MainActivity.forwardTransction) {
            ((IAdView) this.getActivity()).show();
            if (ConnectionDetector.isNetworkPresent(this.getActivity())){
                ZixisngSettings.setFliperB(this.getActivity());
            }
            MainActivity.forwardTransction = true;
            Print.e(this, "Backward Transaction Calling ad");
        }else{
            Print.e(this, "Backward Transaction ad timeout");
        }
        Print.e(this, "onResume");
    }

    @Override
    public void onPause() {
        super.onPause();
        Print.e(this, "onPause");
    }

    @Override
    public void onDestroy() {
        Print.e(this, "onDestroy");
        super.onDestroy();
    }


    private void populateView(long catId, boolean refresh){
        int catState = categoryDao.getCatState(catId);
        Print.e(this, "Cat id: " + catId);
        if (refresh){

            if (catId == CommonConstants.DEFAULT_CAT_HOME_ID){
                new DownloadHome(catId).execute(catId+"");
            }else{
                new DownloadEntries(catId).execute(catId+"");
            }

        }
        else if (catState == CategoryDao.ON_START_DOWNLOAD_COMPLETE){
            setMainProgressBarVsSwipeRefreshViewState(View.GONE, View.VISIBLE);
            noInternetConnectionContainer.setVisibility(View.GONE);
            containerView.setVisibility(View.VISIBLE);
            updateView(catId);
        }else{
            if (catId == CommonConstants.DEFAULT_CAT_HOME_ID){
                new DownloadHome(catId).execute(catId+"");
            }else{
                new DownloadEntries(catId).execute(catId+"");
            }
            setMainProgressBarVsSwipeRefreshViewState(View.VISIBLE, View.GONE);
            containerView.setVisibility(View.GONE);
            noInternetConnectionContainer.setVisibility(View.GONE);

        }
    }

    @Override
    public void onOpenEntryListner(EntryBinding selectedEntry) {
        // called from : onClick(SearchItem searchItem) , GeneralRecyclerView OnClick, SpecialRecycleView OnClick
        //Featured Item on click
        MainActivity.forwardTransction = false;
        StaticFun.OPEN_DETAILS_ACTIVITY(selectedEntry, FragSubGroupList.this.getActivity(), CommonConstants.REQUEST_CODE_FOR_DETAILS_ACTIVITY);
    }

    private class DownloadHome extends AsyncTask<String, Integer, String> {
        private long lastDownloadedTime;
        long catId;

        public DownloadHome(long catId) {
            this.catId = catId;
            String timeStamp =categoryDao.getLastDownloaded(catId);
            this.lastDownloadedTime = timeStamp!=null?  Long.valueOf(timeStamp): 0;
            Print.e(this, "lastDownloadedTime: "+ lastDownloadedTime);
        }

        @Override
        protected String doInBackground(final String... params) {
            // if ( entryDao.count(catId) <= 0){
            HomeGetAPICall.getInstance().callAPI(mainActivity, null, new APIClientResponse() {
                @Override
                public void failureOnApiCall(String msg, Object sender) {
                    checkForEmptyDatabase();
                }

                @Override
                public void failureOnNetworkConnection(String msg, Object sender) {
                    checkForEmptyDatabase();
                }

                @Override
                public void successOnApiCall(String msg, Object sender) {
                    long systemTime = System.currentTimeMillis();
                    Print.e(this, "systemTime: "+ systemTime);
                    if (systemTime > lastDownloadedTime){
                        lastDownloadedTime = systemTime;
                    }
                    Print.e(this, "Download complete");
                    categoryDao.updateLastDownloaded(catId,lastDownloadedTime+"");
                    categoryDao.setCatState(catId, CategoryDao.ON_START_DOWNLOAD_COMPLETE);
                    onRenderView();
                }

                private void checkForEmptyDatabase(){

                    mainActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            setMainProgressBarVsSwipeRefreshViewState(View.GONE, View.VISIBLE);
                            if (lastDownloadedTime > 0){
                                containerView.setVisibility(View.VISIBLE);
                                noInternetConnectionContainer.setVisibility(View.GONE);
                                Print.e(this, "onRenderView");
                                onRenderView();
                            }else{
                                containerView.setVisibility(View.GONE);
                                noInternetConnectionContainer.setVisibility(View.VISIBLE);
                                Print.e(this, "onRenderView else");
                            }
                        }
                    });
                }
                private void onRenderView(){
                    mainActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            setMainProgressBarVsSwipeRefreshViewState(View.GONE, View.VISIBLE);
                            containerView.setVisibility(View.VISIBLE);
                            updateView(catId);
                            Print.e(this, "updateView");
                        }
                    });
                }
            }, params[0], lastDownloadedTime+"", null, null, null, null);

            return params[0];
        }


        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            Print.e(this, "onPostExecute");
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(Integer... values) {

            super.onProgressUpdate(values);
        }

    }
    private class DownloadEntries extends AsyncTask<String, Integer, String> {
        private long lastDownloadedTime;
        long catId;

        public DownloadEntries(long catId) {
            this.catId = catId;
            String timeStamp =categoryDao.getLastDownloaded(catId);
            this.lastDownloadedTime = timeStamp!=null?  Long.valueOf(timeStamp): 0;
            Print.e(this, "lastDownloadedTime: "+ lastDownloadedTime);
        }

        @Override
        protected String doInBackground(final String... params) {
           // if ( entryDao.count(catId) <= 0){
                EntryGetAPICall.getInstance().callAPI(mainActivity, null, new APIClientResponse() {
                    @Override
                    public void failureOnApiCall(String msg, Object sender) {
                       checkForEmptyDatabase();
                    }

                    @Override
                    public void failureOnNetworkConnection(String msg, Object sender) {
                       checkForEmptyDatabase();
                    }

                    @Override
                    public void successOnApiCall(String msg, Object sender) {
                        long systemTime = System.currentTimeMillis();
                        if (systemTime > lastDownloadedTime){
                            lastDownloadedTime = systemTime;
                        }
                        Print.e(this, "Download complete");
                        categoryDao.updateLastDownloaded(catId,lastDownloadedTime+"");
                        categoryDao.setCatState(catId, CategoryDao.ON_START_DOWNLOAD_COMPLETE);
                        onRenderView();
                    }

                    private void checkForEmptyDatabase(){

                        mainActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                setMainProgressBarVsSwipeRefreshViewState(View.GONE, View.VISIBLE);
                                if (lastDownloadedTime > 0){
                                    containerView.setVisibility(View.VISIBLE);
                                    noInternetConnectionContainer.setVisibility(View.GONE);
                                    onRenderView();
                                }else{
                                    containerView.setVisibility(View.GONE);
                                    noInternetConnectionContainer.setVisibility(View.VISIBLE);
                                }
                            }
                        });
                    }
                    private void onRenderView(){
                        mainActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                setMainProgressBarVsSwipeRefreshViewState(View.GONE, View.VISIBLE);
                                containerView.setVisibility(View.VISIBLE);
                                updateView(catId);
                            }
                        });
                    }
                }, params[0], lastDownloadedTime+"", null, null, null, null);

            return params[0];
        }


        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            Print.e(this, "onPostExecute");
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(Integer... values) {

            super.onProgressUpdate(values);
        }

    }

    private void setMainProgressBarVsSwipeRefreshViewState(int progressBarMain, int swipeCon){
        progressBarContainerMain.setVisibility(progressBarMain);
        swipeContainer.setVisibility(swipeCon);
    }
  /*  private void updateSearchView(List<EntryBinding> entryBindings){
        if (Build.VERSION.SDK_INT >= CommonConstants.MIN_SDK_VERSION_FOR_SEARCH_VEIW) {
            mainActivity.getSearchView().addSuggestions(entryBindings);

        }
    }*/
    private void updateView(final long catId){
        mainActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final EntryTypeBinding entryTypeBinding;
                if (catId == CommonConstants.DEFAULT_CAT_HOME_ID){ // home cat id
                    GroupDaoTemp groupDaoTemp = new GroupDaoTemp(FragSubGroupList.this.getActivity());
                    entryTypeBinding = groupDaoTemp.getEntryTypeBinding();
                }else{
                    entryTypeBinding = entryDao.getEntryTypeBinding(catId);
                }
                if (entryTypeBinding != null){
                    List<EntryBinding> feBindings= entryTypeBinding.get( EntryDao.TYPE_FEATURED);
                    if (feBindings != null && feBindings.size() > 0){
                        final EntryBinding entryBinding = feBindings.get(0);
                        featuredTitle.setText(entryBinding.getEntry().getTitle());
                        FloatingGradientEffect.setEffect(featuredTitle, mainActivity);
                        featuredShortDescription.setText(Html.fromHtml(entryBinding.getEntry().getShortDescription()+""));
                        featuredPostedTime.setText(TimestampConverter.parseDate(entryBinding.getEntry().getPubDate()+""));
                        String imageUrl = "";
                        if (entryBinding.getEntry().getMediaType() == EntryDao.TYPE_MEDIA_IMAGE){
                            featuredVideoDuration.setVisibility(View.GONE);
                            if (entryBinding.getLargeImageMedia() != null && entryBinding.getLargeImageMedia().size() > 0){
                                imageUrl = entryBinding.getLargeImageMedia().get(0);
                            }

                        }else{
                            featuredVideoDuration.setVisibility(View.VISIBLE);
                           // imageUrl = "https://img.youtube.com/vi/"+"SRcnnId15BA"+"/hqdefault.jpg";
                            imageUrl = "https://img.youtube.com/vi/"+ StaticFun.getYoutubeIdFromUrl(entryBinding.getVideoMedia().get(0))+"/hqdefault.jpg";
                            featuredVideoDuration.setText(entryBinding.getVideoMediaDuration().get(0));
                        }

                        progressBarImageView.setVisibility(View.VISIBLE);
                        GlideImageRenderer.renderFeatured(CommonAplication.getAppContext(), featuredImageView, imageUrl, new ImageRenderListner() {
                            @Override
                            public void onSuccess() {
                                progressBarImageView.setVisibility(View.GONE);
                            }

                            @Override
                            public void onFailed() {
                                progressBarImageView.setVisibility(View.GONE);
                            }
                        });

                        featuredLayout.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                onOpenEntryListner(entryBinding);
                            }
                        });
                    }else{
                        Print.e(this, "Featured Item not found");
                        featuredLayout.setVisibility(View.GONE);
                    }

                    List<EntryBinding> gb =  entryTypeBinding.get( EntryDao.TYPE_GENERAL);
                    EntryCatRenderer.getInstance().clear();
                    EntryCatRenderer.getInstance().add(catId, entryTypeBinding.getAllEntriesSameCat());
                    if (gb != null){
                        Print.e(this, "size: "+ gb.size());
                        generalRecyclerViewAdapter.mValues.clear();
                        generalRecyclerViewAdapter.selectedCatId = catId;
                        generalRecyclerViewAdapter.mValues.addAll(gb);
                        generalRecyclerViewAdapter.notifyDataSetChanged();
                        recyclerViewGeneral.invalidate();
                    }else{
                        Print.e(this, "No General Items found");
                    }

                    List<EntryBinding> sb =  entryTypeBinding.get( EntryDao.TYPE_SPECIAL);
                    if (sb != null){
                        specialBindings.clear();
                        specialRecyclerViewAdapter.selectedCatId = catId;
                        specialBindings.addAll(sb);
                        specialRecyclerViewAdapter.notifyDataSetChanged();
                        recyclerViewSpecial.invalidate();
                    }else{
                        Print.e(this, "No Special Items found");
                    }

                    if (feBindings == null && gb == null && sb == null){
                        if (ConnectionDetector.isNetworkPresent(FragSubGroupList.this.getActivity())){
                            serverError.setVisibility(View.VISIBLE);
                            noInternetConnectionContainer.setVisibility(View.INVISIBLE);
                        }else{
                            noInternetConnectionContainer.setVisibility(View.VISIBLE);
                            serverError.setVisibility(View.INVISIBLE);
                        }

                        Print.e(this, "No featured, general, special items found");
                    }

                }else{
                    Print.e(this, "No entry founds");
                    if (ConnectionDetector.isNetworkPresent(FragSubGroupList.this.getActivity())){
                        serverError.setVisibility(View.VISIBLE);
                        noInternetConnectionContainer.setVisibility(View.INVISIBLE);
                    }else{
                        noInternetConnectionContainer.setVisibility(View.VISIBLE);
                        serverError.setVisibility(View.INVISIBLE);
                    }

                }


                swipeContainer.setRefreshing(false);
            }
        });
    }


    public class SpecialRecyclerViewAdapter
            extends RecyclerView.Adapter<SpecialRecyclerViewAdapter.ViewHolder> {

        private final TypedValue mTypedValue = new TypedValue();
        private int mBackground;
        private List<EntryBinding> items;
        public Long selectedCatId;
        public class ViewHolder extends RecyclerView.ViewHolder {

            public final View mView;
            public final TextView tvItem;


            public ViewHolder(View view) {
                super(view);
                mView = view;
                tvItem = (TextView) view.findViewById(R.id.tvItem);
            }

            @Override
            public String toString() {
                return super.toString() + " '" + tvItem.getText();
            }
        }

        public Entry getValueAt(int position) {
            return items.get(position).getEntry();
        }

        public SpecialRecyclerViewAdapter(MainActivity context, List<EntryBinding> items) {
            context.getTheme().resolveAttribute(R.attr.selectableItemBackground, mTypedValue, true);
            mBackground = mTypedValue.resourceId;

            this.items = items;
        }


        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.list_row_special_item, parent, false);
            view.setBackgroundResource(mBackground);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {

            holder.tvItem.setText(items.get(position).getEntry().getTitle());

            holder.mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onOpenEntryListner(items.get(position));
                }
            });

        }

        @Override
        public int getItemCount() {
            Print.e(this, "getItemCount: "+items.size() );
            return items.size();
        }
    }


  /*  private void openDetailsActivity(Long catId, List sameCatEntries, EntryBinding selectedEntry){
        //((MainActivity)this.getActivity()).setSelectedCatId(catId);
        // setting the new slected catId, because details view called from MainActivity Search view,
        //example: when in user view showing cat  10 items but user select cat 11 item from search view
        Print.e(this, "Selected cat: "+ catId);

        Intent intent = new Intent(this.getActivity(), EntryDetailsActivity.class);
        intent.putExtra(EXTRA_ENTRY_BINCING,  selectedEntry);
        intent.putExtra(EXTRA_ENTRY_CAT,  catId);
        intent.putParcelableArrayListExtra(EXTRA_LIST_ENTRY_BINCING, (ArrayList<? extends Parcelable>) sameCatEntries);
        getActivity().startActivityForResult(intent, requestCodeForDetailsActivity);
    }*/

}
