package com.base.utility;

import android.app.Activity;
import android.view.Gravity;
import android.widget.Toast;

public class ToastMsg {

	private static ToastMsg toastMsg  = null;
	private static Activity context = null;
	public static ToastMsg getInstance(Activity context)
	{
		if (toastMsg == null)
		{
			toastMsg = new ToastMsg();
			ToastMsg.context = context;
		}
		return toastMsg;
	}
	
	public  void Show(final String _msg)
	{
		if (ToastMsg.context != null)
		{
			context.runOnUiThread(new Runnable() {
				
				@Override
				public void run() {
					if ( _msg != null)
					{
						Toast msg  = Toast.makeText(ToastMsg.context, _msg, Toast.LENGTH_SHORT);
						msg.setGravity(Gravity.CENTER, msg.getXOffset() / 2, msg.getYOffset() / 2);
						msg.show();
					}
				}
			});
		}
		
	}
	
	public  void Show(int id)
	{
		Toast msg = Toast.makeText(ToastMsg.context, ToastMsg.context.getString(id), Toast.LENGTH_SHORT);
		
		msg.setGravity(Gravity.CENTER, msg.getXOffset() / 2, msg.getYOffset() / 2);
		msg.show();
	}
	
	public static void Toast(Activity activity, String _msg)
	{
		Toast msg;
		
		if (_msg != null)
		{
			msg  = Toast.makeText(activity, _msg, Toast.LENGTH_SHORT);
		}
		else
		{
			msg  = Toast.makeText(activity, "null", Toast.LENGTH_SHORT);
		}
		
		msg.setGravity(Gravity.CENTER, msg.getXOffset() / 2, msg.getYOffset() / 2);
		msg.show();
	}
	
	public static void Toast(Activity activity, int id)
	{
		Toast msg = Toast.makeText(activity, activity.getString(id), Toast.LENGTH_SHORT);
		
		msg.setGravity(Gravity.CENTER, msg.getXOffset() / 2, msg.getYOffset() / 2);
		msg.show();
	}
	
	
	public static void Toast(Activity activity, String _msg, int gravity)
	{
		Toast msg = Toast.makeText(activity, _msg, Toast.LENGTH_SHORT);
		
		msg.setGravity(gravity, msg.getXOffset() / 2, msg.getYOffset() / 2);
		msg.show();
	}
	
	public static void Toast(Activity activity, int id, int gravity)
	{
		Toast msg = Toast.makeText(activity, activity.getString(id), Toast.LENGTH_SHORT);
		
		msg.setGravity(gravity, msg.getXOffset() / 2, msg.getYOffset() / 2);
		msg.show();
	}
}
