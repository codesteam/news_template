package com.base.utility;

import android.app.Activity;
import android.content.Context;
import android.os.IBinder;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

public class SoftKeyboard {
	public static void HideSoftKeyboard(Activity context) {
		InputMethodManager imm = (InputMethodManager) context
				.getSystemService(Activity.INPUT_METHOD_SERVICE);

		View currentFocus = context.getCurrentFocus();
		IBinder token = currentFocus == null ? null : currentFocus
				.getWindowToken();
		if (token != null)
			imm.hideSoftInputFromWindow(token, 0);
	}

	public static void OpenSoftKeyboard(Activity context){
		((InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE))
				.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
	}
}
