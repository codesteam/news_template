package com.base.utility;

import android.support.v4.app.Fragment;

public abstract class FragmentNested extends Fragment {

    private FragmentParent parent;

    public FragmentParent getParent() {
        return parent;
    }

    /**
     * Called switching in-between sliding menu item.
     * Suppose you are switching from Targets(menu item) to Task(menu item).
     * awake() of Task child will be called. (child = backStack top of task)
     */
    public abstract void awake();
    /**
     * Called switching in-between sliding menu item.
     * Suppose you are switching from Targets(menu item) to Task(menu item).
     * hide() of Targets child will be called. (child = backStack top of Targets)
     */
    public abstract void hide();

    /**
     * To intercept with hardware back button. When MainActivity received hardware back event it passes the event throw this method.
     */
    public abstract void onBackPressed();

    /**
     * To reload/ refresh nested fragment view.
     */
    public abstract void reload();
    public void setParent(FragmentParent parent) {
        this.parent = parent;
    }

    /**
     * For moving forward from one nested fragment to another. Destination must be a new fragment.
     * If a fragment already in backStack use backtoFragment(Class destination) to go there.
     * @param destination
     */
    public void gotoFragment(FragmentNested destination, String tag){
        SoftKeyboard.HideSoftKeyboard(parent.getActivity());
        this.parent.addFragment(destination, tag);
    }

    /**
     * For going back to a fragment that already in backStack.
     * @param destination
     */
    public void backtoFragment(Class destination) {

        SoftKeyboard.HideSoftKeyboard(parent.getActivity());
        this.parent.backTo(destination);
    }

    /**
     *  if backStack count > 1 then backStack top will be pop.
     */
    public void goBack()
    {
        SoftKeyboard.HideSoftKeyboard(parent.getActivity());
        parent.goBack();
    }

    /**
     * Replace current nested fragment that you are in, with new one.
     * Calling this function will add new fragment in place of backStackTop.
     * @param with The nested fragment that you want to replace with backStackTop
     */
    public void replace(FragmentNested with, String withTag){
        SoftKeyboard.HideSoftKeyboard(parent.getActivity());
        this.parent.replace(with, withTag);
    }

    /**
     * Replace a target nested fragment that already in backStack with new one.
     * Including target. All nested fragments whose has top position in backStack then target will be pop.
     * @param target the fragment you want to remove from backStack
     * @param with the Fragment you want to replace in place of target.
     */
    public void replace(Class target, FragmentNested with, String withTag){
        SoftKeyboard.HideSoftKeyboard(parent.getActivity());
        this.parent.replace(target, with, withTag);
    }

    public void setTitle(String title){
        this.parent.setTitle(title);
    }
}
