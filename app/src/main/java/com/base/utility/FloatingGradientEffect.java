package com.base.utility;

import android.animation.FloatEvaluator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.LinearGradient;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Shader;
import android.text.SpannableString;
import android.text.TextPaint;
import android.text.format.DateUtils;
import android.text.style.CharacterStyle;
import android.text.style.UpdateAppearance;
import android.util.Property;
import android.view.animation.LinearInterpolator;
import android.widget.TextView;

import com.softbondit.news.karu.R;


/**
 * Created by water on 12/2/16.
 */
public class FloatingGradientEffect {
    public static void setEffect(final TextView textView, Context context){
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
            String text = textView.getText().toString();
            AnimatedColorSpan span = new AnimatedColorSpan(context);

            final SpannableString spannableString = new SpannableString(text);
            spannableString.setSpan(span, 0, text.length(), 0);

            ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(
                    span, new Property<AnimatedColorSpan, Float>(Float.class, "ANIMATED_COLOR_SPAN_FLOAT_PROPERTY") {
                        @Override
                        public void set(AnimatedColorSpan span, Float value) {
                            span.setTranslateXPercentage(value);
                        }
                        @Override
                        public Float get(AnimatedColorSpan span) {
                            return span.getTranslateXPercentage();
                        }
                    }, 0, 100);

            objectAnimator.setEvaluator(new FloatEvaluator());
            objectAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animation) {
                    textView.setText(spannableString);
                }
            });
            objectAnimator.setInterpolator(new LinearInterpolator());
            objectAnimator.setDuration(DateUtils.MINUTE_IN_MILLIS * 3);
            objectAnimator.setRepeatCount(ValueAnimator.INFINITE);
            objectAnimator.start();
        }

    }

    private static class AnimatedColorSpan extends CharacterStyle implements UpdateAppearance {
        private final int[] colors;
        private Shader shader = null;
        private Matrix matrix = new Matrix();
        private float translateXPercentage = 0;

        public AnimatedColorSpan(Context context) {
            colors = context.getResources().getIntArray(R.array.rainbow);
        }

        public void setTranslateXPercentage(float percentage) {
            translateXPercentage = percentage;
        }

        public float getTranslateXPercentage() {
            return translateXPercentage;
        }

        @Override
        public void updateDrawState(TextPaint paint) {
            paint.setStyle(Paint.Style.FILL);
            float width = paint.getTextSize() * colors.length;
            if (shader == null) {
                shader = new LinearGradient(0, 0, 0, width, colors, null,
                        Shader.TileMode.MIRROR);
            }
            matrix.reset();
            matrix.setRotate(90);
            matrix.postTranslate(width * translateXPercentage, 0);
            shader.setLocalMatrix(matrix);
            paint.setShader(shader);
        }
    }
}
