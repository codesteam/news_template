package com.base.modarate.view;

import android.app.Activity;
import android.view.View;
import android.widget.LinearLayout;

import com.commonasset.utils.Print;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;

//import com.base.utility.Print;

/**
 * Created by water on 8/11/15.
 */

public class Modarate {

    private InterstitialAd interstitial;
    private AdView mAdView = null;
    AdRequest adRequest;
    long lastRequestTime;
    long adRequestDifferTime = 0;
    Activity activity;
    ZixisngSettings zixisngSettings;
    boolean versionRelease;

    public Modarate(Activity activity, int waitSec, ZixisngSettings zixisngSettings, boolean versionRelease){
        this.activity = activity;
        lastRequestTime = 0;
        adRequestDifferTime = waitSec * 1000;
        this.zixisngSettings = zixisngSettings;
        this.versionRelease = versionRelease;

    }

    private void setIViewFliper()
    {
        interstitial = new InterstitialAd(activity);
        interstitial.setAdUnitId(ZixisngSettings.getInstance(activity).getFliperIApp());
        interstitial.setAdListener(new AdListener() {
            public void onAdLoaded() {
                displayIFliper();
            }
        });
        adRequest = new AdRequest.Builder()
                .build();
        interstitial.loadAd(adRequest);

    }
    public void displayIFliper() {
        if (interstitial.isLoaded()) {
            interstitial.show();
        }
    }

    public void setBViewFliper(final LinearLayout llAdView)
    {
        if (versionRelease){
            this.mAdView = new AdView(activity);
            String value = ZixisngSettings.getInstance(activity).getFliperBApp();

            AdRequest request = new AdRequest.Builder()
                    .build();

            this.mAdView.setAdSize(AdSize.SMART_BANNER);

            this.mAdView.setAdUnitId(value);

            llAdView.addView(this.mAdView);
            this.mAdView.setAdListener(new AdListener() {
                @Override
                public void onAdLoaded() {
                    super.onAdLoaded();
                    setBFliperVisibility(View.VISIBLE);
                }

                @Override
                public void onAdFailedToLoad(int errorCode) {
                    super.onAdFailedToLoad(errorCode);
                }
            });
            this.mAdView.loadAd(request);
            setBFliperVisibility(View.GONE);
            ZixisngSettings.getInstance(activity).setBFliperState(true);
        }
    }

    public void setBFliperVisibility(int v)
    {
        if (versionRelease){
            if (mAdView != null){
                mAdView.setVisibility(v);

            }
        }

    }

    public void onDestroy(){
        if (versionRelease){
            if (interstitial != null) {
                interstitial.setAdListener(null);
            }
            if (mAdView!= null)
            {
                mAdView.destroy();
            }
        }

    }

    public void show(){
        if (versionRelease){
            if (lastRequestTime + adRequestDifferTime <= System.currentTimeMillis()){
                lastRequestTime = System.currentTimeMillis();
                setIViewFliper();
            }
        }
    }

    public void showInstant(){
        if (versionRelease){
            setIViewFliper();
        }
    }
    public void pause(){
        if (mAdView!= null && versionRelease)
        {
            mAdView.pause();

        }
    }

    public void resume(){
        if (mAdView!= null && versionRelease)
        {
            mAdView.resume();
        }
    }

}
