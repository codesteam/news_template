package com.base.modarate.view;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;


public class ZixisngSettings {
	public static String PACKAGE_NAME;
	public static String KEY_FLIPER_I = "KEY_FLIPER_MAX_LOCK_I";
	public static String KEY_FLIPER_B = "KEY_FLIPER_MAX_LOCK_B";
	private String KEY_FLIPER_COUNT = ".KEY_FLIPER_COUNT";
	private String KEY_FLIPER_INTER_OPEN_COUNT = ".KEY_FLIPER_INTER_OPEN_COUNT";
    private String KEY_FLIPER_B_STATE = ".KEY_FLIPER_B_STATE";

    private String KEY_B_FLIPER_APP = ".KEY_B_FLIPER_APP";
	private String KEY_I_FLIPER_APP = ".KEY_I_FLIPER_APP";
	private String KEY_OPEN_COUNT = ".KEY_OPEN_COUNT";
	private String KEY_FLIPER_UP_COUNTER = ".KEY_FLIPER_UP_COUNTER";
	private static ZixisngSettings singleton = null;
	private String KEY_SD_CARD_STATE= ".KEY_SD_CARD_STATE";
	private String KEY_STORAGE_ROOT_PATH = ".KEY_STORAGE_ROOT_PATH";
	private String KEY_FLIPER_PER = ".KEY_FLIPER_PER";
	private SharedPreferences _prefs;
	private SharedPreferences.Editor _editor;
	private static Context context;
	private static Modarate modarate;
	private final int minWaitSec = 5;
	private static IZixing IZixing;
	private ZixisngSettings(Context context) {
		PACKAGE_NAME = context.getPackageName();
		_prefs = PreferenceManager.getDefaultSharedPreferences(context);
		_editor = _prefs.edit();
	}
	public static void createInstance(Activity context){
		ZixisngSettings.context = context;
		singleton = new ZixisngSettings(ZixisngSettings.context);
	}

    public static void setFliperB(Activity context){
        if (!ZixisngSettings.getInstance(context).getBFliperState()){
			IZixing.setBViewFliping();
        }
    }

	public Modarate createViewFliper(Activity context, IZixing IZixing, String unitIDBanner, String unitIDInter, int waitSec, boolean type){

		ZixisngSettings.getInstance(context).setOpenCount(ZixisngSettings.getInstance(context).getOpenCount() + 1);
		ZixisngSettings.getInstance(context).setInterOpenCount(0);
		ZixisngSettings.getInstance(context).setFliperBApp(unitIDBanner);
		ZixisngSettings.getInstance(context).setFliperIApp(unitIDInter);
        ZixisngSettings.getInstance(context).setBFliperState(false);
		this.IZixing = IZixing;


		if (waitSec < minWaitSec){
			waitSec = minWaitSec;
		}
		return modarate = new Modarate(context, waitSec, this, type);
	}

	public Modarate getViewFliper(){
		return modarate;
	}
	public static ZixisngSettings getInstance(Context context) {
		if (singleton == null){
			ZixisngSettings.context = context;
			singleton = new ZixisngSettings(context);
		}
		return singleton;
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {

		return new CloneNotSupportedException();
	}

	public int getFliperCount() {

		return _prefs.getInt(PACKAGE_NAME + KEY_FLIPER_COUNT, 0);
	}

	public void setFliperCount(int value) {

		_editor.putInt(PACKAGE_NAME + KEY_FLIPER_COUNT, value);
		_editor.commit();
	}


	public int getInterOpenCount() {

		return _prefs.getInt(PACKAGE_NAME + KEY_FLIPER_INTER_OPEN_COUNT, 1);
	}

	public void setInterOpenCount(int value) {

		_editor.putInt(PACKAGE_NAME + KEY_FLIPER_INTER_OPEN_COUNT, value);
		_editor.commit();
	}

	public int getOpenCount() {

		return _prefs.getInt(PACKAGE_NAME + KEY_OPEN_COUNT, -1);
	}

	public void setOpenCount(int value) {

		_editor.putInt(PACKAGE_NAME + KEY_OPEN_COUNT, value);
		_editor.commit();
	}
	public void setString(String key, String value){
		Log.e("set String:", PACKAGE_NAME +key);
		_editor.putString(PACKAGE_NAME +key, value);
		_editor.commit();
	}

	public String getString(String key){
		return _prefs.getString(PACKAGE_NAME +key, null);
	}

	public void setBoolean(String key, boolean value){
		_editor.putBoolean(PACKAGE_NAME +key, value);
		_editor.commit();
	}

	public boolean getBoolean(String key){
		return _prefs.getBoolean(PACKAGE_NAME +key, false);
	}
	public void setFliperBApp(String value){
		_editor.putString(PACKAGE_NAME + KEY_B_FLIPER_APP, value);
		_editor.commit();
	}

	public String getFliperBApp(){
		return _prefs.getString(PACKAGE_NAME + KEY_B_FLIPER_APP, null);

	}

	public void setFliperIApp(String value){
		_editor.putString(PACKAGE_NAME + KEY_I_FLIPER_APP, value);
		_editor.commit();
	}

	public String getFliperIApp(){
		return _prefs.getString(PACKAGE_NAME + KEY_I_FLIPER_APP, null);

	}

	public void setPer(int value){
		_editor.putInt(PACKAGE_NAME + KEY_FLIPER_PER, value);
		_editor.commit();
	}

	public int getPer(){
		return _prefs.getInt(PACKAGE_NAME + KEY_FLIPER_PER, 5);

	}

	public void setSDCardState(boolean value)
	{
		_editor.putBoolean(PACKAGE_NAME + KEY_SD_CARD_STATE, value);
		_editor.commit();
	}

	public boolean getSDCardState()
	{
		return _prefs.getBoolean(PACKAGE_NAME + KEY_SD_CARD_STATE, true);

	}

	public String getStorageRootPath() {

		return _prefs.getString(PACKAGE_NAME + KEY_STORAGE_ROOT_PATH, null);
	}

	public void setStorageRootPath(String value)
	{
		_editor.putString(PACKAGE_NAME + KEY_STORAGE_ROOT_PATH, value);
		_editor.commit();
	}

    public boolean getBFliperState() {

        return _prefs.getBoolean(PACKAGE_NAME + KEY_FLIPER_B_STATE, false);
    }

    public void setBFliperState(boolean value)
    {
        _editor.putBoolean(PACKAGE_NAME + KEY_FLIPER_B_STATE, value);
        _editor.commit();
    }

	public void setFliperUpCounter(int value){
		_editor.putInt(PACKAGE_NAME + KEY_FLIPER_UP_COUNTER, value);
		_editor.commit();
	}

	public int getFliperUpCounter(){
		return _prefs.getInt(PACKAGE_NAME + KEY_FLIPER_UP_COUNTER, -1);

	}
}
