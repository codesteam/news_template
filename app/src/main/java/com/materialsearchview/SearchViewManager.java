package com.materialsearchview;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Toast;

import com.db.dao.EntryDao;
import com.softbondit.news.karu.R;
import com.materialsearchview.db.SearchItem;

import java.util.List;

/**
 * Created by water on 1/13/17.
 */

public class SearchViewManager {
    private MaterialSearchView searchView;
    private static SearchViewManager searchViewManager;
    Context context;
    OnSearchItemClick onSearchItemClick;
    public SearchViewManager(Context context){
        this.context = context;
    }


    public  MaterialSearchView getSearchView() {
        return searchView;
    }

    public void create(){ // only call from base MainActivity,
        if (searchView == null){
            searchView = new MaterialSearchView(context);
            searchView.setLayoutParams(new ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT));

            initSearchView(context);
        }
    }

    public void setSearchItemClickListner(OnSearchItemClick onSearchItemClick){
        this.onSearchItemClick = onSearchItemClick;
    }
    public void removeParrent(){
        if (searchView.getParent() != null){
            ((ViewGroup)searchView.getParent()).removeView(searchView);
        }
    }

    public void addParrent(ViewGroup container){
        if (searchView.getParent() != null){
            ((ViewGroup)searchView.getParent()).removeView(searchView);
        }
        container.addView(searchView);
    }
    public static SearchViewManager getInstance(Context context){
        if (searchViewManager == null){
            searchViewManager = new SearchViewManager(context);
        }
        return searchViewManager;
    }


    private  void initSearchView(final Context context){
        searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        searchView.setSearchViewListener(new MaterialSearchView.SearchViewListener() {
            @Override
            public void onSearchViewOpened() {
                // Do something once the view is open.
            }

            @Override
            public void onSearchViewClosed() {
                // Do something once the view is closed.
            }
        });

        searchView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // Do something when the suggestion list is clicked.
                SearchItem suggestion = searchView.getSuggestionAtPosition(position);
                searchView.closeSearch();
                onSearchItemClick.onClick(suggestion);

            }
        });

        searchView.adjustTintAlpha(0.8f);
        searchView.setHintTextColor(ContextCompat.getColor(context, R.color.text1_40_percent_inverse));
        searchView.setBackgroundColor(ContextCompat.getColor(context, R.color.bg2));
        searchView.setTextColor(ContextCompat.getColor(context, R.color.text1_95_percent_inverse));
        searchView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                Toast.makeText(context, "Long clicked position: " + i, Toast.LENGTH_SHORT).show();
                return true;
            }
        });

        searchView.setOnVoiceClickedListener(new MaterialSearchView.OnVoiceClickedListener() {
            @Override
            public void onVoiceClicked() {
                Toast.makeText(context, "Voice clicked!", Toast.LENGTH_SHORT).show();
            }
        });

        searchView.setShouldKeepHistory(false);
        EntryDao entryDao = new EntryDao(context);
        List<SearchItem> entryBindings = entryDao.getSearchItems();
        if (entryBindings != null){
            searchView.addSuggestions(entryBindings);
        }

    }
}
