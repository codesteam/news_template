package com.materialsearchview;

import com.materialsearchview.db.SearchItem;

/**
 * Created by water on 1/17/17.
 */

public interface OnSearchItemClick {
    void onClick(SearchItem searchItem);
}
