package com.materialsearchview.db;

/**
 * Created by water on 1/12/17.
 */

public class SearchItem {
    long id;
    long catId; // this should not be Box Premetive, box premitive can not be stored int db
    long pubDate;
    int mediaType;
    String title;
    String tumbUrl;
    String videoUrl;
    String videoDuration;

    public SearchItem(long id){
        this.id = id;
    }
    public SearchItem(long id, long pubDate, String title, int mediaType){
        this.id = id;
        this.pubDate = pubDate;
        this.title = title;
        this.mediaType = mediaType;
    }


    public long getCatId() { // for some reason its return 0
        return catId;
    }

    public long getId() {
        return id;
    }

    public long getPubDate() {
        return pubDate;
    }

    public String getTitle() {
        return title;
    }

    public String getTumbUrl() {
        return tumbUrl;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setTumbUrl(String tumbUrl) {
        this.tumbUrl = tumbUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public void setCatId(long catId) {
        this.catId = catId;
    }

    public int getMediaType() {
        return mediaType;
    }

    public void setVideoDuration(String videoDuration) {
        this.videoDuration = videoDuration;
    }

    public String getVideoDuration() {
        return videoDuration;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setMediaType(int mediaType) {
        this.mediaType = mediaType;
    }

    public void setPubDate(long pubDate) {
        this.pubDate = pubDate;
    }

}
