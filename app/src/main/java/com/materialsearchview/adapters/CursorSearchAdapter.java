package com.materialsearchview.adapters;

import android.content.Context;
import android.database.Cursor;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.commonasset.utils.GlideImageRenderer;
import com.commonasset.utils.TimestampConverter;
import com.softbondit.news.karu.R;
import com.materialsearchview.db.HistoryContract;
import com.materialsearchview.db.SearchItem;

/**
 * Created by mauker on 19/04/2016.
 *
 * Default adapter used for the suggestion/history ListView.
 */
public class CursorSearchAdapter extends CursorAdapter {

    Context context;
    @RequiresApi(api = Build.VERSION_CODES.HONEYCOMB)
    public CursorSearchAdapter(Context context, Cursor cursor, int flags) {
        super(context, cursor, 0);
        this.context = context;
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.list_item_serach_view, parent, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        ListViewHolder vh = new ListViewHolder(view);
        view.setTag(vh);
        vh.searchTitle.setText(cursor.getString(cursor.getColumnIndexOrThrow(HistoryContract.HistoryEntry.COLUMN_QUERY))+"");
        vh.searchPubTime.setText(TimestampConverter.parseDate(cursor.getString(cursor.getColumnIndexOrThrow(HistoryContract.HistoryEntry.COLUMN_IS_PUB_DATE))+""));
        GlideImageRenderer.renderThumbnail(context, vh.searchImage,
                cursor.getString(cursor.getColumnIndexOrThrow(HistoryContract.HistoryEntry.COLUMN_IS_THUMBNAIL_URL)), null);
    }

    @Override
    public SearchItem getItem(int position) {
        Cursor cursor = getCursor();
        SearchItem searchItem = null;
        if(cursor.moveToPosition(position)) {
            searchItem = new SearchItem( (long)cursor.getInt(cursor.getColumnIndexOrThrow(HistoryContract.HistoryEntry.COLUMN_IS_NEWS_ID)));
        }
        return searchItem;
    }

    private class ListViewHolder {
        ImageView searchImage;
        TextView searchTitle;
        TextView searchPubTime;

        public ListViewHolder(View convertView) {
            searchImage = (ImageView) convertView.findViewById(R.id.searchImage);
            searchTitle = (TextView) convertView.findViewById(R.id.searchTitle);
            searchPubTime = (TextView) convertView.findViewById(R.id.searchPostedTime);
        }
    }
}
