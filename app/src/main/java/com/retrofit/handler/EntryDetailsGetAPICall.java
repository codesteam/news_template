package com.retrofit.handler;


import android.content.Context;

import com.base.components.RenderRecognizer;
import com.commonasset.utils.Print;
import com.retrofit.api.APICallHandler;
import com.retrofit.api.APIClientResponse;
import com.retrofit.response.entry.details.EntryDetailsResponce;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class EntryDetailsGetAPICall extends APICallHandler<String> {

	private final static EntryDetailsGetAPICall singleton = new EntryDetailsGetAPICall();

	private EntryDetailsGetAPICall() {
	}

	public static EntryDetailsGetAPICall getInstance() {

		return singleton;
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {

		return new CloneNotSupportedException();
	}

	@Override
	public void callAPI(final Context context, final RenderRecognizer renderRecognizer, final APIClientResponse callback, final String... args) {

		apis.getEntryDetails(args[0],args[1],true, new Callback<EntryDetailsResponce>() {

			@Override
			public void failure(RetrofitError arg0) {

				if (callback != null) {
					callback.failureOnApiCall("failed", arg0);
				}
			}

			@Override
			public void success(EntryDetailsResponce arg0,
								Response arg1) {
				Print.e(this, arg0.getEntryDetails().getDetails());
				/*EntryDao entryDao = new EntryDao(context);
				entryDao.insert(arg0.getEntries(),  Long.valueOf(args[0]));*/

				if (callback != null) {
					callback.successOnApiCall("success", arg0);
				}
			}
		});


	}

}
