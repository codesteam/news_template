package com.retrofit.handler;


import android.content.Context;

import com.base.components.RenderRecognizer;
import com.commonasset.utils.Print;
import com.db.dao.GroupDaoTemp;
import com.retrofit.api.APICallHandler;
import com.retrofit.api.APIClientResponse;
import com.retrofit.response.home.Group;
import com.retrofit.response.home.HomeResponce;

import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class HomeGetAPICall extends APICallHandler<String> {

	public static final String SUCCESS_ENTRY_RESPONCE = "SU_EN_RES";
	private final static HomeGetAPICall singleton = new HomeGetAPICall();

	private HomeGetAPICall() {
	}

	public static HomeGetAPICall getInstance() {

		return singleton;
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {

		return new CloneNotSupportedException();
	}

	@Override
	public void callAPI(final Context context, final RenderRecognizer renderRecognizer, final APIClientResponse callback, final String... args) {

		/*@Query("categories") String category,
		@Query("startTimestamp") String startTimestamp,
		@Query("endTimestamp") String endTimestamp,
		@Query("nextPageToken") String nextPageToken,
		@Query("maxResults") String maxResults,
		@Query("notification") String notification,
*/

		apis.getHome(new Callback<HomeResponce>() {

			@Override
			public void failure(RetrofitError arg0) {
				Print.e(this, "Failed");
				if (callback != null) {
					callback.failureOnApiCall("failed", arg0);
				}
			}

			@Override
			public void success(HomeResponce arg0,
								Response arg1) {
				Print.e(this, "Successfull");
				GroupDaoTemp groupDaoTemp = new GroupDaoTemp(context);
				List<Group> groups = arg0.getGroups();
				if (groups != null && groups.size() > 0){
					groupDaoTemp.deleteAllRows();
					for (int i = 0; i < groups.size(); i++){
						groupDaoTemp.insert(groups.get(i));
					}
				}
				/*EntryDao entryDao = new EntryDao(context);
				List<Entry> entries = arg0.getEntries();
				List<Long> entriesId = new ArrayList<Long>();
				if (entries != null){
					List<Entry> insertList = new ArrayList<Entry>();
					List<Entry> updateList = new ArrayList<Entry>();
					for (Entry entry: entries){
						if (entryDao.exist(entry.getId()) > 0){
							updateList.add(entry);
							//Print.e(this, "update Entry");
						}else{
							insertList.add(entry);
							//Print.e(this, "insert Entry");
						}
						entriesId.add(entry.getId());
					}
					entryDao.insert(insertList,  args[0]);
					Print.e(this, "update list size: "+ updateList.size()+"");
					entryDao.update(updateList,  args[0]);
				}

				// delete: deleting entries from status call*/

				if (callback != null) {
					callback.successOnApiCall(SUCCESS_ENTRY_RESPONCE, arg0);
				}
			}
		});


	}

}
