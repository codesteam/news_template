package com.retrofit.handler;


import android.content.Context;

import com.base.components.RenderRecognizer;
import com.commonasset.utils.Print;
import com.db.dao.CategoryDao;
import com.db.dao.EntryDao;
import com.db.dao.StatusDao;
import com.retrofit.api.APICallHandler;
import com.retrofit.api.APIClientResponse;
import com.retrofit.response.status.Status;
import com.retrofit.response.status.StatusCallback;

import java.util.HashMap;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class StatusGetAPICall extends APICallHandler<String> {

	private final static StatusGetAPICall singleton = new StatusGetAPICall();
	private long startTimeStamp = 0;
	private StatusGetAPICall() {
	}

	public static StatusGetAPICall getInstance() {

		return singleton;
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {

		return new CloneNotSupportedException();
	}

	@Override
	public void callAPI(final Context context, final RenderRecognizer renderRecognizer, final APIClientResponse callback, String... args) {

		apis.getStatus(new Callback<StatusCallback>() {

			@Override
			public void failure(RetrofitError arg0) {

				if (callback != null) {
					callback.failureOnApiCall("failed", arg0);
				}
			}

			@Override
			public void success(StatusCallback arg0,
								Response arg1) {
				StatusDao statusDao = new StatusDao(context);
				List<Status> statuses = arg0.getStatus();
				HashMap<String, Long> dbStatus = statusDao.getStatuses();

				if (statuses!= null && statuses.size() > 0){
					for (final Status status: arg0.getStatus()){
						Long value = dbStatus.get(status.getTag());
						if (value == null){
							statusDao.insert(status);
							//Print.e(this, "inserting status into local db");
						}else{
							if (value - 5000< status.getModifiedAt()){
								update(context, callback, renderRecognizer, statusDao, status, value);

							}else{
								Print.e(this, "Local Db already updated");
							}
						}
						if (status.getDeleted() != null){
							delete(context, renderRecognizer, status.getTag(), status.getDeleted());
						}

					}
				}
				if (callback != null) {
					callback.successOnApiCall("success", arg0);
				}
			}

			private void update(Context context, final APIClientResponse callBack, final RenderRecognizer renderRecognizer,final StatusDao statusDao, final Status status, Long lastUpdatedTime){
				switch (status.getTag()){
					case "category":
						Print.e(this, "updating status category");
						CategoryGetAPICall.getInstance().callAPI(context, renderRecognizer, new APIClientResponse() {
						@Override
						public void failureOnApiCall(String msg, Object sender) {

						}

						@Override
						public void failureOnNetworkConnection(String msg, Object sender) {

						}

						@Override
						public void successOnApiCall(String msg, Object sender) {
							statusDao.update(status);
						}
					},lastUpdatedTime+"", status.getModifiedAt()+"", null, null);
						break;
					case "entry":
						EntryGetAPICall.getInstance().callAPI(context, null, new APIClientResponse() {
									@Override
									public void failureOnApiCall(String msg, Object sender) {

									}

									@Override
									public void failureOnNetworkConnection(String msg, Object sender) {

									}

									@Override
									public void successOnApiCall(String msg, Object sender) {
										statusDao.update(status);
										if (callBack != null){
											callBack.successOnApiCall(msg, sender); // returning Entrylist to StatusReceiver for pushing them to notification manager
										}
									}
								},
								null, lastUpdatedTime + "", status.getModifiedAt() + "", null, null, "1");
						break;
				}
			}

			private void delete(Context context, final RenderRecognizer renderRecognizer,  String tag,List<Long> ids){
				if (ids != null){
					switch (tag){
						case "category":
							CategoryDao categoryDao = new CategoryDao(context);
							for (Long id: ids){
								categoryDao.delete(id, renderRecognizer);
							}
							break;
						case "entry":
							EntryDao entryDao = new EntryDao(context);
							entryDao.delete(ids);
							break;
					}
				}

			}
		});
	}

}
