package com.retrofit.handler;


import android.content.Context;

import com.base.common.CommonConstants;
import com.base.components.RenderRecognizer;
import com.commonasset.utils.Print;
import com.db.dao.CategoryDao;
import com.retrofit.api.APICallHandler;
import com.retrofit.api.APIClientResponse;
import com.retrofit.response.category.Category;
import com.retrofit.response.category.CategoryResponse;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class CategoryGetAPICall extends APICallHandler<String> {

	private final static CategoryGetAPICall singleton = new CategoryGetAPICall();

	private CategoryGetAPICall() {
	}

	public static CategoryGetAPICall getInstance() {

		return singleton;
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {

		return new CloneNotSupportedException();
	}

	@Override
	public void callAPI(final Context context, final RenderRecognizer renderRecognizer, final APIClientResponse callback, String... args) {
		/*@Query("startTimestamp") String startTimestamp,
		@Query("endTimestamp") String endTimestamp,
		@Query("nextPageToken") String nextPageToken,
		@Query("maxResults") String maxResults,*/
		apis.getCategory(args[0], args[1], args[2], args[3], new Callback<CategoryResponse>() {

			@Override
			public void failure(RetrofitError arg0) {

				if (callback != null) {
					callback.failureOnApiCall("failed", arg0);
				}
			}

			@Override
			public void success(CategoryResponse arg0,
								Response arg1) {


				CategoryDao categoryDao = new CategoryDao(context);
				ArrayList<Category> categories = (ArrayList<Category>) arg0.getCategories();

				if (categories != null){
					Category catHome = new Category();
					catHome.setId((long) CommonConstants.DEFAULT_CAT_HOME_ID);
					catHome.setParent(null);
					catHome.setOrder(0);
					catHome.setTitle("প্রচ্ছদ");
					categoryDao.insert(catHome, renderRecognizer); // adding default home category

					for(Category category: categories){
						/*if (categoryDao.exist(category.getId())> 0){
							categoryDao.update(category, renderRecognizer);
						}else{
							categoryDao.insert(category, renderRecognizer);
						}*/
						Print.e(this, "inserting.."+category.getId());
						categoryDao.insert(category, renderRecognizer);

					}
				}
				//dbHelper.insertCatsSubCats((ArrayList<Category>) arg0.getCategories());
				if (callback != null) {
					callback.successOnApiCall("success", arg0);
				}
			}
		});


	}

}
