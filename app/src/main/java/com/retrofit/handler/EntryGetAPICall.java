package com.retrofit.handler;


import android.content.Context;

import com.base.components.RenderRecognizer;
import com.commonasset.utils.Print;
import com.db.dao.EntryDao;
import com.retrofit.api.APICallHandler;
import com.retrofit.api.APIClientResponse;
import com.retrofit.response.entry.Entry;
import com.retrofit.response.entry.EntryResponce;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class EntryGetAPICall extends APICallHandler<String> {

	public static final String SUCCESS_ENTRY_RESPONCE = "SU_EN_RES";
	private final static EntryGetAPICall singleton = new EntryGetAPICall();

	private EntryGetAPICall() {
	}

	public static EntryGetAPICall getInstance() {

		return singleton;
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {

		return new CloneNotSupportedException();
	}

	@Override
	public void callAPI(final Context context, final RenderRecognizer renderRecognizer, final APIClientResponse callback, final String... args) {

		/*@Query("categories") String category,
		@Query("startTimestamp") String startTimestamp,
		@Query("endTimestamp") String endTimestamp,
		@Query("nextPageToken") String nextPageToken,
		@Query("maxResults") String maxResults,
		@Query("notification") String notification,
*/

		apis.getEntry(args[0], args[1], args[2], args[3], args[4], args[5], new Callback<EntryResponce>() {

			@Override
			public void failure(RetrofitError arg0) {
				Print.e(this, "Failed");
				if (callback != null) {
					callback.failureOnApiCall("failed", arg0);
				}
			}

			@Override
			public void success(EntryResponce arg0,
								Response arg1) {
				Print.e(this, "Successfull");
				EntryDao entryDao = new EntryDao(context);
				List<Entry> entries = arg0.getEntries();
				//List<Long> entriesId = new ArrayList<Long>();
				if (entries != null){
					List<Entry> insertList = new ArrayList<Entry>();
					List<Entry> updateList = new ArrayList<Entry>();
					for (Entry entry: entries){
						if (entryDao.exist(entry.getId()) > 0){
							updateList.add(entry);
							//Print.e(this, "update Entry");
						}else{
							insertList.add(entry);
							//Print.e(this, "insert Entry");
						}
						//entriesId.add(entry.getId());
					}
					entryDao.insert(insertList,  args[0]);
					Print.e(this, "update list size: "+ updateList.size()+"");
					entryDao.update(updateList,  args[0]);
				}

				// delete: deleting entries from status call

				if (callback != null) {
					callback.successOnApiCall(SUCCESS_ENTRY_RESPONCE, arg0);
				}
			}
		});


	}

}
