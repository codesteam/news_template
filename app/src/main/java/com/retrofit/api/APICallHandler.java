package com.retrofit.api;


import android.content.Context;

import com.base.components.RenderRecognizer;

public abstract class APICallHandler<T> {

	protected APIs apis = APIHandler.getApiInterface();

	abstract public void callAPI(final Context context, final RenderRecognizer renderRecognizer,
								 final APIClientResponse callback, T... args);
}
