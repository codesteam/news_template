package com.retrofit.api;

import com.retrofit.response.category.CategoryResponse;
import com.retrofit.response.entry.EntryResponce;
import com.retrofit.response.entry.details.EntryDetailsResponce;
import com.retrofit.response.home.HomeResponce;
import com.retrofit.response.status.StatusCallback;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;


public interface APIs {
	@GET("/api/v1/status/")
	public void getStatus(
			Callback<StatusCallback> callback);
	@GET("/api/v1/categories/")
	public void getCategory(
			@Query("startTimestamp") String startTimestamp,
			@Query("endTimestamp") String endTimestamp,
			@Query("nextPageToken") String nextPageToken,
			@Query("maxResults") String maxResults,
			Callback<CategoryResponse> callback);
	@GET("/test.json")
	public void getTestCategory(
			Callback<CategoryResponse> callback);
	@GET("/api/v1/entries/")
	public void getEntry(
			@Query("categories") String category,
			@Query("startTimestamp") String startTimestamp,
			@Query("endTimestamp") String endTimestamp,
			@Query("nextPageToken") String nextPageToken,
			@Query("maxResults") String maxResults,
			@Query("notification") String notification,
			Callback<EntryResponce> callback);
	@GET("/api/v1/entries/{id}/")
	public void getEntryDetails(
			@Path("id") String id,
			@Query("startTimestamp") String startTimestamp,
			@Query("details") boolean details,
			Callback<EntryDetailsResponce> callback);

	@GET("/api/v1/system/index/")
	public void getHome(
			Callback<HomeResponce> callback);
}
