package com.retrofit.api;

import android.util.Log;

import java.net.SocketTimeoutException;

import retrofit.ErrorHandler;
import retrofit.Profiler;
import retrofit.RestAdapter;
import retrofit.RestAdapter.LogLevel;
import retrofit.RetrofitError;

public class APIHandler {

	public static final String TEST_API_URL = "http://45.79.69.207/crowcode/";
	public static final String API_URL = "http://karunews24.com/";
	private static RestAdapter restAdapter;
	public static final int CONNECTION_TIME_OUT = 2;
	
    
	private static RestAdapter getRestAdapter() {
		if (restAdapter == null) {
			
			restAdapter = new RestAdapter.Builder()
		    .setErrorHandler(new ErrorRetrofitHandlerException())
		    .setEndpoint(API_URL)
		    .setLogLevel(LogLevel.FULL)
			//.setProfiler(new CustomProfiler())
		   // .setClient(new OkClient(getClient()))
		    .build();

		}

		return restAdapter;
	}

	public static APIs getApiInterface() {

		APIs apiS = null;

		try {
			if (restAdapter == null) {
				restAdapter = getRestAdapter();
			}

			apiS = restAdapter.create(APIs.class);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return apiS;
	}
	
	static class ErrorRetrofitHandlerException implements ErrorHandler{
	    @Override
	    public Throwable handleError(RetrofitError cause) {
	        if (cause.isNetworkError()) {
	            if (cause.getCause() instanceof SocketTimeoutException) {
	                Log.e("Retrofit Error handler", "SocketTimeoutException, url=" + cause.getUrl());
	            } else {
	                Log.e("Retrofit Error handler", "NoConnectionException, url=" + cause.getUrl());
	            }
	        } else {
	            Log.e("Retrofit Error handler", "Error status:" + cause.getResponse().getStatus());
	        }

	        return cause;
	    }
	}
	
	static class CustomProfiler implements Profiler<Object>{

		@Override
		public void afterCall(RequestInformation requestInfo,
				long elapsedTime, int statusCode, Object beforeCallData) {
			Log.e("API Handler", String.format("HTTP %d %s %s (%dms) %s",
					statusCode, requestInfo.getMethod(), requestInfo.getRelativePath(), elapsedTime, beforeCallData.toString()));
		}

		@Override
		public Object beforeCall() {
			// TODO Auto-generated method stub
			return new String("Custom profiler");
		}
		
	}
}
