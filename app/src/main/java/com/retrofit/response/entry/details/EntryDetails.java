
package com.retrofit.response.entry.details;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.retrofit.response.entry.Medium;

import java.util.List;

public class EntryDetails {

    @SerializedName("id")
    @Expose
    private Long id;
    @SerializedName("entry_type")
    @Expose
    private Long entryType;
    @SerializedName("related")
    @Expose
    private List<Long> related = null;
    @SerializedName("categories")
    @Expose
    private List<Long> categories = null;
    @SerializedName("media")
    @Expose
    private List<Medium> media = null;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("short_description")
    @Expose
    private String shortDescription;
    @SerializedName("details")
    @Expose
    private String details;
    @SerializedName("pub_date")
    @Expose
    private Long pubDate;
    @SerializedName("created_at")
    @Expose
    private Long createdAt;
    @SerializedName("modified_at")
    @Expose
    private Long modifiedAt;

    /**
     * 
     * @return
     *     The id
     */
    public Long getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The entryType
     */
    public Long getEntryType() {
        return entryType;
    }

    /**
     * 
     * @param entryType
     *     The entry_type
     */
    public void setEntryType(Long entryType) {
        this.entryType = entryType;
    }

    /**
     * 
     * @return
     *     The categories
     */
    public List<Long> getCategories() {
        return categories;
    }

    /**
     * 
     * @param categories
     *     The categories
     */
    public void setCategories(List<Long> categories) {
        this.categories = categories;
    }
    /**
     *
     * @return
     *     The related
     */
    public List<Long> getRelated() {
        return related;
    }

    /**
     *
     * @param related
     *     The related
     */
    public void setRelated(List<Long> related) {
        this.related = related;
    }

    /**
     * 
     * @return
     *     The media
     */
    public List<Medium> getMedia() {
        return media;
    }

    /**
     * 
     * @param media
     *     The media
     */
    public void setMedia(List<Medium> media) {
        this.media = media;
    }

    /**
     * 
     * @return
     *     The title
     */
    public String getTitle() {
        return title;
    }

    /**
     * 
     * @param title
     *     The title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * 
     * @return
     *     The shortDescription
     */
    public String getShortDescription() {
        return shortDescription;
    }

    /**
     * 
     * @param shortDescription
     *     The short_description
     */
    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    /**
     * 
     * @return
     *     The details
     */
    public String getDetails() {
        return details;
    }

    /**
     * 
     * @param details
     *     The details
     */
    public void setDetails(String details) {
        this.details = details;
    }

    /**
     * 
     * @return
     *     The pubDate
     */
    public Long getPubDate() {
        return pubDate;
    }

    /**
     * 
     * @param pubDate
     *     The pub_date
     */
    public void setPubDate(Long pubDate) {
        this.pubDate = pubDate;
    }

    /**
     * 
     * @return
     *     The createdAt
     */
    public Long getCreatedAt() {
        return createdAt;
    }

    /**
     * 
     * @param createdAt
     *     The created_at
     */
    public void setCreatedAt(Long createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * 
     * @return
     *     The modifiedAt
     */
    public Long getModifiedAt() {
        return modifiedAt;
    }

    /**
     * 
     * @param modifiedAt
     *     The modified_at
     */
    public void setModifiedAt(Long modifiedAt) {
        this.modifiedAt = modifiedAt;
    }

}
