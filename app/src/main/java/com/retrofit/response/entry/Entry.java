
package com.retrofit.response.entry;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Entry implements Serializable {

    @SerializedName("id")
    @Expose
    private Long id;
    @SerializedName("media_type")
    @Expose
    private int mediaType;
    @SerializedName("media")
    @Expose
    private List<Medium> media = null;
    @SerializedName("entry_type")
    @Expose
    private Long entryType;
    @SerializedName("categories")
    @Expose
    private List<Long> categories = null;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("short_description")
    @Expose
    private String shortDescription;
    @SerializedName("pub_date")
    @Expose
    private Long pubDate;
    @SerializedName("created_at")
    @Expose
    private Long createdAt;
    @SerializedName("modified_at")
    @Expose
    private Long modifiedAt;
    @SerializedName("order")
    @Expose
    private Integer order;
    @SerializedName("deleted")
    @Expose
    private List<Integer> deleted = null;
    /**
     * 
     * @return
     *     The id
     */
    public Long getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The mediaType
     */
    public int getMediaType() {
        return mediaType;
    }

    /**
     * 
     * @param mediaType
     *     The media_type
     */
    public void setMediaType(int mediaType) {
        this.mediaType = mediaType;
    }

    /**
     * 
     * @return
     *     The media
     */
    public List<Medium> getMedia() {
        return media;
    }

    /**
     * 
     * @param media
     *     The media
     */
    public void setMedia(List<Medium> media) {
        this.media = media;
    }

    /**
     * 
     * @return
     *     The entryType
     */
    public Long getEntryType() {
        return entryType;
    }

    /**
     * 
     * @param entryType
     *     The entry_type
     */
    public void setEntryType(Long entryType) {
        this.entryType = entryType;
    }

    /**
     * 
     * @return
     *     The categories
     */
    public List<Long> getCategories() {
        return categories;
    }

    /**
     * 
     * @param categories
     *     The categories
     */
    public void setCategories(List<Long> categories) {
        this.categories = categories;
    }

    /**
     * 
     * @return
     *     The title
     */
    public String getTitle() {
        return title;
    }

    /**
     * 
     * @param title
     *     The title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * 
     * @return
     *     The shortDescription
     */
    public String getShortDescription() {
        return shortDescription;
    }

    /**
     * 
     * @param shortDescription
     *     The short_description
     */
    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    /**
     * 
     * @return
     *     The pubDate
     */
    public Long getPubDate() {
        return pubDate;
    }

    /**
     * 
     * @param pubDate
     *     The pub_date
     */
    public void setPubDate(Long pubDate) {
        this.pubDate = pubDate;
    }

    /**
     * 
     * @return
     *     The createdAt
     */
    public Long getCreatedAt() {
        return createdAt;
    }

    /**
     * 
     * @param createdAt
     *     The created_at
     */
    public void setCreatedAt(Long createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * 
     * @return
     *     The modifiedAt
     */
    public Long getModifiedAt() {
        return modifiedAt;
    }

    /**
     * 
     * @param modifiedAt
     *     The modified_at
     */
    public void setModifiedAt(Long modifiedAt) {
        this.modifiedAt = modifiedAt;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }
    public List<Integer> getDeleted() {
        return deleted;
    }

    public void setDeleted(List<Integer> deleted) {
        this.deleted = deleted;
    }

    private Long primaryCatId = null;

    public void setPrimaryCatId(Long primaryCatId) {
        this.primaryCatId = primaryCatId;
    }

    public Long getPrimaryCatId() {
        return primaryCatId;
    }

    private String catName;

    public void setCatName(String catName) {
        this.catName = catName;
    }

    public String getCatName() {
        return catName;
    }
}
