
package com.retrofit.response.entry.details;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EntryDetailsResponce {

    @SerializedName("entry_details")
    @Expose
    private EntryDetails entryDetails;

    /**
     * 
     * @return
     *     The entryDetails
     */
    public EntryDetails getEntryDetails() {
        return entryDetails;
    }

    /**
     * 
     * @param entryDetails
     *     The entry_details
     */
    public void setEntryDetails(EntryDetails entryDetails) {
        this.entryDetails = entryDetails;
    }

}
