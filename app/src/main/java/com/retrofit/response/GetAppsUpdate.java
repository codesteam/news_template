
package com.retrofit.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetAppsUpdate {

    @SerializedName("version")
    @Expose
    private Long version;

    /**
     * 
     * @return
     *     The version
     */
    public Long getVersion() {
        return version;
    }

    /**
     * 
     * @param version
     *     The version
     */
    public void setVersion(Long version) {
        this.version = version;
    }


}
