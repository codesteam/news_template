
package com.retrofit.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Result {

    @SerializedName("status")
    @Expose
    private Long status;
    @SerializedName("priority")
    @Expose
    private Long priority;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("pakage")
    @Expose
    private String pakage;
    @SerializedName("icon_url")
    @Expose
    private String iconUrl;

    @SerializedName("app_name")
    @Expose
    private String appName;

    @SerializedName("size")
    @Expose
    private String size;
    /**
     * 
     * @return
     *     The status
     */
    public Long getStatus() {
        return status;
    }

    /**
     * 
     * @param status
     *     The status
     */
    public void setStatus(Long status) {
        this.status = status;
    }

    /**
     *
     * @return
     *     The priority
     */
    public Long getPriority() {
        return priority;
    }

    /**
     *
     * @param priority
     *     The priority
     */
    public void setPriority(Long priority) {
        this.priority = priority;
    }

    /**
     * 
     * @return
     *     The message
     */
    public String getMessage() {
        return message;
    }

    /**
     * 
     * @param message
     *     The message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * 
     * @return
     *     The pakage
     */
    public String getPakage() {
        return pakage;
    }

    /**
     * 
     * @param pakage
     *     The pakage
     */
    public void setPakage(String pakage) {
        this.pakage = pakage;
    }

    /**
     * 
     * @return
     *     The iconUrl
     */
    public String getIconUrl() {
        return iconUrl;
    }

    /**
     * 
     * @param iconUrl
     *     The icon_url
     */
    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

    /**
     *
     * @return
     *     The appName
     */
    public String getAppName() {
        return appName;
    }

    /**
     *
     * @param appName
     *     The app_name
     */
    public void setAppName(String appName) {
        this.appName = appName;
    }

    /**
     *
     * @return
     *     The size
     */
    public String getSize() {
        return size;
    }

    /**
     *
     * @param size
     *     The size
     */
    public void setSize(String size) {
        this.size = size;
    }
}
