package com.db.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.commonasset.utils.Print;
import com.db.DBHelper;
import com.retrofit.response.category.Category;

/**
 * Created by water on 12/28/16.
 */

public class SubCategoryDao {
    public static String ID = "id";
    public static String CAT_ID = "cat_id";
    public static String NAME= "name";
    public static String TABLE_NAME= "sub_category";
    public static String CREATE= "create table "+TABLE_NAME+"( "+ID+" integer primary key NOT NULL, "+NAME+" text, "+
            CAT_ID+" integer, foreign key ("+CAT_ID+") references "+CategoryDao.TABLE_NAME+" ("+CategoryDao.ID+"));";

    protected SQLiteDatabase database;
    private DBHelper dbHelper;
    private Context mContext;
    String whereIdAndParentId =  ""+ID+"=? and "+CAT_ID+"=? ";
    String whereParentId = CAT_ID+"=? ";
    public SubCategoryDao(Context context) {
        this.mContext = context;
        dbHelper = DBHelper.getHelper(mContext);
        open();

    }

    public void open() throws SQLException {
        if(dbHelper == null)
            dbHelper = DBHelper.getHelper(mContext);
        database = dbHelper.getWritableDatabase();
    }

    public void insert(Category category){
        long parentID = getParentId(category);

        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(ID, category.getId());
        contentValues.put(CAT_ID, parentID);
        contentValues.put(NAME, category.getTitle());
        //Print.e(this, "inserting subCat: "+ subCatetory.getTitle());
        try{
            db.insertOrThrow(TABLE_NAME, null, contentValues);
            // Print.e(this, "Successfully added subcat: "+ subCatetory.getName());
        }catch (Exception e){
            e.printStackTrace();
            Print.e(this, "Failed to insert Category: "+category.getTitle());
        }
    }

    public long update(Category category){
        long parentID = getParentId(category);
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put(NAME, category.getTitle());
            contentValues.put(CAT_ID, parentID);
            long update = database.update(TABLE_NAME, contentValues,
                    whereIdAndParentId,
                    new String[] {category.getId()+"", parentID+""});
            return update;

        }catch (Exception ex){
            ex.printStackTrace();
            Print.e(this, "Failed to update Category: "+category.getTitle());
            return 0;
        }
    }

    public int deleteAll(Category parent){
        long parentID = getParentId(parent);
        try {
            return database.delete(TABLE_NAME,
                    whereParentId, new String[] {parentID+""});

        }catch (Exception ex){
            ex.printStackTrace();
            Print.e(this, "Failed to delete all child of parent: "+ parent.getTitle());
            return 0;
        }
    }

    public int delete(Category category){
        long parentID = getParentId(category);
        try {
            return database.delete(TABLE_NAME,
                    whereIdAndParentId, new String[] {category.getId()+"", parentID+""});

        }catch (Exception ex){
            ex.printStackTrace();
            Print.e(this, "Failed to delete Sub Category: "+ category.getTitle());
            return 0;
        }
    }

    private long getParentId(Category category){
        return (int)Double.parseDouble(category.getParent().toString());
    }

    public int exist(long id){
        try {
            String query = "select "+ID+" from "+TABLE_NAME+" where "+ID+" = "+id;
            Cursor cursor = database.rawQuery(query, null);
            return cursor.getCount();
        }catch (Exception ex){
            ex.printStackTrace();
            Print.e(this, "Entry Exist Check Failed Exception");
        }
        return 0;
    }
}
