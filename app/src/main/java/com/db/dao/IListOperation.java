package com.db.dao;

import java.util.List;


/**
 * Created by water on 12/6/16.
 */
public interface IListOperation<T>{
    public void insert(List<T> arg0, String... args);
    public void update(List<T> arg0, String... args);
    public void delete(List<T> arg0, String... args);
}
