package com.db.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.commonasset.utils.Print;
import com.retrofit.response.status.Status;

import java.util.HashMap;

/**
 * Created by water on 12/27/16.
 */

public class StatusDao extends BaseDao{
    public static final String TAG_ENTRY = "entry";
    public static final String TAG_CATEGORY = "category";
    public static String ID = "id";
    public static String LAST_UPDATE = "last_update";
    public static String TAG = "tag";
    public static String TABLE_NAME = "table_status";
    public static String CREATE= "create table "+TABLE_NAME+"( "+
            ID+" integer primary key NOT NULL, "+
            LAST_UPDATE+" integer NOT NULL, "+
            TAG+" integer NOT NULL);";

    public StatusDao(Context context) {
      super(context);

    }

    public long insert(Status status){
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put(ID, status.getId());
            contentValues.put(LAST_UPDATE, status.getModifiedAt());
            contentValues.put(TAG, status.getTag());
            return database.insert(TABLE_NAME, null, contentValues);

        }catch (Exception ex){
            ex.printStackTrace();
            Print.e(this, "Failed to insert Status: "+status.getTag());
            return 0;
        }
    }

    public long update(Status status){
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put(LAST_UPDATE, status.getModifiedAt());
            contentValues.put(TAG, status.getTag());
            long update = database.update(TABLE_NAME, contentValues,
                    whereId,
                    new String[] {status.getId()+"" });
            return update;

        }catch (Exception ex){
            ex.printStackTrace();
            Print.e(this, "Failed to update Status: "+status.getTag());
            return 0;
        }
    }

    public int delete(Status status){
        try {
            return database.delete(TABLE_NAME,
                    whereId, new String[] {status.getId()+""});

        }catch (Exception ex){
            ex.printStackTrace();
            Print.e(this, "Failed to delete Status: "+ status.getTag());
            return 0;
        }
    }

    public HashMap<String, Long> getStatuses(){
        HashMap<String, Long> statuses = new HashMap<>();
        try {
            Cursor catCursor =  database.rawQuery( "select * from "+TABLE_NAME, null );
            catCursor.moveToFirst();
            while(catCursor.isAfterLast() == false){
                statuses.put(catCursor.getString(catCursor.getColumnIndex(TAG)),(long)catCursor.getInt(catCursor.getColumnIndex(LAST_UPDATE)));
                catCursor.moveToNext();
            }
        }catch (Exception ex){
            ex.printStackTrace();
            Print.e(this, "Faild ");
        }
        return  statuses;
    }
}
