package com.db.dao;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Pair;

import com.base.factory.RenderManager;
import com.base.factory.Renderer;
import com.db.DBHelper;

/**
 * Created by water on 1/4/17.
 */

public class BaseDao {
    public static String ID = "id";
    protected SQLiteDatabase database;
    private DBHelper dbHelper;
    protected Context mContext;
    protected String whereId =  ID+"=? ";

    public BaseDao(Context context) {
        this.mContext = context;
        dbHelper = DBHelper.getHelper(mContext);
        open();

    }



    public void open() throws SQLException {
        if(dbHelper == null)
            dbHelper = DBHelper.getHelper(mContext);
        database = dbHelper.getWritableDatabase();
    }

    public  Pair<Renderer, String> getRederer(String rendererKey){
        return  RenderManager.getInstance(mContext).getRenderer(rendererKey);
    }
}
