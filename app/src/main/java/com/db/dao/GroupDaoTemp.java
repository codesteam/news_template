package com.db.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.commonasset.utils.Print;
import com.db.helper.EntryBinding;
import com.db.helper.EntryTypeBinding;
import com.retrofit.response.entry.Entry;
import com.retrofit.response.home.Group;
import com.retrofit.response.status.Status;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by water on 12/27/16.
 */

public class GroupDaoTemp extends BaseDao{
    public static String GROUP_NAME = "group_name";
    public static String ORDER = "item_order";
    public static String ENTRY_ID = "entry_id_group";
    public static String TABLE_NAME = "table_group";
    public static String CREATE= "create table "+TABLE_NAME+"( "+
            ID+" integer primary key NOT NULL, "+
            GROUP_NAME+" text, "+
            ENTRY_ID+" integer NOT NULL, "+
            ORDER+" integer NOT NULL);";

    private EntryDao entryDao;

    public GroupDaoTemp(Context context) {
      super(context);
        entryDao = new EntryDao(context);

    }

    public long insert(Group group){

        try {
            List<Entry> entries = group.getEntries();
            if (entries == null || entries.size() <=0){
                return -1;
            }

            if (entries != null){
                List<Entry> insertList = new ArrayList<Entry>();
                List<Entry> updateList = new ArrayList<Entry>();
                for (Entry entry: entries){
                    if (entryDao.exist(entry.getId()) > 0){
                        updateList.add(entry);
                        Print.e(this, "update Entry");
                    }else{
                        insertList.add(entry);
                        Print.e(this, "insert Entry");
                    }
                }
                entryDao.insert(insertList,  null);
                Print.e(this, "update list size: "+ updateList.size()+"");
                entryDao.update(updateList,  null);
            }

            for (int i = 0; i < entries.size(); i++){
                long entryId = group.getEntries().get(i).getId();
                ContentValues contentValues = new ContentValues();
                //contentValues.put(ID, group.getId());
                contentValues.put(GROUP_NAME, group.getName());
                contentValues.put(ORDER,group.getOrder());
                contentValues.put(ENTRY_ID,entryId);
                database.insert(TABLE_NAME, null, contentValues);
                Print.e(this, "inserted group");

            }
            return 1;

        }catch (Exception ex){
            ex.printStackTrace();
            Print.e(this, "Failed to insert group: "+group.getName());
            return 0;
        }
    }

    public int exist(long entryId){
        try {
            String query = "select "+ID+" from "+TABLE_NAME+" where "+ENTRY_ID+" = "+entryId;
            Cursor cursor = database.rawQuery(query, null);
            return cursor.getCount();
        }catch (Exception ex){
            ex.printStackTrace();
            Print.e(this, "Entry Exist Check Failed Exception");
        }
        return 0;
    }
    public long update(Status status){
       /* try {
            ContentValues contentValues = new ContentValues();
            contentValues.put(LAST_UPDATE, status.getModifiedAt());
            contentValues.put(TAG, status.getTag());
            long update = database.update(TABLE_NAME, contentValues,
                    whereId,
                    new String[] {status.getId()+"" });
            return update;

        }catch (Exception ex){
            ex.printStackTrace();
            Print.e(this, "Failed to update Status: "+status.getTag());
            return 0;
        }*/
        return  -1;
    }

    public int deleteAllRows(){
        try {
            return database.delete(TABLE_NAME,
                    null, null);

        }catch (Exception ex){
            ex.printStackTrace();
            Print.e(this, "Failed to delete : ");
            return 0;
        }
    }

    public EntryTypeBinding getEntryTypeBinding(){
        EntryTypeBinding entryTypeBinding = new EntryTypeBinding();

        try {
            List<Group> groups = new ArrayList<>();
            Cursor cursor =  database.rawQuery( "select * from "+TABLE_NAME, null );
            cursor.moveToFirst();
            while(cursor.isAfterLast() == false){
                Group group = new Group();
                group.setName(cursor.getString(cursor.getColumnIndex(GROUP_NAME)));
                group.setEntryId((long)cursor.getInt(cursor.getColumnIndex(ENTRY_ID)));
                groups.add(group);
                cursor.moveToNext();
            }

            boolean featuredFound = false;
            Print.e(this, "Totla Entry Count for home: "+ groups.size());
            for (int i = 0; i < groups.size(); i++){
                EntryBinding entryBinding = entryDao.getEntryBinding(groups.get(i).getEntryId());
                if (groups.get(i).getName().equals("featured") && !featuredFound){
                    entryTypeBinding.put(EntryDao.TYPE_FEATURED, entryBinding);
                    featuredFound = true;
                }else{
                    entryTypeBinding.put(EntryDao.TYPE_GENERAL, entryBinding);
                }
            }
        }catch (Exception ex){
            ex.printStackTrace();
            Print.e(this, "Faild ");
        }

        return entryTypeBinding;
    }

}
