package com.db.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Pair;

import com.base.components.RenderRecognizer;
import com.base.factory.Renderer;
import com.base.factory.SingleRenderer;
import com.commonasset.utils.Print;
import com.retrofit.response.category.Category;

import java.util.ArrayList;

import static android.R.attr.category;

/**
 * Created by water on 12/28/16.
 */

public class CategoryDao extends BaseDao{
    public static final String RENDERER_KEY = "category";
    public static final int ON_APP_START = 0;
    public static final int ON_START_DOWNLOAD_COMPLETE = 1;
    public static String TABLE_NAME= "category";
    public static String TITLE = "title_cat";
    public static String PARENT = "parent";
    public static String LAST_DOWNLOADED = "last_downloaded";
    public static String CAT_STATE = "cat_state"; // 0 on app start, 1 on download complete
    public static String ORDER = "item_order";
    public static String CREATE="create table "+TABLE_NAME +"("+
            ID+" integer primary key NOT NULL, "+
            PARENT+" integer, "+
            CAT_STATE+" integer, "+
            LAST_DOWNLOADED+" text, "+
            ORDER+" integer, "+
            TITLE+" text NOT NULL"+");";
    public static String ORDER_BY = " ORDER BY "+ ORDER + " ASC";

    public CategoryDao(Context context) {
        super(context);
    }


    public void insert(Category category, RenderRecognizer renderRecognizer){

        ContentValues contentValues = new ContentValues();
        contentValues.put(ID, category.getId());
        contentValues.put(PARENT, filterParent(category));
        contentValues.put(TITLE, category.getTitle());
        contentValues.put(ORDER, category.getOrder());
        //contentValues.put(CAT_STATE, 0); // don not need value will maintain from app lifecycle
        //contentValues.put(LAST_DOWNLOADED, 0); do not need insert LAST_DOWNLOADED or set it to 0; cause there is no value for it from server
        //it will maintain from locally, when user last download data for this category
        //Print.e(this, "inserting cat: "+ category.getTitle());
        try{
            database.insert(TABLE_NAME, null, contentValues);
            if (renderRecognizer != null){
                //Print.e(this, "Render Recognizer name: "+ renderRecognizer.getClass().getName());
                Pair<Renderer, String> pair = getRederer(RENDERER_KEY);
                if (pair != null && pair.first != null){
                    ((SingleRenderer)pair.first).insertRenderer(category);
                }
            }else{
                Print.e(this, "RenderRecognizer found null");
            }

            Print.e(this, "Successfully added cat: "+ category.getTitle());
        }catch (Exception e){
            e.printStackTrace();
        }


    }
    public void updateLastDownloaded(long catId, String lastDownloadedTime){
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put(LAST_DOWNLOADED, lastDownloadedTime);
            database.update(TABLE_NAME, contentValues,
                    whereId,
                    new String[] {catId+"" });


        }catch (Exception ex){
            ex.printStackTrace();
            Print.e(this, "Failed to update Category LAST_DOWNLOADED: "+catId);
        }
    }

    public void setCatState(long catId, int state){
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put(CAT_STATE, state);
            database.update(TABLE_NAME, contentValues,
                    whereId,
                    new String[] {catId+"" });

            Print.e(this, "update Category CatState complete");
        }catch (Exception ex){
            ex.printStackTrace();
            Print.e(this, "Failed to update Category CatState: "+catId);
        }
    }

    public void resetCatState(){
        try {
            String updateQuery ="UPDATE "+TABLE_NAME+" SET "+CAT_STATE+" = 0";
            database.rawQuery(updateQuery, null).moveToFirst();

        }catch (Exception ex){
            ex.printStackTrace();
            Print.e(this, "Failed to reset cat state");
        }
    }
    public int getCatState(long catId){
        try {
            String query = "select "+CAT_STATE+" from "+TABLE_NAME+" where "+ID+" = "+catId;
            Cursor cursor = database.rawQuery(query, null);
            cursor.moveToFirst();
            return cursor.getInt(cursor.getColumnIndex(CAT_STATE));
        }catch (Exception ex){
            ex.printStackTrace();
            Print.e(this, "Failed to getLastDownloaded time stapm of cat"+ catId);
            return 0;
        }
    }

    public String getLastDownloaded(long catId){
        try {
            String query = "select "+LAST_DOWNLOADED+" from "+TABLE_NAME+" where "+ID+" = "+catId;
            Cursor cursor = database.rawQuery(query, null);
            cursor.moveToFirst();
            return cursor.getString(cursor.getColumnIndex(LAST_DOWNLOADED));
        }catch (Exception ex){
            ex.printStackTrace();
            Print.e(this, "Failed to getLastDownloaded time stapm of cat"+ catId);
            return null;
        }
    }
    public long update(Category category, RenderRecognizer renderRecognizer){
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put(TITLE, category.getTitle());
            contentValues.put(PARENT, filterParent(category));
            contentValues.put(ORDER, category.getOrder());
            long update = database.update(TABLE_NAME, contentValues,
                    whereId,
                    new String[] {category.getId()+"" });
            if (renderRecognizer != null){
                //Print.e(this, "Render Recognizer name: "+ renderRecognizer.getClass().getName());
                Pair<Renderer, String> pair = getRederer(RENDERER_KEY);
                if (pair != null && pair.first != null){
                    ((SingleRenderer)pair.first).updateRenderer(category);
                }
            }else{
                Print.e(this, "RenderRecognizer found null");
            }
            return update;

        }catch (Exception ex){
            ex.printStackTrace();
            Print.e(this, "Failed to update Category: "+category.getTitle());
            return 0;
        }
    }

    public boolean delete(long id, RenderRecognizer renderRecognizer){
        try {
           // delete from core_category where id in (select id from core_category where parent_id = 9 or id=9)
           String query = "delete from "+TABLE_NAME +" where "+ID +
                   " in (select "+ID +" from "+TABLE_NAME+" where "+PARENT+" = "+id+
                   " or "+ID +" = "+id+")";
            boolean state = database.rawQuery(query,null).moveToFirst();
            if (renderRecognizer != null){
                Print.e(this, "Render Recognizer name: "+ renderRecognizer.getClass().getName());
                Pair<Renderer, String> pair = getRederer(RENDERER_KEY);
                if (pair != null && pair.first != null){
                    ((SingleRenderer)pair.first).deleteRenderer(category);
                }
            }else{
                Print.e(this, "RenderRecognizer found null");
            }
            return state;
        }catch (Exception ex){
            ex.printStackTrace();
            Print.e(this, "Failed to delete category: "+ id);
            return false;
        }
    }

    public ArrayList<Category> getCategories(){
        ArrayList<Category> categories = new ArrayList<>();
        Cursor cursor =  database.rawQuery( "select * from "+CategoryDao.TABLE_NAME +ORDER_BY, null );
        cursor.moveToFirst();

        while(cursor.isAfterLast() == false){
            Category cat = new Category();
            long catId = (long)cursor.getInt(cursor.getColumnIndex(CategoryDao.ID));
            cat.setId(catId);
            cat.setTitle(cursor.getString(cursor.getColumnIndex(CategoryDao.TITLE)));
            cat.setParent((long)cursor.getInt(cursor.getColumnIndex(CategoryDao.PARENT)));
            cat.setOrder(cursor.getInt(cursor.getColumnIndex(CategoryDao.ORDER)));
           categories.add(cat);

            cursor.moveToNext();
        }
        return categories;
    }
  /*  public Pair<ArrayList<Category>,  HashMap<Long, ArrayList<Category>>> getCategories(){
        ArrayList<Category> parents = new ArrayList<>();
        Cursor cursor =  database.rawQuery( "select * from "+CategoryDao.TABLE_NAME +ORDER_BY, null );
        cursor.moveToFirst();
        HashMap<Long, ArrayList<Category>> mapSubCategory = new HashMap<>();
        while(cursor.isAfterLast() == false){
            Category cat = new Category();
            long catId = (long)cursor.getInt(cursor.getColumnIndex(CategoryDao.ID));
            cat.setId(catId);
            cat.setTitle(cursor.getString(cursor.getColumnIndex(CategoryDao.TITLE)));
            cat.setParent((long)cursor.getInt(cursor.getColumnIndex(CategoryDao.PARENT)));
            cat.setOrder(cursor.getInt(cursor.getColumnIndex(CategoryDao.ORDER)));
            if (cat.getParent() != -1){
                ArrayList<Category> subCat = mapSubCategory.get(cat.getParent());
                if (subCat == null){
                    subCat = new ArrayList<>();
                    mapSubCategory.put(cat.getParent(), subCat);
                }
                subCat.add(cat);
            }else{
                parents.add(cat);
            }

            cursor.moveToNext();
        }
        return new Pair<>(parents, mapSubCategory);
    }*/

    public int exist(long id){
        try {
            String query = "select "+ID+" from "+TABLE_NAME+" where "+ID+" = "+id;
            Cursor cursor = database.rawQuery(query, null);

            return cursor.getCount();
        }catch (Exception ex){
            ex.printStackTrace();
            Print.e(this, "Entry Exist Check Failed Exception");
        }
        return 0;
    }

    private long filterParent(Category category){
        if (category.getParent() == null){
            return -1;
        }else{
            return category.getParent();
        }
    }
}
