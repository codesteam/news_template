package com.db.dao;

import android.content.ContentValues;
import android.content.Context;

import com.base.common.CommonConstants;
import com.commonasset.utils.Print;
import com.retrofit.response.entry.Medium;

import java.util.List;

/**
 * Created by water on 12/14/16.
 */
public class MediaImageDao extends BaseDao{
    public static String SERVER_ID = "server_id";
    public static String SMALL = "small";
    public static String LARGE = "large";
    public static String ORDER = "item_order";
    public static String SHORT_DESCRIPTION = "short_desc";
    public static String ENTRY_ID = "entry_id";
    public static String TABLE_NAME= "table_media_image";
    public static String CREATE= "create table "+TABLE_NAME+"( "+
            ID+" integer primary key NOT NULL, "+
            SERVER_ID+" integer, "+
            SMALL+" text, "+
            LARGE+" text, "+
            SHORT_DESCRIPTION+" text, "+
            ORDER+" integer, "+
            ENTRY_ID+" integer, " +
            "foreign key ("+ENTRY_ID+") references "+ EntryDao.TABLE_NAME+" ("+EntryDao.ID+") ON DELETE CASCADE);";

    String whereEntryIdAndServerId =  ""+ENTRY_ID+"=? and "+SERVER_ID+"=? ";
    public static String ORDER_BY = " ORDER BY "+ ORDER + " ASC";
    public MediaImageDao(Context context) {
        super(context);
    }

    public long insert(List<Medium> mediumList, Long id) {
        long mId = -1;
        String small = CommonConstants.NOT_FOUND;
        String large = CommonConstants.NOT_FOUND;
        String shortDesc = CommonConstants.NOT_FOUND;
        int order = -1;

        for(Medium medium: mediumList){
            mId = medium.getId();
            order = medium.getOrder();
            if (medium.getLarge() != null){
                large = medium.getLarge();
                shortDesc = medium.getShortDescription();
                if (shortDesc == null){
                    shortDesc = CommonConstants.NOT_FOUND;
                }
            }else if (medium.getSmall() != null){
                small = medium.getSmall();
            }
        }

        if (mId == -1){
            return 0;
        }

        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put(SERVER_ID,mId);
            contentValues.put(SMALL, small);
            contentValues.put(LARGE, large);
            contentValues.put(ORDER, order);
            contentValues.put(SHORT_DESCRIPTION, shortDesc);

            contentValues.put(ENTRY_ID, id);
            return database.insert(TABLE_NAME, null, contentValues);

        }catch (Exception ex){
            ex.printStackTrace();
            Print.e(this, "Failed to insert Entry: "+mId);
            return 0;
        }
    }

    public int update(List<Medium> mediumList, Long id) {

        long mId = -1;
        String small = CommonConstants.NOT_FOUND;
        String large = CommonConstants.NOT_FOUND;
        String shortDesc = CommonConstants.NOT_FOUND;
        int order = -1;

        for(Medium medium: mediumList){
            mId = medium.getId();
            order = medium.getOrder();
            if (medium.getLarge() != null){
                large = medium.getLarge();
                shortDesc = medium.getShortDescription();
                if (shortDesc == null){
                    shortDesc = CommonConstants.NOT_FOUND;
                }
            }else if (medium.getSmall() != null){
                small = medium.getSmall();
            }
        }
        if (mId == -1){
            return 0;
        }
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put(SERVER_ID,mId);
            contentValues.put(SMALL, small);
            contentValues.put(LARGE, large);
            contentValues.put(ORDER, order);
            contentValues.put(SHORT_DESCRIPTION, shortDesc);

            contentValues.put(ENTRY_ID, id);
            return database.update(TABLE_NAME, contentValues,
                    whereEntryIdAndServerId,
                    new String[] {id+"", mId+"" });

        }catch (Exception ex){
            ex.printStackTrace();
            Print.e(this, "Failed to insert Entry: "+mId);
            return 0;
        }
    }

   /* public int delete(Medium medium, Long id) {
        try {
            return  database.delete(TABLE_NAME,
                    whereEntryIdAndServerId, new String[] { id+"", medium.getId()+"" });

        }catch (Exception ex){
            ex.printStackTrace();
            Print.e(this, "Failed to delete Entry: "+ id+"");
            return 0;
        }
    }*/
}
