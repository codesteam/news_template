package com.db.dao;

import android.content.ContentValues;
import android.content.Context;

import com.base.common.CommonConstants;
import com.commonasset.utils.Print;
import com.retrofit.response.entry.Medium;

/**
 * Created by water on 12/14/16.
 */
public class MediaVideoDao extends BaseDao{
    public static String ID = "id";
    public static String VIDEO = "video";
    public static String DURATION = "duration";
    public static String SHORT_DESCRIPTION = "short_desc";
    public static String ENTRY_ID = "entry_id";
    public static String ORDER = "item_order";
    public static String TABLE_NAME= "table_media_video";
    public static String CREATE= "create table "+TABLE_NAME+"( "+
            ID+" integer primary key NOT NULL, "+
            VIDEO+" text, "+
            DURATION+" text, "+
            SHORT_DESCRIPTION+" text, "+
            ORDER+" integer, "+
            ENTRY_ID+" integer, " +
            "foreign key ("+ENTRY_ID+") references "+ EntryDao.TABLE_NAME+" ("+EntryDao.ID+") ON DELETE CASCADE );";

    String whereEntryIdAndMediaId =  ""+ENTRY_ID+"=? and "+ID+"=? ";
    public static String ORDER_BY = " ORDER BY "+ ORDER + " ASC";

    public MediaVideoDao(Context context) {
        super(context);

    }

    public long insert(Medium medium, Long entryId) {
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put(ID, medium.getId());
            contentValues.put(VIDEO, medium.getVideo());
            contentValues.put(DURATION, medium.getDuration());
            contentValues.put(ORDER, medium.getOrder());
            if (medium.getShortDescription() == null){
                contentValues.put(SHORT_DESCRIPTION, CommonConstants.NOT_FOUND);
            }else{
                contentValues.put(SHORT_DESCRIPTION, medium.getShortDescription());
            }
            contentValues.put(ENTRY_ID, entryId);
            return database.insert(TABLE_NAME, null, contentValues);

        }catch (Exception ex){
            ex.printStackTrace();
            Print.e(this, "Failed to insert Entry: "+medium.getLarge());
            return 0;
        }
    }

    public int update(Medium medium, Long entryId) {

        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put(VIDEO, medium.getVideo());
            contentValues.put(DURATION, medium.getVideo());
            contentValues.put(ORDER, medium.getOrder());
            if (medium.getShortDescription() == null){
                contentValues.put(SHORT_DESCRIPTION, CommonConstants.NOT_FOUND);
            }else{
                contentValues.put(SHORT_DESCRIPTION, medium.getShortDescription());
            }
            contentValues.put(ENTRY_ID, entryId);
            return database.update(TABLE_NAME, contentValues,
                    whereEntryIdAndMediaId,
                    new String[] {entryId+"", medium.getId()+"" });

        }catch (Exception ex){
            ex.printStackTrace();
            Print.e(this, "Failed to insert Entry: "+medium.getLarge());
            return 0;
        }
    }

 /*   public int delete(Medium medium, Long entryId) {
        try {
            return database.delete(TABLE_NAME,
                    whereEntryIdAndMediaId, new String[] { entryId+"", medium.getId()+"" });

        }catch (Exception ex){
            ex.printStackTrace();
            Print.e(this, "Failed to delete Entry: "+ entryId);
            return 0;
        }
    }*/
}
