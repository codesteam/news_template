package com.db.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.text.TextUtils;

import com.commonasset.utils.Print;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by water on 12/6/16.
 */
public class EntryRelatedDao extends BaseDao {
    public static String TABLE_NAME= "table_en_related_en";
    public static String ENTRY_ID = "entry_id";
    public static String RELETED_ID = "related_id";
    public static String CREATE= "create table "+TABLE_NAME+
            "( "+
            ENTRY_ID +" integer not null, " +
            RELETED_ID +" integer not null, "+
            "PRIMARY KEY ("+ RELETED_ID +", "+ ENTRY_ID +"));";

    private static final String WHERE_ENTRY_ID_EQUALS = RELETED_ID
            + " =?";
    public EntryRelatedDao(Context context) {
        super(context);
    }
    public long insert(long entryId, long relatedEntryId){
        ContentValues contentValues = new ContentValues();
        contentValues.put(ENTRY_ID, entryId);
        contentValues.put(RELETED_ID, relatedEntryId);
        //Print.e(this, "inserting subCat: "+ subCatetory.getTitle());
        return database.insertOrThrow(TABLE_NAME, null, contentValues);
    }

    public void updateEntryRelatedEntrites(long entryID, Long[] reletedIds) {
        String csvRelatedIds = TextUtils.join(",", reletedIds);
        Print.e(this, csvRelatedIds);
        try {
            String deleteSql = "delete from "+TABLE_NAME+" where "+ RELETED_ID +" not in("+csvRelatedIds+")"+" and "+ ENTRY_ID + " = "+entryID+";";
            database.rawQuery(deleteSql, null).moveToFirst();
            Print.e(this, "EntryRelatedDao delete Operation successful");
        }catch (Exception ex){
            ex.printStackTrace();
            Print.e(this, "Failed to delete using EntryRelatedDao ");
        }
        try {
            for (int i = 0; i < reletedIds.length; i++){
                /*String insert = "insert into "+TABLE_NAME+" values( "+entryID+", "+catIds[i]+" ) where NOT EXISTS "+
                        "(select "+ENTRY_ID+" from "+TABLE_NAME+" where "+RELETED_ID+" = "+entryID+");";*/
                String insert = "insert or ignore into "+TABLE_NAME+" values ("+entryID+", "+reletedIds[i]+" ) ";
                database.rawQuery(insert, null).moveToFirst();
                Print.e(this, "Inserted related id: "+ reletedIds[i]+ "  entry id: "+ entryID);
            }
            Print.e(this, "EntryRelatedDao delete then insert Operation successful");
        }catch (Exception ex){
            ex.printStackTrace();
            Print.e(this, "Failed to insert using EntryRelatedDao");
        }

    }

    public int delete(String csvEntryIds){
        try {
           /* String deleteSql = "delete from "+TABLE_NAME+" where "+RELETED_ID+" in("+csvEntriesId+");";
             Cursor cursor = database.rawQuery( deleteSql, null );
             cursor.moveToFirst();
             return cursor.getCount();*/
            return database.delete(TABLE_NAME, ENTRY_ID+" IN (" + csvEntryIds + ")", null);
        }catch (Exception ex){
            ex.printStackTrace();
            Print.e(this, "Unable to delete all row from EntryRelatedDao under entries " + csvEntryIds);
        }

        return 0;
    }
    public void delete(List[] entriesId){
        String csvEntriesId = TextUtils.join(",", entriesId);
        try {
            String deleteSql = "delete from "+TABLE_NAME+" where "+ ENTRY_ID +" in("+csvEntriesId+");";
            database.rawQuery( deleteSql, null ).moveToFirst();
        }catch (Exception ex){
            ex.printStackTrace();
            Print.e(this, "Unable to delete all row from EntryRelatedDao under entries " + csvEntriesId);
        }


    }

    public List<Long> getEntryRelatedEntriesId(long entryId){
        List<Long> entyIds = new ArrayList<>();
        Cursor catCursor =  database.rawQuery( "select * from "+TABLE_NAME + " where "+ ENTRY_ID +" = "+ entryId, null );
        catCursor.moveToFirst();
        while(catCursor.isAfterLast() == false){

            long relId = (long)catCursor.getInt(catCursor.getColumnIndex(RELETED_ID));
            entyIds.add(relId);
            Print.e(this, "Found Related Id: "+ relId + " entryId"+ entryId);
            catCursor.moveToNext();
        }
        return  entyIds;
    }

    public int count(long catId){
        String count = "SELECT "+ID+" FROM "+ TABLE_NAME + " where "+ ENTRY_ID +" = "+ catId;
        Cursor mcursor = database.rawQuery(count, null);
        mcursor.moveToFirst();
        return mcursor.getCount();
    }

}
