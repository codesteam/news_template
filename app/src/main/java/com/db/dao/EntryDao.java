package com.db.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.text.TextUtils;
import android.util.Pair;

import com.base.factory.ListRenderer;
import com.base.factory.RenderManager;
import com.base.factory.Renderer;
import com.commonasset.utils.Print;
import com.db.helper.EntryBinding;
import com.db.helper.EntryTypeBinding;
import com.materialsearchview.MaterialSearchView;
import com.materialsearchview.db.SearchItem;
import com.retrofit.response.entry.Entry;
import com.retrofit.response.entry.Medium;
import com.retrofit.response.entry.details.EntryDetails;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by water on 12/6/16.
 */
public class EntryDao extends BaseDao implements IListOperation { // this class extends IListOperation cause this is root dao renderer class
                                        // if multiple object incoming first we will insert them then call to renderer
                                        // for render them at once
    public static String ENTRY_TYPE = "entry_type";
    public static String TITLE= "title";
    public static String PRIMARY_CAT_ID = "downloaded_cat_id"; // cat id that is used for downloading this entry
    public static String CATS_ID= "cats_id";
    public static String MEDIA_LARGE= "media_large";
    public static String MEDIA_IMAGE_SHORT_DESCRIPTION= "media_image_short_desc";
    public static String MEDIA_SMALL= "media_small";
    public static String MEDIA_VIDEO= "media_video";
    public static String MEDIA_VIDEO_DURATION= "media_video_duration";
    public static String MEDIA_VIDEO_SHORT_DESCRIPTION= "media_video_short_desc";
    public static String SHORT_DESCRIPTION= "short_description";
    public static String DETAILS= "details";
    public static String ORDER = "item_order";
    public static String NOTIFIED = "notified";
    public static String PUB_DATE= "pub_date";
    public static String MEDIA_TYPE= "media_type";
    public static String MODIFIED_AT= "modified_at";
    public static String TABLE_NAME= "table_entry";
    public static String CREATE= "create table "+TABLE_NAME+
            "( "+ID+" integer primary key NOT NULL, "+
            PRIMARY_CAT_ID + " integer, "+
            ENTRY_TYPE + " integer, "+
            TITLE + " text, "+
            SHORT_DESCRIPTION + " text, "+
            DETAILS + " text, "+
            MEDIA_TYPE + " integer, "+
            ORDER+" integer, "+
            NOTIFIED+" integer default 0, "+
            PUB_DATE + " integer, "+
            MODIFIED_AT + " integer "+");";

    public static final int TYPE_GENERAL = 0;
    public static final int TYPE_FEATURED = 1;
    public static final int MAX_ROW_INFLATE_LIMITE_FOR_SPECIAL_NEWS = 3;
    public static final int TYPE_SPECIAL = 2;
    public static final int TYPE_MEDIA_IMAGE = 0;
    public static final int TYPE_MEDIA_VIDEO = 1;
    public static final int TYPE_MEDIA_NONE = 99;
    public static final int TYPE_CUSTOM = 3;
    public static final int TYPE_BREAKING = 4;

    String orderBy = " ORDER BY "+ PUB_DATE + " DESC";
    Entry_CatDao entry_catDao;
    EntryRelatedDao entryRelatedDao;
    MediaImageDao mediaImageDao;
    MediaVideoDao mediaVideoDao;
    EntryTypeBinding entryTypeBinding;
    RenderManager renderManager;
    public EntryDao(Context context) {
        super(context);
        entry_catDao = new Entry_CatDao(context);
        entryRelatedDao = new EntryRelatedDao(context);
        mediaImageDao = new MediaImageDao(context);
        mediaVideoDao = new MediaVideoDao(context);
        renderManager = RenderManager.getInstance(context);
        entryRelatedDao = new EntryRelatedDao(context);

    }

    @Override
    public void insert(List arg0, String... args) {
        List<Entry> entries = (List<Entry>)arg0;
        List<SearchItem> searchItems = new ArrayList<>();
        for(Entry entry: entries){
            try {
                Long insertedItemCatId = null;
                if (args != null && args.length > 0 ){
                    if (args[0] != null){ // through args[0] always pass catId
                        insertedItemCatId =  Long.valueOf(args[0]).longValue();
                    }
                }

                if (insertedItemCatId == null){ // if item downloaded whitout category api call like from push notification or releted item
                    List<Long> categories = entry.getCategories();
                    if (categories != null && categories.size() >0){
                        insertedItemCatId = categories.get(0);
                    }
                }
                ContentValues contentValues = new ContentValues();
                contentValues.put(ID, entry.getId());
                contentValues.put(PRIMARY_CAT_ID,insertedItemCatId);
                contentValues.put(ENTRY_TYPE, entry.getEntryType());
                contentValues.put(TITLE, entry.getTitle());
                contentValues.put(SHORT_DESCRIPTION, entry.getShortDescription());
                contentValues.put(MEDIA_TYPE, entry.getMediaType());
                contentValues.put(PUB_DATE, entry.getPubDate());
                contentValues.put(MODIFIED_AT, entry.getModifiedAt());
                contentValues.put(ORDER, entry.getOrder());
                database.insert(TABLE_NAME, null, contentValues);

                SearchItem searchItem = new SearchItem(entry.getId());
                searchItem.setMediaType(entry.getMediaType());
                searchItem.setPubDate(entry.getPubDate());
                searchItem.setTitle(entry.getTitle());
                searchItem.setCatId(insertedItemCatId);

                List<Long> categories = entry.getCategories();
                if (categories != null && categories.size() > 0){
                    for(Long catId: categories){
                        entry_catDao.insert(catId, entry.getId());
                    }
                }
                List<Medium> mediumList = entry.getMedia();
                if (mediumList != null && mediumList.size() > 0){
                    if (entry.getMediaType() == TYPE_MEDIA_IMAGE){
                        searchItem.setTumbUrl(mediumList.get(0).getSmall());
                        mediaImageDao.insert(mediumList, entry.getId());


                    }else{
                        searchItem.setVideoUrl(mediumList.get(0).getVideo());
                        searchItem.setVideoDuration(mediumList.get(0).getDuration());
                        for(Medium medium: mediumList){
                            mediaVideoDao.insert(medium, entry.getId());
                        }
                    }
                }
                searchItems.add(searchItem);

            }catch (Exception ex){
                ex.printStackTrace();
                Print.e(this, "Failed to insert Entry: "+entry.getId());
            }
        }

        Pair<Renderer, String> pair = getRederer(MaterialSearchView.RENDERER_KEY);
        if (pair != null && pair.first != null){
            ((ListRenderer)pair.first).insertRenderer(searchItems);
        }

       /* insertSearchView(searchItems);
        updateRenderer(args[0]+"");*/
    }

    @Override
    public void update(List arg0, String... args) {
        List<Entry> entries = (List<Entry>)arg0;
        List<SearchItem> searchItems = new ArrayList<>();
        for(Entry entry: entries){
            try {
                Long updatedItemCatId = null;
                if (args != null && args.length > 0 ){
                    if (args[0] != null){ // through args[0] always pass catId
                        updatedItemCatId =  Long.valueOf(args[0]).longValue();
                    }
                }

                if (updatedItemCatId == null){ // if item downloaded whitout category api call like from push notification or releted item
                    List<Long> categories = entry.getCategories();
                    if (categories != null && categories.size() >0){
                        updatedItemCatId = categories.get(0);
                    }
                }

                ContentValues contentValues = new ContentValues();
                contentValues.put(PRIMARY_CAT_ID, updatedItemCatId);
                contentValues.put(TITLE, entry.getTitle());
                contentValues.put(ENTRY_TYPE, entry.getEntryType());
                contentValues.put(SHORT_DESCRIPTION, entry.getShortDescription());
                contentValues.putNull(DETAILS); // entry item updated, its details should be downloaded againg by user when
                                                // user goes to details activity to show the details again.
                contentValues.put(PUB_DATE, entry.getPubDate());
                contentValues.put(MODIFIED_AT, entry.getModifiedAt());
                contentValues.put(ORDER, entry.getOrder());

                SearchItem searchItem = new SearchItem(entry.getId());
                searchItem.setMediaType(entry.getMediaType());
                searchItem.setPubDate(entry.getPubDate());
                searchItem.setTitle(entry.getTitle());
                searchItem.setCatId(updatedItemCatId);

                List<Long> categories = entry.getCategories();
                if (categories != null && categories.size() > 0){
                    Long[] cats = new Long[categories.size()];
                    cats = entry.getCategories().toArray(cats);
                    entry_catDao.updateCat(entry.getId(), cats);
                }
                List<Medium> mediumList = entry.getMedia();
                if (mediumList != null && mediumList.size() > 0){
                    if (entry.getMediaType() == TYPE_MEDIA_IMAGE){
                        searchItem.setTumbUrl(mediumList.get(0).getSmall());
                        int state = mediaImageDao.update(mediumList, entry.getId()); // if state > 0, items found and updated
                        if (state == 0){ // new item entered through update call
                            mediaImageDao.insert(mediumList, entry.getId());
                        }

                    }else{
                        searchItem.setVideoUrl(mediumList.get(0).getVideo());
                        searchItem.setVideoDuration(mediumList.get(0).getDuration());
                        for(Medium medium: mediumList){
                            int state = mediaVideoDao.update(medium, entry.getId()); // if state > 0, items found and updated
                            if (state == 0){ // new item entered through update call
                                mediaVideoDao.insert(medium, entry.getId());
                            }
                        }
                    }
                }

                database.update(TABLE_NAME, contentValues,
                        whereId,
                        new String[] { String.valueOf(entry.getId()) });
                searchItems.add(searchItem);

            }catch (Exception ex){
                ex.printStackTrace();
                Print.e(this, "Failed to update Entry: "+entry.getId());
            }
        }
        Pair<Renderer, String> pair = getRederer(MaterialSearchView.RENDERER_KEY);
        if (pair != null && pair.first != null){
            ((ListRenderer)pair.first).updateRenderer(searchItems);
        }
    }

    public void updateDetails(EntryDetails entryDetails){
        try{
            ContentValues contentValues = new ContentValues();
            contentValues.put(DETAILS, entryDetails.getDetails());
            database.update(TABLE_NAME, contentValues,
                    whereId,
                    new String[] { String.valueOf(entryDetails.getId()) });
        }catch (Exception ex){
            ex.printStackTrace();
        }

        try {
            Long[] relateds = new Long[entryDetails.getRelated().size()];
            relateds = entryDetails.getRelated().toArray(relateds);
            if (relateds != null && relateds.length > 0){
                for (int i=0; i< relateds.length; i++){
                    Print.e(this, "related Id: "+ relateds[i]);
                }
            }else{
                Print.e(this, "No Related Items found");
            }
            entryRelatedDao.updateEntryRelatedEntrites(entryDetails.getId(),relateds);

        }catch (Exception ex){
            ex.printStackTrace();
        }

    }


    @Override
    public void delete(List arg0, String... args) {
        /*List<Entry> entries = (List<Entry>)arg0;
        List<EntryBinding> searchItems = new ArrayList<>();
        for(Entry entry: entries){
            try {
                List<Long> categories = entry.getCategories();
                if (categories != null && categories.size() > 0){
                    for(Long catId: categories){
                       // entry_catDao.delete(catId, entry.getId());
                    }
                }
                // do not delete them by force, cause they are cascade
               *//* if (entry.getMediaType() == TYPE_MEDIA_IMAGE){
                    for(Medium medium: entry.getMedia()){
                        mediaImageDao.delete(medium, entry.getId());
                    }

                }else{
                    for(Medium medium: entry.getMedia()){
                        mediaVideoDao.delete(medium, entry.getId());
                    }
                }*//*
                database.delete(TABLE_NAME,
                        whereId, new String[] { entry.getId() + "" });
                EntryBinding entryBinding = new EntryBinding();
                entryBinding.setEntry(entry);
                searchItems.add(entryBinding);

            }catch (Exception ex){
                ex.printStackTrace();
                Print.e(this, "Failed to delete Entry: "+ entry.getId());
            }
        }
        Pair<Renderer, String> pair = getRederer(MaterialSearchView.RENDERER_KEY);
        if (pair != null && pair.first != null){
            ((ListRenderer)pair.first).deleteRenderer(searchItems);
        }*/
    }


    public int delete(List<Long> ids) {
        List<SearchItem> searchItems = new ArrayList<>();
        String csvEntriesId = TextUtils.join(",", ids);
        try{
            entry_catDao.delete(csvEntriesId);
            entryRelatedDao.delete(csvEntriesId);
           /* String deleteSql = "delete from "+TABLE_NAME+" where "+ID+" in("+csvEntriesId+");";
            Cursor cursor = database.rawQuery( deleteSql, null);
            cursor.moveToFirst();
            return cursor.getCount();*/
            int count = database.delete(TABLE_NAME, ID+" IN (" + csvEntriesId + ")", null);
            if (count > 0){
                for (Long id: ids){
                    SearchItem searchItem = new SearchItem(id);
                    searchItems.add(searchItem);
                }
            }

        }catch (Exception ex){
            ex.printStackTrace();
            Print.e(this, "Unable to delete all row from EntryDao under entries " + csvEntriesId);

        }

        if (searchItems.size() > 0){
            Pair<Renderer, String> pair = getRederer(MaterialSearchView.RENDERER_KEY);
            if (pair != null && pair.first != null){
                ((ListRenderer)pair.first).deleteRenderer(searchItems);
            }
        }

        return searchItems.size();
    }

    public List<EntryBinding> getAllEntry(long catId) {
        List<EntryBinding> entryBindings = new ArrayList<>();
        try {
                  /*String  query = "select en.* from "+TABLE_NAME
                        +" en inner join "+Entry_CatDao.TABLE_NAME
                        +" cat on (en."+ID+" = cat."+Entry_CatDao.RELETED_ID+
                        ") where cat."+Entry_CatDao.ENTRY_ID+"="+catId
                        +" and "+ ENTRY_TYPE+" = "+type;*/
            /*String query = "SELECT entry.*, cat.cat_id, (SELECT GROUP_CONCAT(ei.large, \",\") from MediaImageDao ei WHERE ei.entry_id = entry.id) as media_large,\n" +
                    "(SELECT GROUP_CONCAT(ei.small, \",\") from MediaImageDao ei WHERE ei.entry_id = entry.id) as media_small,\n" +
                    "(SELECT GROUP_CONCAT(vi.video, \",\") from MediaVideoDao vi WHERE vi.entry_id = entry.id) as media_video\n" +
                    "FROM\n" +
                    "entry entry\n" +
                    "INNER JOIN entry_m2m_cat  cat ON (entry.id = cat.entry_id)\n" +
                    "WHERE cat.cat_id="+catId+
                    " and "+ ENTRY_TYPE+" = "+type+"";*/

            String query = "SELECT en.*, " +
                    "cat."+Entry_CatDao.CAT_ID+", " +
                    "(SELECT c."+CategoryDao.TITLE+" from "+CategoryDao.TABLE_NAME+" c WHERE c."+CategoryDao.ID+" = "+catId+") as "+CategoryDao.TITLE+",\n" +
                    "(SELECT GROUP_CONCAT(cat."+Entry_CatDao.CAT_ID+", \",\") from "+Entry_CatDao.TABLE_NAME+" cat WHERE cat."+Entry_CatDao.ENTRY_ID+" = en."+ID+") as "+CATS_ID+",\n" +
                    "(SELECT GROUP_CONCAT(ei."+MediaImageDao.LARGE+", \",\") from "+MediaImageDao.TABLE_NAME+" ei WHERE ei."+MediaImageDao.ENTRY_ID+" = en."+ID+MediaImageDao.ORDER_BY+") as "+MEDIA_LARGE+",\n" +
                    "(SELECT GROUP_CONCAT(ei."+MediaImageDao.SMALL+", \",\") from "+MediaImageDao.TABLE_NAME+" ei WHERE ei."+MediaImageDao.ENTRY_ID+" = en."+ID+MediaImageDao.ORDER_BY+") as "+MEDIA_SMALL+",\n" +
                    "(SELECT GROUP_CONCAT(ei."+MediaImageDao.SHORT_DESCRIPTION+", \",\") from "+MediaImageDao.TABLE_NAME+" ei WHERE ei."+MediaImageDao.ENTRY_ID+" = en."+ID+MediaImageDao.ORDER_BY+") as "+MEDIA_IMAGE_SHORT_DESCRIPTION+",\n" +
                    "(SELECT GROUP_CONCAT(vi."+MediaVideoDao.VIDEO+", \",\") from "+MediaVideoDao.TABLE_NAME+" vi WHERE vi."+MediaVideoDao.ENTRY_ID+" = en."+ID+MediaVideoDao.ORDER_BY+") as "+MEDIA_VIDEO+",\n" +
                    "(SELECT GROUP_CONCAT(vi."+MediaVideoDao.DURATION+", \",\") from "+MediaVideoDao.TABLE_NAME+" vi WHERE vi."+MediaVideoDao.ENTRY_ID+" = en."+ID+MediaVideoDao.ORDER_BY+") as "+MEDIA_VIDEO_DURATION+",\n" +
                    "(SELECT GROUP_CONCAT(vi."+MediaVideoDao.SHORT_DESCRIPTION+", \",\") from "+MediaVideoDao.TABLE_NAME+" vi WHERE vi."+MediaVideoDao.ENTRY_ID+" = en."+ID+MediaVideoDao.ORDER_BY+") as "+MEDIA_VIDEO_SHORT_DESCRIPTION+"\n" +
                    "FROM " + TABLE_NAME+" en " +
                    "INNER JOIN "+Entry_CatDao.TABLE_NAME+"  cat ON (en."+ID+" = cat."+Entry_CatDao.ENTRY_ID+")\n" +
                    "WHERE cat."+Entry_CatDao.CAT_ID+"="+catId + orderBy;

            Cursor cursor = database.rawQuery(query, null);
            cursor.moveToFirst();
            //Print.e(this, "Cursor Size: "+ cursor.getCount());
            while (cursor.isAfterLast() == false) {
                EntryBinding entryBinding = new EntryBinding();
                Entry entry = new Entry();
                entry.setId((long) cursor.getInt(cursor.getColumnIndex(ID)));
                entry.setPrimaryCatId((long) cursor.getInt(cursor.getColumnIndex(PRIMARY_CAT_ID)));
                String csvCatsId = cursor.getString(cursor.getColumnIndex(CATS_ID));
                if (csvCatsId!= null){
                    List<Long> list = new ArrayList<Long>();
                    for (String s : csvCatsId.split(",")){
                        list.add(Long.parseLong(s));
                    }
                    entry.setCategories(list);
                }
                entry.setTitle(cursor.getString(cursor.getColumnIndex(TITLE)));
                int entryType = cursor.getInt(cursor.getColumnIndex(ENTRY_TYPE));
                entry.setEntryType((long)entryType);
                entry.setCatName(cursor.getString(cursor.getColumnIndex(CategoryDao.TITLE)));
                entry.setShortDescription(cursor.getString(cursor.getColumnIndex(SHORT_DESCRIPTION)));
                entry.setPubDate((long) cursor.getInt(cursor.getColumnIndex(PUB_DATE)));
                entry.setMediaType(cursor.getInt(cursor.getColumnIndex(MEDIA_TYPE)));
                entry.setOrder(cursor.getInt(cursor.getColumnIndex(ORDER)));
                entryBinding.setEntry(entry);
                entryBinding.setCsvImageLarge(cursor.getString(cursor.getColumnIndex(MEDIA_LARGE)));
                entryBinding.setCsvImageLargeShortDesc(cursor.getString(cursor.getColumnIndex(MEDIA_IMAGE_SHORT_DESCRIPTION)));
                entryBinding.setCsvImageSmall(cursor.getString(cursor.getColumnIndex(MEDIA_SMALL)));
                entryBinding.setCsvVideo(cursor.getString(cursor.getColumnIndex(MEDIA_VIDEO)));
                entryBinding.setCsvVideoMediaShotdescription(cursor.getString(cursor.getColumnIndex(MEDIA_VIDEO_SHORT_DESCRIPTION)));
                entryBinding.setCsvVideoMediaDuration(cursor.getString(cursor.getColumnIndex(MEDIA_VIDEO_DURATION)));
                //Print.e(this, "Entry Type: "+ entryType +" Entry Id: "+ entry.getId());
                entryBindings.add(entryBinding);
                cursor.moveToNext();
            }
        }catch (Exception ex){
            ex.printStackTrace();
            Print.e(this, "failed to fatch getEntries");
            return null;
        }

        return entryBindings;
    }
    public EntryTypeBinding getEntryTypeBinding(long catId) {
        entryTypeBinding = new EntryTypeBinding();
        try {
                  /*String  query = "select en.* from "+TABLE_NAME
                        +" en inner join "+Entry_CatDao.TABLE_NAME
                        +" cat on (en."+ID+" = cat."+Entry_CatDao.RELETED_ID+
                        ") where cat."+Entry_CatDao.ENTRY_ID+"="+catId
                        +" and "+ ENTRY_TYPE+" = "+type;*/
            /*String query = "SELECT entry.*, cat.cat_id, (SELECT GROUP_CONCAT(ei.large, \",\") from MediaImageDao ei WHERE ei.entry_id = entry.id) as media_large,\n" +
                    "(SELECT GROUP_CONCAT(ei.small, \",\") from MediaImageDao ei WHERE ei.entry_id = entry.id) as media_small,\n" +
                    "(SELECT GROUP_CONCAT(vi.video, \",\") from MediaVideoDao vi WHERE vi.entry_id = entry.id) as media_video\n" +
                    "FROM\n" +
                    "entry entry\n" +
                    "INNER JOIN entry_m2m_cat  cat ON (entry.id = cat.entry_id)\n" +
                    "WHERE cat.cat_id="+catId+
                    " and "+ ENTRY_TYPE+" = "+type+"";*/

            String query = "SELECT en.*, " +
                    "cat."+Entry_CatDao.CAT_ID+", " +
                    "(SELECT c."+CategoryDao.TITLE+" from "+CategoryDao.TABLE_NAME+" c WHERE c."+CategoryDao.ID+" = "+catId+") as "+CategoryDao.TITLE+",\n" +
                    "(SELECT GROUP_CONCAT(cat."+Entry_CatDao.CAT_ID+", \",\") from "+Entry_CatDao.TABLE_NAME+" cat WHERE cat."+Entry_CatDao.ENTRY_ID+" = en."+ID+") as "+CATS_ID+",\n" +
                    "(SELECT GROUP_CONCAT(ei."+MediaImageDao.LARGE+", \",\") from "+MediaImageDao.TABLE_NAME+" ei WHERE ei."+MediaImageDao.ENTRY_ID+" = en."+ID+MediaImageDao.ORDER_BY+") as "+MEDIA_LARGE+",\n" +
                    "(SELECT GROUP_CONCAT(ei."+MediaImageDao.SMALL+", \",\") from "+MediaImageDao.TABLE_NAME+" ei WHERE ei."+MediaImageDao.ENTRY_ID+" = en."+ID+MediaImageDao.ORDER_BY+") as "+MEDIA_SMALL+",\n" +
                    "(SELECT GROUP_CONCAT(ei."+MediaImageDao.SHORT_DESCRIPTION+", \",\") from "+MediaImageDao.TABLE_NAME+" ei WHERE ei."+MediaImageDao.ENTRY_ID+" = en."+ID+MediaImageDao.ORDER_BY+") as "+MEDIA_IMAGE_SHORT_DESCRIPTION+",\n" +
                    "(SELECT GROUP_CONCAT(vi."+MediaVideoDao.VIDEO+", \",\") from "+MediaVideoDao.TABLE_NAME+" vi WHERE vi."+MediaVideoDao.ENTRY_ID+" = en."+ID+MediaVideoDao.ORDER_BY+") as "+MEDIA_VIDEO+",\n" +
                    "(SELECT GROUP_CONCAT(vi."+MediaVideoDao.DURATION+", \",\") from "+MediaVideoDao.TABLE_NAME+" vi WHERE vi."+MediaVideoDao.ENTRY_ID+" = en."+ID+MediaVideoDao.ORDER_BY+") as "+MEDIA_VIDEO_DURATION+",\n" +
                    "(SELECT GROUP_CONCAT(vi."+MediaVideoDao.SHORT_DESCRIPTION+", \",\") from "+MediaVideoDao.TABLE_NAME+" vi WHERE vi."+MediaVideoDao.ENTRY_ID+" = en."+ID+MediaVideoDao.ORDER_BY+") as "+MEDIA_VIDEO_SHORT_DESCRIPTION+"\n" +
                    "FROM " + TABLE_NAME+" en " +
                    "INNER JOIN "+Entry_CatDao.TABLE_NAME+"  cat ON (en."+ID+" = cat."+Entry_CatDao.ENTRY_ID+")\n" +
                    "WHERE cat."+Entry_CatDao.CAT_ID+"="+catId + orderBy;

            Cursor cursor = database.rawQuery(query, null);
            cursor.moveToFirst();
            //Print.e(this, "Cursor Size: "+ cursor.getCount());
            while (cursor.isAfterLast() == false) {
                EntryBinding entryBinding = new EntryBinding();
                Entry entry = new Entry();
                entry.setId((long) cursor.getInt(cursor.getColumnIndex(ID)));
                entry.setPrimaryCatId((long) cursor.getInt(cursor.getColumnIndex(PRIMARY_CAT_ID)));
                String csvCatsId = cursor.getString(cursor.getColumnIndex(CATS_ID));
                if (csvCatsId!= null){
                    List<Long> list = new ArrayList<Long>();
                    for (String s : csvCatsId.split(",")){
                        list.add(Long.parseLong(s));
                    }
                    entry.setCategories(list);
                }
                entry.setTitle(cursor.getString(cursor.getColumnIndex(TITLE)));
                int entryType = cursor.getInt(cursor.getColumnIndex(ENTRY_TYPE));
                entry.setEntryType((long)entryType);
                entry.setCatName(cursor.getString(cursor.getColumnIndex(CategoryDao.TITLE)));
                entry.setShortDescription(cursor.getString(cursor.getColumnIndex(SHORT_DESCRIPTION)));
                entry.setPubDate((long) cursor.getInt(cursor.getColumnIndex(PUB_DATE)));
                entry.setMediaType(cursor.getInt(cursor.getColumnIndex(MEDIA_TYPE)));
                entry.setOrder(cursor.getInt(cursor.getColumnIndex(ORDER)));
                entryBinding.setEntry(entry);
                entryBinding.setCsvImageLarge(cursor.getString(cursor.getColumnIndex(MEDIA_LARGE)));
                entryBinding.setCsvImageLargeShortDesc(cursor.getString(cursor.getColumnIndex(MEDIA_IMAGE_SHORT_DESCRIPTION)));
                entryBinding.setCsvImageSmall(cursor.getString(cursor.getColumnIndex(MEDIA_SMALL)));
                entryBinding.setCsvVideo(cursor.getString(cursor.getColumnIndex(MEDIA_VIDEO)));
                entryBinding.setCsvVideoMediaShotdescription(cursor.getString(cursor.getColumnIndex(MEDIA_VIDEO_SHORT_DESCRIPTION)));
                entryBinding.setCsvVideoMediaDuration(cursor.getString(cursor.getColumnIndex(MEDIA_VIDEO_DURATION)));
                //Print.e(this, "Entry Type: "+ entryType +" Entry Id: "+ entry.getId());
                entryTypeBinding.put(entryType, entryBinding);
                cursor.moveToNext();
            }
        }catch (Exception ex){
            ex.printStackTrace();
            Print.e(this, "failed to fatch getEntries");
            return null;
        }

        return entryTypeBinding;
    }

    public ArrayList<EntryBinding> getRelatedEntries(long entryId){
        List<Long> relatedsId = entryRelatedDao.getEntryRelatedEntriesId(entryId);
        return  getEntryBindings(relatedsId);
    }
    public ArrayList<EntryBinding> getEntryBindings(List<Long> entriesId){
        ArrayList<EntryBinding> entryBindings = new ArrayList<>();
        if (entriesId!=null){
            for (Long id: entriesId) {
                entryBindings.add(getEntryBinding(id));
            }
        }
        return entryBindings;
    }
    public EntryBinding getEntryBinding(long entyId) {
        EntryBinding entryBinding = new EntryBinding();
        try {

            String query = "SELECT en.*, " +
                    "(SELECT c."+CategoryDao.TITLE+" from "+CategoryDao.TABLE_NAME+" c WHERE c."+CategoryDao.ID+" = "+"(SELECT e."+PRIMARY_CAT_ID+" from "+TABLE_NAME+" e WHERE e."+ID+" = "+entyId+")) as "+CategoryDao.TITLE+",\n" +
                    "(SELECT GROUP_CONCAT(cat."+Entry_CatDao.CAT_ID+", \",\") from "+Entry_CatDao.TABLE_NAME+" cat WHERE cat."+Entry_CatDao.ENTRY_ID+" = en."+ID+") as "+CATS_ID+",\n" +
                    "(SELECT GROUP_CONCAT(ei."+MediaImageDao.LARGE+", \",\") from "+MediaImageDao.TABLE_NAME+" ei WHERE ei."+MediaImageDao.ENTRY_ID+" = en."+ID+") as "+MEDIA_LARGE+",\n" +
                    "(SELECT GROUP_CONCAT(ei."+MediaImageDao.SMALL+", \",\") from "+MediaImageDao.TABLE_NAME+" ei WHERE ei."+MediaImageDao.ENTRY_ID+" = en."+ID+") as "+MEDIA_SMALL+",\n" +
                    "(SELECT GROUP_CONCAT(ei."+MediaImageDao.SHORT_DESCRIPTION+", \",\") from "+MediaImageDao.TABLE_NAME+" ei WHERE ei."+MediaImageDao.ENTRY_ID+" = en."+ID+") as "+MEDIA_IMAGE_SHORT_DESCRIPTION+",\n" +
                    "(SELECT GROUP_CONCAT(vi."+MediaVideoDao.VIDEO+", \",\") from "+MediaVideoDao.TABLE_NAME+" vi WHERE vi."+MediaVideoDao.ENTRY_ID+" = en."+ID+") as "+MEDIA_VIDEO+",\n" +
                    "(SELECT GROUP_CONCAT(vi."+MediaVideoDao.DURATION+", \",\") from "+MediaVideoDao.TABLE_NAME+" vi WHERE vi."+MediaVideoDao.ENTRY_ID+" = en."+ID+") as "+MEDIA_VIDEO_DURATION+",\n" +
                    "(SELECT GROUP_CONCAT(vi."+MediaVideoDao.SHORT_DESCRIPTION+", \",\") from "+MediaVideoDao.TABLE_NAME+" vi WHERE vi."+MediaVideoDao.ENTRY_ID+" = en."+ID+") as "+MEDIA_VIDEO_SHORT_DESCRIPTION+"\n" +
                    "FROM " + TABLE_NAME+" en " +
                    "WHERE en."+ID+"="+entyId;

            Cursor cursor = database.rawQuery(query, null);
            cursor.moveToFirst();
            while (cursor.isAfterLast() == false) {
                Entry entry = new Entry();
                entry.setId((long) cursor.getInt(cursor.getColumnIndex(ID)));
                entry.setPrimaryCatId((long) cursor.getInt(cursor.getColumnIndex(PRIMARY_CAT_ID)));
                String csvCatsId = cursor.getString(cursor.getColumnIndex(CATS_ID));
                if (csvCatsId!= null){
                    List<Long> list = new ArrayList<Long>();
                    for (String s : csvCatsId.split(",")){
                        list.add(Long.parseLong(s));
                    }
                    entry.setCategories(list);
                }
                entry.setTitle(cursor.getString(cursor.getColumnIndex(TITLE)));
                int entryType = cursor.getInt(cursor.getColumnIndex(ENTRY_TYPE));
                entry.setEntryType((long)entryType);
                entry.setCatName(cursor.getString(cursor.getColumnIndex(CategoryDao.TITLE)));
                entry.setShortDescription(cursor.getString(cursor.getColumnIndex(SHORT_DESCRIPTION)));
                entry.setPubDate((long) cursor.getInt(cursor.getColumnIndex(PUB_DATE)));
                entry.setMediaType(cursor.getInt(cursor.getColumnIndex(MEDIA_TYPE)));
                entry.setOrder(cursor.getInt(cursor.getColumnIndex(ORDER)));
                entryBinding.setEntry(entry);
                entryBinding.setCsvImageLarge(cursor.getString(cursor.getColumnIndex(MEDIA_LARGE)));
                entryBinding.setCsvImageLargeShortDesc(cursor.getString(cursor.getColumnIndex(MEDIA_IMAGE_SHORT_DESCRIPTION)));
                entryBinding.setCsvImageSmall(cursor.getString(cursor.getColumnIndex(MEDIA_SMALL)));
                entryBinding.setCsvVideo(cursor.getString(cursor.getColumnIndex(MEDIA_VIDEO)));
                entryBinding.setCsvVideoMediaShotdescription(cursor.getString(cursor.getColumnIndex(MEDIA_VIDEO_SHORT_DESCRIPTION)));
                entryBinding.setCsvVideoMediaDuration(cursor.getString(cursor.getColumnIndex(MEDIA_VIDEO_DURATION)));
                cursor.moveToNext();
            }
        }catch (Exception ex){
            ex.printStackTrace();
            Print.e(this, "failed to fetch getEntries");
            return null;
        }

        return entryBinding;
    }

    public List<SearchItem> getSearchItems(){
        List<SearchItem> searchItems = new ArrayList<>();
        try {
            String query = "select "+ID+", "+TITLE+", "+PUB_DATE+", "+MEDIA_TYPE+" from "+TABLE_NAME;
            Cursor cursor = database.rawQuery(query, null);
            cursor.moveToFirst();
            while (cursor.isAfterLast() == false) {
                long id = (long)cursor.getInt(cursor.getColumnIndex(ID));
                String title = cursor.getString(cursor.getColumnIndex(TITLE));
                long pubDate = (long)cursor.getInt(cursor.getColumnIndex(PUB_DATE));
                int mediaType = (int)cursor.getInt(cursor.getColumnIndex(MEDIA_TYPE));
                SearchItem searchItem = new SearchItem(id, pubDate, title, mediaType);

                String queryCat = "select "+ PRIMARY_CAT_ID +" from "+TABLE_NAME+" where "+ ID +" = "+id +" limit 1";
                Cursor curEnCat = database.rawQuery(queryCat, null);
                curEnCat.moveToFirst();
                searchItem.setCatId(curEnCat.getInt(curEnCat.getColumnIndex(PRIMARY_CAT_ID)));

                Print.e(this, "getSearchIteam catId: "+ searchItem.getCatId());
                if (mediaType == TYPE_MEDIA_IMAGE){
                    String queryImage = "select "+MediaImageDao.SMALL+" from "+MediaImageDao.TABLE_NAME+" where "+ MediaImageDao.ENTRY_ID +" = "+id+" "+MediaImageDao.ORDER_BY +" limit 1";
                    Cursor cursorImage = database.rawQuery(queryImage, null);
                    cursorImage.moveToFirst();
                    if (cursorImage.getCount() > 0){
                        searchItem.setTumbUrl(cursorImage.getString(cursorImage.getColumnIndex(MediaImageDao.SMALL)));
                    }
                }else{
                    String queryVideo = "select "+MediaVideoDao.VIDEO+", "+MediaVideoDao.DURATION+ " from "+MediaVideoDao.TABLE_NAME+" where "+ MediaVideoDao.ENTRY_ID +" = "+id+" "+MediaVideoDao.ORDER_BY+" limit 1";
                    Cursor cursorVideo = database.rawQuery(queryVideo, null);
                    cursorVideo.moveToFirst();
                    if (cursorVideo.getCount() > 0){
                        searchItem.setVideoUrl(cursorVideo.getString(cursorVideo.getColumnIndex(MediaVideoDao.VIDEO)));
                        searchItem.setVideoDuration(cursorVideo.getString(cursorVideo.getColumnIndex(MediaVideoDao.DURATION)));
                    }
              }
                searchItems.add(searchItem);
                cursor.moveToNext();

            }

        }catch (Exception ex){
            ex.printStackTrace();
            Print.e(this, "Failed to retrive search items");
        }


                return searchItems;
    }

    /*public List<SearchItem> getSearchItems() {
        List<SearchItem> searchItems = new ArrayList<>();

        try {

            String query = "SELECT en.*, " +
                    "(SELECT GROUP_CONCAT(cat."+Entry_CatDao.ENTRY_ID+", \",\") from "+Entry_CatDao.TABLE_NAME+" cat WHERE cat."+Entry_CatDao.RELETED_ID+" = en."+ID+") as "+CATS_ID+",\n" +
                    "(SELECT GROUP_CONCAT(ei."+MediaImageDao.LARGE+", \",\") from "+MediaImageDao.TABLE_NAME+" ei WHERE ei."+MediaImageDao.RELETED_ID+" = en."+ID+") as "+MEDIA_LARGE+",\n" +
                    "(SELECT GROUP_CONCAT(ei."+MediaImageDao.SMALL+", \",\") from "+MediaImageDao.TABLE_NAME+" ei WHERE ei."+MediaImageDao.RELETED_ID+" = en."+ID+") as "+MEDIA_SMALL+",\n" +
                    "(SELECT GROUP_CONCAT(ei."+MediaImageDao.SHORT_DESCRIPTION+", \",\") from "+MediaImageDao.TABLE_NAME+" ei WHERE ei."+MediaImageDao.RELETED_ID+" = en."+ID+") as "+MEDIA_IMAGE_SHORT_DESCRIPTION+",\n" +
                    "(SELECT GROUP_CONCAT(vi."+MediaVideoDao.VIDEO+", \",\") from "+MediaVideoDao.TABLE_NAME+" vi WHERE vi."+MediaVideoDao.RELETED_ID+" = en."+ID+") as "+MEDIA_VIDEO+",\n" +
                    "(SELECT GROUP_CONCAT(vi."+MediaVideoDao.DURATION+", \",\") from "+MediaVideoDao.TABLE_NAME+" vi WHERE vi."+MediaVideoDao.RELETED_ID+" = en."+ID+") as "+MEDIA_VIDEO_DURATION+",\n" +
                    "(SELECT GROUP_CONCAT(vi."+MediaVideoDao.SHORT_DESCRIPTION+", \",\") from "+MediaVideoDao.TABLE_NAME+" vi WHERE vi."+MediaVideoDao.RELETED_ID+" = en."+ID+") as "+MEDIA_VIDEO_SHORT_DESCRIPTION+"\n" +
                    "FROM " + TABLE_NAME+" en ;";

            Cursor cursor = database.rawQuery(query, null);
            cursor.moveToFirst();
            while (cursor.isAfterLast() == false) {
                EntryBinding entryBinding = new EntryBinding();
                Entry entry = new Entry();
                entry.setId((long) cursor.getInt(cursor.getColumnIndex(ID)));
                String csvCatsId = cursor.getString(cursor.getColumnIndex(CATS_ID));
                if (csvCatsId!= null){
                    List<Long> list = new ArrayList<Long>();
                    for (String s : csvCatsId.split(",")){
                        list.add(Long.parseLong(s));
                    }
                    entry.setCategories(list);
                }
                entry.setTitle(cursor.getString(cursor.getColumnIndex(TITLE)));
                int entryType = cursor.getInt(cursor.getColumnIndex(ENTRY_TYPE));
                entry.setEntryType((long)entryType);
                entry.setShortDescription(cursor.getString(cursor.getColumnIndex(SHORT_DESCRIPTION)));
                entry.setPubDate((long) cursor.getInt(cursor.getColumnIndex(PUB_DATE)));
                entry.setMediaType((long)cursor.getInt(cursor.getColumnIndex(MEDIA_TYPE)));
                entry.setOrder(cursor.getInt(cursor.getColumnIndex(ORDER)));
                entryBinding.setEntry(entry);
                entryBinding.setCsvImageLarge(cursor.getString(cursor.getColumnIndex(MEDIA_LARGE)));
                entryBinding.setCsvImageLargeShortDesc(cursor.getString(cursor.getColumnIndex(MEDIA_IMAGE_SHORT_DESCRIPTION)));
                entryBinding.setCsvImageSmall(cursor.getString(cursor.getColumnIndex(MEDIA_SMALL)));
                entryBinding.setCsvVideo(cursor.getString(cursor.getColumnIndex(MEDIA_VIDEO)));
                entryBinding.setCsvVideoMediaShotdescription(cursor.getString(cursor.getColumnIndex(MEDIA_VIDEO_SHORT_DESCRIPTION)));
                entryBinding.setCsvVideoMediaDuration(cursor.getString(cursor.getColumnIndex(MEDIA_VIDEO_DURATION)));
                entryBindings.add(entryBinding);
                cursor.moveToNext();
            }
        }catch (Exception ex){
            ex.printStackTrace();
            Print.e(this, "failed to fatch getEntries");
            return null;
        }

        return entryBindings;
    }*/

    public String getDetails(long id){
        String description = null;
        try {
            String query = "select "+DETAILS+" from "+TABLE_NAME+" where "+ID+" = "+id;
            Cursor cursor = database.rawQuery(query, null);
            cursor.moveToFirst();
            while (cursor.isAfterLast() == false){
                description = cursor.getString(cursor.getColumnIndex(DETAILS));
                cursor.moveToNext();
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return description;

    }

    public void updateNotified(Entry entry, int value){
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put(NOTIFIED, value);
            database.update(TABLE_NAME, contentValues,
                    whereId,
                    new String[] {entry.getId()+"" });


        }catch (Exception ex){
            ex.printStackTrace();
            Print.e(this, "Failed to update Entry NOTIFIED: "+entry.getId());
        }
    }

    public int getNotified(Entry entry){
        String select = "select "+NOTIFIED+" from "+TABLE_NAME+" where "+ID+" = "+entry.getId();
        Cursor cursor = database.rawQuery(select, null);
        cursor.moveToFirst();
        if (cursor.getCount() > 0){
            return cursor.getInt(cursor.getColumnIndex(NOTIFIED));
        }
        return 0;
    }
    public int exist(long entryId){
        try {
            String query = "select "+ID+" from "+TABLE_NAME+" where "+ID+" = "+entryId;
            Cursor cursor = database.rawQuery(query, null);
            return cursor.getCount();
        }catch (Exception ex){
            ex.printStackTrace();
            Print.e(this, "Entry Exist Check Failed Exception");
        }
        return 0;
    }
    public int count(long id){
        try {
            String count = "SELECT "+ID+" FROM "+ TABLE_NAME + " where "+ID+" = "+ id;
            Cursor mcursor = database.rawQuery(count, null);
            mcursor.moveToFirst();
            return mcursor.getCount();
        }catch (Exception ex){
            ex.printStackTrace();
            Print.e(this, "Failed to count Entries of cat: "+ id);
            return -1;
        }

    }


}
