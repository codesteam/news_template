package com.db.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.text.TextUtils;

import com.commonasset.utils.Print;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by water on 12/6/16.
 */
public class Entry_CatDao extends BaseDao {
    public static String TABLE_NAME= "table_entry_m2m_cat";
    public static String CAT_ID = "cat_id";
    public static String ENTRY_ID = "entry_id";
    public static String CREATE= "create table "+TABLE_NAME+
            "( "+
            ENTRY_ID+" integer not null, "+
            CAT_ID+" integer not null, " +
            "PRIMARY KEY ("+ENTRY_ID+", "+CAT_ID+"));";

    private static final String WHERE_ENTRY_ID_EQUALS = ENTRY_ID
            + " =?";
    public Entry_CatDao(Context context) {
        super(context);
    }
    public long insert(long catId, long entryID){
        ContentValues contentValues = new ContentValues();
        contentValues.put(CAT_ID, catId);
        contentValues.put(ENTRY_ID, entryID);
        //Print.e(this, "inserting subCat: "+ subCatetory.getTitle());
        return database.insertOrThrow(TABLE_NAME, null, contentValues);
    }

    public void updateCat(long entryID,   Long[] catIds) {
        String csvCatIds = TextUtils.join(",", catIds);
        Print.e(this, csvCatIds);
        try {
            String deleteSql = "delete from "+TABLE_NAME+" where "+CAT_ID+" not in("+csvCatIds+")"+" and "+ENTRY_ID+ " = "+entryID+";";
            database.rawQuery(deleteSql, null).moveToFirst();
           // Print.e(this, "updateCat delete Operation successful");
        }catch (Exception ex){
            ex.printStackTrace();
            Print.e(this, "Failed to delete using updateCat ");
        }
        try {
            for (int i = 0; i < catIds.length; i++){
                /*String insert = "insert into "+TABLE_NAME+" values( "+entryID+", "+catIds[i]+" ) where NOT EXISTS "+
                        "(select "+ENTRY_ID+" from "+TABLE_NAME+" where "+RELETED_ID+" = "+entryID+");";*/
                String insert = "insert or ignore into "+TABLE_NAME+" values ("+entryID+", "+catIds[i]+" ) ";
                database.rawQuery(insert, null).moveToFirst();
            }
            //Print.e(this, "updateCat delete then insert Operation successful");
        }catch (Exception ex){
            ex.printStackTrace();
            Print.e(this, "Failed to insert using updateCat");
        }

    }

    public int delete(String csvEntriesId){
        try {
           /* String deleteSql = "delete from "+TABLE_NAME+" where "+ENTRY_ID+" in("+csvEntriesId+");";
             Cursor cursor = database.rawQuery( deleteSql, null );
             cursor.moveToFirst();
             return cursor.getCount();*/
            return database.delete(TABLE_NAME, ENTRY_ID+" IN (" + csvEntriesId + ")", null);
        }catch (Exception ex){
            ex.printStackTrace();
            Print.e(this, "Unable to delete all row from Entry_catDao under entries " + csvEntriesId);
        }

        return 0;
    }
    public void delete(List[] entriesID){
        String csvEntriesId = TextUtils.join(",", entriesID);
        try {
            String deleteSql = "delete from "+TABLE_NAME+" where "+ENTRY_ID+" in("+csvEntriesId+");";
            database.rawQuery( deleteSql, null ).moveToFirst();
        }catch (Exception ex){
            ex.printStackTrace();
            Print.e(this, "Unable to delete all row from Entry_catDao under entries " + csvEntriesId);
        }


    }
  /*  public void delete(long catId, ) {
        String csvEntriesId = TextUtils.join(",", entriesID);

        List<Long> entyIds = new ArrayList<>();
        String selectQuery = "select "+RELETED_ID+" from "+TABLE_NAME+" where "+RELETED_ID+" not in("+csvEntriesId+")"+" and "+ENTRY_ID+ " = "+catId+";";
        Cursor selectCursor =  database.rawQuery( selectQuery, null );
        selectCursor.moveToFirst();
        while(selectCursor.isAfterLast() == false){
            entyIds.add((long)selectCursor.getInt(selectCursor.getColumnIndex(RELETED_ID)));
            selectCursor.moveToNext();
        }

        String deleteSql = "delete from "+TABLE_NAME+" where "+RELETED_ID+" not in("+csvEntriesId+")"+" and "+ENTRY_ID+ " = "+catId+";";
        database.rawQuery(deleteSql, null).moveToFirst();

        String csvDeleteEntriesId = TextUtils.join(",", entriesID);
        String checkQuery = "select "+RELETED_ID+" from "+TABLE_NAME+" where "+RELETED_ID+" in("+csvDeleteEntriesId+")"+";";

        Cursor checkCursor =  database.rawQuery( checkQuery, null );
        checkCursor.moveToFirst();
        if (checkCursor.getCount() == 0){ // delete from entry table

        }
    }*/

    public List<Long> getEntryIds(long catId){
        List<Long> entyIds = new ArrayList<>();
        Cursor catCursor =  database.rawQuery( "select * from "+TABLE_NAME + " where "+CAT_ID+" = "+ catId, null );
        catCursor.moveToFirst();

        while(catCursor.isAfterLast() == false){
            entyIds.add((long)catCursor.getInt(catCursor.getColumnIndex(ENTRY_ID)));
            catCursor.moveToNext();
        }
        return  entyIds;
    }

    public int count(long catId){
        String count = "SELECT "+ID+" FROM "+ TABLE_NAME + " where "+CAT_ID+" = "+ catId;
        Cursor mcursor = database.rawQuery(count, null);
        mcursor.moveToFirst();
        return mcursor.getCount();
    }

}
