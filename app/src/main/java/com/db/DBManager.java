package com.db;

import android.content.Context;

import com.db.dao.IListOperation;
import com.retrofit.response.category.Category;

import java.util.HashMap;
import java.util.List;

/**
 * Created by water on 11/28/16.
 */
public class DBManager {
    private static DBManager dbManager;
    private List<Category> categories;
    private static DBHelper dbHelper;
    HashMap<String, IListOperation> daoMap = new HashMap<String, IListOperation>();
    public static DBManager getInstance(Context context){
        if (dbManager == null){
            dbHelper = new DBHelper(context);
            return dbManager = new DBManager();
        }
        return  dbManager;
    }


    public void createDaoMap(Context context){
       // daoMap.put(EntryDao.class.getName(), new EntryDao(context));
    }

    public IListOperation getDao(String className){
       return daoMap.get(className);
    }
    /*public List<Category> getCategories(boolean refresh) {
        if (refresh || categories == null){
            categories = dbHelper.getQueryManager().getCategoriesSubCategories();
            return categories;
        }
        return categories;
    }*/


}
