package com.db.helper;

import com.db.dao.EntryDao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by water on 12/18/16.
 */

public class EntryTypeBinding {
    HashMap<Integer, List<EntryBinding>> hashMap =new HashMap<>(); // all entris in a category sapareted according to their type like:
                                                                    // general entry , special entry, featured entry, custom entry ....

    List<EntryBinding> allEntriesSameCat = new ArrayList<>();

    public void put(int key,  EntryBinding value){
        List<EntryBinding> entryBindings = get(key);
        if (entryBindings == null){
            entryBindings = new ArrayList<>();
            hashMap.put(key, entryBindings);
        }
       /* switch (key){
            case EntryDao.TYPE_SPECIAL:
                if (entryBindings.size() < CommonConstants.MAX_NUMBER_OF_SPECIAL_NEWS){
                    entryBindings.add(value);
                }
                break;
            default:
                entryBindings.add(value);
        }*/

        entryBindings.add(value);
        allEntriesSameCat.add(value);
    }

    public List<EntryBinding> get(int key){
        if (key == EntryDao.TYPE_SPECIAL){
            List<EntryBinding> items = hashMap.get(key);
            if (items != null){
                if (items.size() > 2){
                    return items.subList(0, 2);
                }
            }
        }
        return  hashMap.get(key);
    }

    public List<EntryBinding> getAllEntriesSameCat() {
        return allEntriesSameCat;
    }
}
