package com.db.helper;

import com.commonasset.utils.Print;
import com.retrofit.response.entry.Entry;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by water on 12/15/16.
 */
public class EntryBinding implements Serializable {

    Entry entry;
    ArrayList<String> smallImageMedia;
    ArrayList<String> largeImageMedia;
    ArrayList<String> largeImageMediaShortDesc;
    ArrayList<String> videoMedia;
    ArrayList<String> videoMediaShortDescription;
    ArrayList<String> videoMediaDuration;

    public Entry getEntry() {
        return entry;
    }

    public void setEntry(Entry entry) {
        this.entry = entry;
    }

    public ArrayList<String> getLargeImageMedia() {
        return largeImageMedia;
    }

    public ArrayList<String> getLargeImageMediaShortDesc() {
        return largeImageMediaShortDesc;
    }

    public ArrayList<String> getSmallImageMedia() {
        return smallImageMedia;
    }

    public ArrayList<String> getVideoMedia() {
        return videoMedia;
    }

    public ArrayList<String> getVideoMediaShortDescription() {
        return videoMediaShortDescription;
    }

    public ArrayList<String> getVideoMediaDuration() {
        return videoMediaDuration;
    }

    public void setCsvImageSmall(String csv){
        if (csv != null){
            smallImageMedia = new ArrayList<>(Arrays.asList(csv.split("\\s*,\\s*")));
        }

    }
    public void setCsvImageLarge(String csv){
        Print.e(this, "setCsvImageLarge: " + csv);
        if (csv != null){
            Print.e(this, csv);
            largeImageMedia = new ArrayList<>(Arrays.asList(csv.split("\\s*,\\s*")));
        }

    }

    public void setCsvImageLargeShortDesc(String csv){
        Print.e(this, "setCsvImageLargeShortDesc: "+csv);
        if (csv != null){
            Print.e(this, csv);
            largeImageMediaShortDesc = new ArrayList<>(Arrays.asList(csv.split("\\s*,\\s*")));
        }

    }

    public void setCsvVideo(String csv){
        Print.e(this, "setCsvVideo"+csv);
        if (csv != null){
            videoMedia = new ArrayList<>(Arrays.asList(csv.split("\\s*,\\s*")));
        }

    }

    public void setCsvVideoMediaShotdescription(String csv){
        if (csv != null){
            videoMediaShortDescription = new ArrayList<>(Arrays.asList(csv.split("\\s*,\\s*")));
        }

    }

    public void setCsvVideoMediaDuration(String csv){
        if (csv != null){
            videoMediaDuration = new ArrayList<>(Arrays.asList(csv.split("\\s*,\\s*")));
        }

    }

    public void setLargeImageMedia(ArrayList<String> largeImageMedia) {
        this.largeImageMedia = largeImageMedia;
    }

    public void setSmallImageMedia(ArrayList<String> smallImageMedia) {
        this.smallImageMedia = smallImageMedia;
    }

    public void setLargeImageMediaShortDesc(ArrayList<String> largeImageMediaShortDesc) {
        this.largeImageMediaShortDesc = largeImageMediaShortDesc;
    }

    public void setVideoMedia(ArrayList<String> videoMedia) {
        this.videoMedia = videoMedia;
    }

    public void setVideoMediaDuration(ArrayList<String> videoMediaDuration) {
        this.videoMediaDuration = videoMediaDuration;
    }

    public void setVideoMediaShortDescription(ArrayList<String> videoMediaShortDescription) {
        this.videoMediaShortDescription = videoMediaShortDescription;
    }
}
