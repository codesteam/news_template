package com.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.db.dao.CategoryDao;
import com.db.dao.EntryDao;
import com.db.dao.EntryRelatedDao;
import com.db.dao.Entry_CatDao;
import com.db.dao.GroupDaoTemp;
import com.db.dao.MediaImageDao;
import com.db.dao.MediaVideoDao;
import com.db.dao.StatusDao;

/**
 * Created by water on 3/19/16.
 */
public class DBHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "karunews.db";
    public static final String TABLE_NAME = "table_category";

    private static DBHelper instance;

    public static synchronized DBHelper getHelper(Context context) {
        if (instance == null)
            instance = new DBHelper(context);
        return instance;
    }

    QueryManager queryManager;

    public QueryManager getQueryManager() {
        return queryManager;
    }

    public DBHelper(Context context)
    {
        super(context, DATABASE_NAME, null, 1);
        queryManager = new QueryManager(context, this);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // TODO Auto-generated method stub
        db.execSQL("PRAGMA foreign_keys = ON;");
        db.execSQL(StatusDao.CREATE);
        db.execSQL(CategoryDao.CREATE);
        db.execSQL(EntryDao.CREATE);
        db.execSQL(Entry_CatDao.CREATE);
        db.execSQL(EntryRelatedDao.CREATE);
        db.execSQL(MediaImageDao.CREATE);
        db.execSQL(MediaVideoDao.CREATE);
        db.execSQL(GroupDaoTemp.CREATE);
        //db.execSQL(gpAd);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub
        db.execSQL("DROP TABLE IF EXISTS "+ StatusDao.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS "+ CategoryDao.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS "+ Entry_CatDao.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS "+ EntryRelatedDao.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS "+ MediaImageDao.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS "+ MediaVideoDao.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS "+ EntryDao.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS "+ GroupDaoTemp.TABLE_NAME);
        onCreate(db);
    }



    public void deleteGPads ()
    {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from " + TABLE_NAME);
    }


}