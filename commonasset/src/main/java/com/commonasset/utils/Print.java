package com.commonasset.utils;

import android.util.Log;

public class Print {

	public static boolean enable_log_print = true; //= GlobalStates.dev_build;
	
	// e
	public static void e(Object obj, String msg)
	{
		Print.printE(Print.pullOut(obj.getClass().getName()), msg );
	}
	
	public static void e(Object obj, int msg)
	{
		Print.printE(Print.pullOut(obj.getClass().getName()), msg + "");
	}
	
	public static void e(Object obj, boolean msg)
	{
		Print.printE(Print.pullOut(obj.getClass().getName()), msg + "");
	}
	
	public static void e(Object obj, float msg)
	{
		Print.printE(Print.pullOut(obj.getClass().getName()), msg + "");
	}
	
	public static void e(Object obj, Long msg)
	{
		Print.printE(Print.pullOut(obj.getClass().getName()), msg + "");
	}
	
	
	// d
	
	public static void d(Object obj, String msg)
	{
		Print.printD(Print.pullOut(obj.getClass().getName()), msg );
	}
	
	public static void d(Object obj, int msg)
	{
		Print.printD(Print.pullOut(obj.getClass().getName()), msg + "");
	}
	
	public static void d(Object obj, boolean msg)
	{
		Print.printD(Print.pullOut(obj.getClass().getName()), msg + "");
	}
	
	public static void d(Object obj, float msg)
	{
		Print.printD(Print.pullOut(obj.getClass().getName()), msg + "");
	}
	
	
	// v
	
	
	public static void v(Object obj, String msg)
	{
		Print.printV(Print.pullOut(obj.getClass().getName()), msg);
	}
	
	public static void v(Object obj, int msg)
	{
		Print.printV(Print.pullOut(obj.getClass().getName()), msg + "");
	}
	
	public static void v(Object obj, boolean msg)
	{
		Print.printV(Print.pullOut(obj.getClass().getName()), msg + "");
	}
	
	public static void v(Object obj, float msg)
	{
		Print.printV(Print.pullOut(obj.getClass().getName()), msg + "");
	}
	
	// i
	
	
	public static void i(Object obj, String msg)
	{
		Print.printI(Print.pullOut(obj.getClass().getName()), msg);
	}
	
	public static void i(Object obj, int msg)
	{
		Print.printI(Print.pullOut(obj.getClass().getName()), msg + "");
	}
	
	public static void i(Object obj, boolean msg)
	{
		Print.printI(Print.pullOut(obj.getClass().getName()), msg + "");
	}
	
	public static void i(Object obj, float msg)
	{
		Print.printI(Print.pullOut(obj.getClass().getName()), msg + "");
	}
	
	
	private static void printE(String class_name, String msg)
	{
		if (enable_log_print)
		{
			Log.e(class_name, msg);
		}
	}
	
	private static void printD(String class_name, String msg)
	{
		if (enable_log_print)
		{
			Log.d(class_name, msg);
		}
	}
	
	private static void printV(String class_name, String msg)
	{
		if (enable_log_print)
		{
			Log.v(class_name, msg);
		}
	}
	
	private static void printI(String class_name, String msg)
	{
		if (enable_log_print)
		{
			Log.i(class_name, msg);
		}
	}
	
	private static String pullOut(String class_name)
	{
		 int mid=class_name.lastIndexOf ('.') + 1;
		 
		 return class_name.substring(mid);
	}
}
