package com.commonasset.utils;

import android.content.Context;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.target.Target;
import com.commonasset.R;

/**
 * Created by water on 12/13/16.
 */
public class GlideImageRenderer {
    public static void renderThumbnail(Context context, ImageView imageView, String url, final ImageRenderListner imageRenderListner){
        Print.e("", "Glide Url Thumb: "+url);
        Glide.with(context).load(url+"")
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .dontAnimate()
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        return true;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        return false;
                    }
                })
                .error(R.drawable.image_not_found_thumbnail)
                .into(imageView);
    }

    public static void renderFeatured(Context context, ImageView imageView, String url, final ImageRenderListner imageRenderListner){
        Glide.with(context).load(url+"")
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .dontAnimate()
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        if (imageRenderListner != null){
                            imageRenderListner.onFailed();
                        }
                        Print.e(this, "Glide onException");
                        return true;
                    }

                    @Override
                    protected void finalize() throws Throwable {
                        super.finalize();
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        if (imageRenderListner != null){
                            imageRenderListner.onSuccess();
                        }
                        return false;
                    }
                })
                .error(R.drawable.image_not_found_large)
                .into(imageView);
    }

    public static void renderGallery(Context context, final ImageView imageView, String url, final ImageRenderListner imageRenderListner){
        Glide.with(context).load(url+"")
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .dontAnimate()
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        if (imageRenderListner != null){
                            imageRenderListner.onFailed();
                        }
                        return true;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        if (imageRenderListner != null){
                            imageRenderListner.onSuccess();
                        }
                        return false;
                    }
                })
                .into(new SimpleTarget<GlideDrawable>() {
                    @Override
                    public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> glideAnimation) {
                        imageView.setImageDrawable(resource);
                    }
                });
    }
}
